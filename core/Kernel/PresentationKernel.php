<?php
/**
 * Class file for Laabs Dynamic Kernel
 * @package core\Kernel
 */
namespace core\Kernel;

/**
 * Class Laabs Dynamic Kernel
 *
 * @extends core\Kernel\AbstractKernel
 */
class PresentationKernel
    extends AbstractKernel
{
    /* Constants */

    /* Properties */
    /**
     * The Command definition if found in presentation commands
     * @var \core\Reflection\UserCommand
     */
    public $userCommand;

    /**
     * The userInput Router
     * @var \core\Route\UserInputRouter
     */
    public $userInputRouter;

    /**
     * The View Router
     * @var \core\Route\ViewRouter
     */
    public $viewRouter;

    /**
     * The message parts
     * @var array
     */
    public $userMessage = array();

    /**
     * The Return of the business action(s)
     * @var array
     */
    public $serviceReturns = array();

    /* Methods */
    /**
     * Run the kernel to process request
     */
    public static function run()
    {
        /* Initalize components app/dependecy/bundle */
        self::$instance->initPackages();

        self::$instance->attachObservers();

        \core\Observer\Dispatcher::notify(LAABS_REQUEST, self::$instance->request);

        /* Establish routes (input, action, output) */
        self::$instance->setRoutes();

        self::$instance->parseRequest();

        /* Call Command */
        try {

            self::$instance->callUserCommand();

        } catch (\Exception $exception) {
            if (!self::$instance->handleException($exception)) {
                self::$instance->response->setBody((string) $exception);
                self::$instance->response->setCode(500);
                self::$instance->sendResponse();

                return;
            }
        }

        self::$instance->presentResponse();

        \core\Observer\Dispatcher::notify(LAABS_RESPONSE, self::$instance->response);

        //Send response;
        self::$instance->sendResponse();
    }

    /**
     * Set the action router for kernel
     * @access protected
     */
    protected function setRoutes()
    {
        $this->userCommand = \laabs::command($this->request->method, $this->request->uri);

        // Notify of command
        \core\Observer\Dispatcher::notify(LAABS_USER_COMMAND, $this->userCommand);

        if ($this->response->mode == 'http') {
            $this->response->setHeader("X-Laabs-UserStory", $this->userCommand->userStory);
        }

        $this->setUserInputRouter();
        $this->setViewRouter();
    }

    protected function setUserInputRouter()
    {
        if (isset($this->userCommand->userInput)) {
            $this->userInputRouter = new \core\Route\UserInputRouter($this->userCommand->userInput);
        } else {
            try {
                $this->userInputRouter = new \core\Route\UserInputRouter($this->userCommand->getName());
            } catch (\Exception $e) {
            }
        }
    }

    protected function setViewRouter()
    {
        if (isset($this->userCommand->view)) {
            $this->viewRouter = new \core\Route\ViewRouter($this->userCommand->view);
        } else {
            try {
                $this->viewRouter = new \core\Route\ViewRouter($this->userCommand->getName());
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * Parse current request body with parser
     * @access protected
     */
    protected function parseRequest()
    {
        if (isset($this->userInputRouter)) {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-Composer", $this->userInputRouter->uri);
            }
            $composer = $this->userInputRouter->composer->newInstance();

            $this->userMessage = $this->userInputRouter->userInput->compose($composer, $this->request->body, $this->request->query);
        } else {
            switch ($this->request->queryType) {
                case 'lql':
                    // Parse query string
                    //$queryArguments = \laabs::parseQueryString($this->request->query);

                case 'url':
                default:
                    $queryArguments = $_GET;
                    break;
            }

            switch ($this->request->contentType) {
                case 'url':
                    $bodyArguments = \core\Encoding\url::decode($this->request->body);
                    break;

                case 'json':
                default:
                    $bodyArguments = \core\Encoding\json::decode($this->request->body);
                    break;
            }

            $this->userMessage = array_merge($queryArguments, $bodyArguments);
        }
    }

    /**
     * Call current command with userStory
     * @access protected
     */
    protected function callUserCommand()
    {
        if (!count($this->userCommand->services)) {
            return;
        }

        foreach ($this->userCommand->services as $name => $service) {
            $this->serviceReturns[] = $this->callService($service);
        }

        // Notify of command for authorizations and log
        \core\Observer\Dispatcher::notify(LAABS_COMMAND_RETURN, $this->serviceReturns);

    }

    protected function callService($service)
    {
        $pathRouter = new \core\Route\PathRouter($service);
        $servicePath = $pathRouter->path;

        // Get service message from request arguments and parsed body
        $serviceMessage = $servicePath->getMessage($this->userMessage);

        $valid = \laabs::validateMessage($serviceMessage, $servicePath);
        
        if (!$valid) {
            $e = new \core\Exception\BadRequestException();
            $e->errors = \laabs::getValidationErrors();

            throw $e;
        }

        // Extract service path variables from received user command AND previous services named returns
        if (!empty($servicePath->variables)) {
            foreach ($servicePath->variables as $name => $value) {
                switch (true) {
                    // Value available from command
                    case isset($this->userCommand->variables[$name]):
                        $value = $this->userCommand->variables[$name];
                        break;
                    // Value available in a previous service return
                    case isset($this->serviceReturns[$name]):
                        $value = $this->serviceReturns[$name];
                        break;
                }

                $servicePath->setVariable($name, $value);
            }
        }

        \core\Observer\Dispatcher::notify(LAABS_SERVICE_PATH, $servicePath, $serviceMessage);

        // Get controller action
        if (isset($servicePath->action)) {
            $actionRouter = new \core\Route\ActionRouter($servicePath->action);
        } else {
            $actionRouter = new \core\Route\ActionRouter($servicePath->getName());
        }

        // Order arguments using action parameters
        $parameters = $actionRouter->action->getParameters();
        
        $actionParameters = array();
        foreach ($parameters as $parameter) {
            switch (true) {
                // Value available in route pattern
                case isset($servicePath->variables[$parameter->name]):
                    $value = $servicePath->variables[$parameter->name];
                    break;

                // Value available from request arguments or body: cast into message part type
                case isset($serviceMessage[$parameter->name]):
                    $value = $serviceMessage[$parameter->name];
                    break;

                // Default value
                case $parameter->isDefaultValueAvailable():
                    $value = $parameter->getDefaultValue();
                    break;

                // Optional : null
                case $parameter->isOptional():
                    $value = null;
                    break;

                // No other case should raise an exception
                default:
                    // Throw exception
                    $value = null;
            }

            $actionParameters[$parameter->name] = $value;
        }

        // Backward remove null values from array of arguments
        do {
            $arg = end($actionParameters);
            if ($arg === null) {
                array_pop($actionParameters);
            }
        } while ($arg === null && count($actionParameters));

        $controller = $actionRouter->controller->newInstance();

        $serviceReturn = $actionRouter->action->call($controller, $actionParameters);

        \core\Observer\Dispatcher::notify(LAABS_SERVICE_RETURN, $serviceReturn);

        return $serviceReturn;
    }

    /**
     * Handle Exception sent by action
     * @param \Exception $exception The Exception thrown by the Action
     *
     * @return bool
     */
    protected function handleException(\Exception $exception)
    {

        \core\Observer\Dispatcher::notify(LAABS_BUSINESS_EXCEPTION, $exception);

        // Manage specific exception handler
        $exceptionClass = get_class($exception);
        $exceptionName = \laabs\basename($exceptionClass);
        
        if ($this->response->mode == 'http') {
            $this->response->setHeader("X-Laabs-Exception", $exceptionClass."; ".str_replace("\n", " ", $exception->getMessage()));
        }

        $this->serviceReturns = array($exception);

        if ($exception instanceof \core\Exception) {
            $this->response->setCode($exception->getCode());
        }

        // Try to find view for the raised exception else send exception as string as response content
        if (isset($this->viewRouter)) {

            switch (true) {
                case $this->viewRouter->presenter->hasView($exceptionName) :
                    $this->viewRouter->setView($exceptionName);

                    return true;

                case \laabs::presentation()->hasPresenter('Exception'):
                    $presenter = \laabs::presentation()->getPresenter('Exception');
                    $this->viewRouter->setPresenter("Exception");
                    switch (true) {
                        case $this->viewRouter->presenter->hasView($exceptionName) :
                            $this->viewRouter->setView($exceptionName);

                            return true;

                        case $this->viewRouter->presenter->hasView('Exception'):
                            $this->viewRouter->setView('Exception');

                            return true;
                    }
            }
        }

        return false;
    }

    /**
     * Present current return to set response body with presenter
     * @access protected
     */
    protected function presentResponse()
    {
        if (isset($this->viewRouter)) {
            if ($this->response->mode == 'http') {
                $this->response->setHeader("X-Laabs-View", $this->viewRouter->uri);
            }

            $presenter = $this->viewRouter->presenter->newInstance();

            $content = $this->viewRouter->view->present($presenter, $this->serviceReturns);

        } else {

            switch ($this->response->contentType) {
                case 'json':
                default:
                    if (count($this->serviceReturns) == 1) {
                        $content = \core\Encoding\json::encode(reset($this->serviceReturns));
                    } else {
                        $content = \core\Encoding\json::encode($this->serviceReturns);
                    }

                    break;
            }
        }

        $this->response->setBody($content);
    }
}

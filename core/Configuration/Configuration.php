<?php
/**
 * Class file for App Configuration
 * @package core\Configuration
 */
namespace core\Configuration;

/**
 * Class that defines the configuration singleton object
 */
class Configuration
    extends Section
{
    /* Constants */

    /* Properties */
    /**
     * The object storage for the singleton instance
     * @var array
     * @static
     * @access protected
     */
    protected static $instance;

    /**
     * The set of variables defined on configurations
     * @var array
     */
    public $variables;

    /* Methods */
    /**
     * Get the configuration instance. It can be
     *  * from configuration files
     *  * from Instance of application if loaded
     *  * from the already instantiated instance during the same call to app
     * @return \core\Configuration\Configuration object
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            if ($configuration = \laabs::getCache('configuration')) {
                self::$instance = $configuration;
            } else {
                self::$instance = new Configuration();
                self::$instance->registerVariable('laabsDirectory', dirname(getcwd()));
                
                $confFile = \laabs::getConfiguration();
                self::$instance->loadFile($confFile);
                
                \laabs::setCache('configuration', self::$instance);
            }
        }

        return self::$instance;
    }

    /**
     * Registers a variable on the global scope
     * @param string $name  The name of the variable
     * @param mixed  $value The value of the variable
     * 
     * @return void
     */
    public function registerVariable($name, $value)
    {
        $this->variables[$name] = $value;
    }

    /**
     * Retrieves a variable on the global scope
     * @param string $name the name of the variable
     * 
     * @return mixed $value The value of the variable
     */
    public function getVariable($name)
    {
        if (isset($this->variables[$name])) {
            return $this->variables[$name];
        }
    }

}
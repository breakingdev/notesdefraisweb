DELETE FROM "businessExpenses"."businessCategory";
DELETE FROM "businessExpenses"."mileageAllowanceScale";
DELETE FROM "businessExpenses"."project";
DELETE FROM "businessExpenses"."expenseLine";
DELETE FROM "businessExpenses"."expenseReport";

-- business category
INSERT INTO "businessExpenses"."businessCategory"("businessCategoryId", "code", "description", "formType", "system") VALUES 
('REGULARIZATION', 'REGULARIZATION', 'Régularisation', 'amount', true),
('DEPLACEMENT_PRO', 'DEPLACEMENT_PRO', 'Déplacement professionnel', 'mileageExpense', false),
('LOCATION', 'LOCATION', 'Location', 'amount', false),
('TAXI', 'TAXI', 'Taxi', 'amount', false),
('RESTAURANT', 'RESTAURANT', 'Restaurant', 'amount', false),
('HOTEL', 'HOTEL', 'Hotel', 'amount', false);



-- business category
INSERT INTO "businessExpenses"."mileageAllowanceScale"("mileageAllowanceScaleId", "year", "category", "taxHorsepower", "rangeBegin", "rangeEnd", "rate", "regularization") VALUES 
('mileageAllowanceScale_01', '2016', 'car', '3', '0', '5000', '0.41', null),
('mileageAllowanceScale_02', '2016', 'car', '3', '5001', '20000', '0.245', 824),
('mileageAllowanceScale_03', '2016', 'car', '3', '20000', '100000', '0.286', null),

('mileageAllowanceScale_04', '2016', 'car', '4', '0', '5000', '0.493', null),
('mileageAllowanceScale_05', '2016', 'car', '4', '5001', '20000', '0.277', 1082),
('mileageAllowanceScale_06', '2016', 'car', '4', '20000', '100000', '0.332', null),

('mileageAllowanceScale_07', '2016', 'car', '5', '0', '5000', '0.543', null),
('mileageAllowanceScale_08', '2016', 'car', '5', '5001', '20000', '0.305', 1188),
('mileageAllowanceScale_09', '2016', 'car', '5', '20000', '100000', '0.364', null),

('mileageAllowanceScale_10', '2016', 'car', '6', '0', '5000', '0.68', null),
('mileageAllowanceScale_11', '2016', 'car', '6', '5001', '20000', '0.32', 1244),
('mileageAllowanceScale_12', '2016', 'car', '6', '20000', '100000', '0.382', null),

('mileageAllowanceScale_13', '2016', 'car', '7', '0', '5000', '0.595', null),
('mileageAllowanceScale_14', '2016', 'car', '7', '5001', '20000', '0.337', 1288),
('mileageAllowanceScale_15', '2016', 'car', '7', '20000', '100000', '0.401', null);


-- customer
INSERT INTO "businessExpenses"."customer"("customerId", "code","name") VALUES
('businessExpenses_1NWxVU1vrm1zigoZ','ESGI','ESGI'),
('businessExpenses_1NWxX73HJbghcl7','MAIRIE_BREST','Mairie Brest'),
('businessExpenses_1NWxY7r_C1uIEL0','SAMSUNG','Samsung'),
('businessExpenses_1NWxYg1BdH1_WQCN','LA_POSTE','La poste');


-- project
INSERT INTO "businessExpenses"."project"("projectId", "customerId", "code", "name", "rebilling") VALUES
('businessExpenses_1NWxadIom1AWmCO','businessExpenses_1NWxX73HJbghcl7','BREST_NDF','Brest NDF',TRUE),
('businessExpenses_1NWxf21UVh-Cddf','businessExpenses_1NWxY7r_C1uIEL0','SAMSUNG_SAM1','SAMSUNG projet 1',TRUE),
('businessExpenses_1NWxfS18Xv1CA6ER','businessExpenses_1NWxVU1vrm1zigoZ','ESGI_MYGES','Myges',FALSE),
('businessExpenses_1NWxgG1Sod1rC_qQ','businessExpenses_1NWxVU1vrm1zigoZ','ESGI','Business Expenses ESGI',TRUE);

-- expenseReport
INSERT INTO "businessExpenses"."expenseReport" ("expenseReportId", "validatorId","accountantId","personalId","description","serviceId","status","validatorComment","beginDate","endDate","sendToValidationDate","validationDate","closingDate") VALUES
('businessExpenses_1NWwv32gQL1_AOOT','','','aorluc','Juillet 4-8 integration','INTEGRATION','inEdition','','2016-07-04 00:00:00','2016-07-08 00:00:00',null,null,null),
('businessExpenses_1NWwvl3n1WE38Kd','aragot','','aorluc','Juin 20-24 integration','INTEGRATION','inAccounting','''','2016-06-20 00:00:00','2016-06-24 00:00:00','2016-07-11 18:00:56','2016-07-11 18:02:03',null),
('businessExpenses_1NWx1B30o1KUKeK','','','aorluc','Juillet 4-8 support','SUPPORT','inEdition','','2016-07-04 00:00:00','2016-07-08 00:00:00',null,null,null),
('businessExpenses_1NWx3R3dN_1uQS4f','aragot','','aorluc','Juin 20-24 Support','SUPPORT','inValidation','','2016-06-20 00:00:00','2016-06-24 00:00:00','2016-07-11 18:01:23',null,null);


-- expenseLine
INSERT INTO "businessExpenses"."expenseLine" ("expenseLineId","expenseReportId","date","lineNumber","businessCategoryId","customerId","projectId","rebilling","partyName","proofReference","reason","amount","VatAmount","distance","taxHorsepower") VALUES
('businessExpenses_1NWxlM26MO1UVicf','businessExpenses_1NWx3R3dN_1uQS4f','2016-06-20 00:00:00',1,'RESTAURANT','businessExpenses_1NWxVU1vrm1zigoZ','businessExpenses_1NWxfS18Xv1CA6ER',FALSE,'Maison Doucet','ref_001','Entretiens avec Mr SANANES',20.8,null,0,0),
('businessExpenses_1NWxMD2_4b1Arixm','businessExpenses_1NWwv32gQL1_AOOT','2016-07-04 00:00:00',1,'DEPLACEMENT_PRO','','',FALSE,'','','Séminaire',43.44,null,80,5),
('businessExpenses_1NWxN02WgrBY11L','businessExpenses_1NWwv32gQL1_AOOT','2016-07-08 00:00:00',2,'HOTEL','','',FALSE,'Ibis','Ibis 1','Hotel séminaire',350,null,0,0),
('businessExpenses_1NWxom2-Va5tsXq','businessExpenses_1NWx3R3dN_1uQS4f','2016-06-23 00:00:00',2,'TAXI','businessExpenses_1NWxVU1vrm1zigoZ','businessExpenses_1NWxgG1Sod1rC_qQ',TRUE,'Taxi bleu','ref_002','Trajet Maarch - ESGI',40.1,null,0,0),
('businessExpenses_1NWy7t3WE3DmekQ','businessExpenses_1NWwvl3n1WE38Kd','2016-06-20 00:00:00',1,'DEPLACEMENT_PRO','businessExpenses_1NWxX73HJbghcl7','businessExpenses_1NWxadIom1AWmCO',TRUE,'','','Paris - Brest',374,null,550,6),
('businessExpenses_1NWy9c1DYpsp35-','businessExpenses_1NWwvl3n1WE38Kd','2016-06-24 00:00:00',2,'HOTEL','businessExpenses_1NWxX73HJbghcl7','businessExpenses_1NWxadIom1AWmCO',TRUE,'Sofitel','sofitel 554655','Semaine hotel brest',574.86,null,0,0),
('businessExpenses_1NWyAT1tTK_mXYp','businessExpenses_1NWwvl3n1WE38Kd','2016-06-23 00:00:00',3,'RESTAURANT','businessExpenses_1NWxX73HJbghcl7','businessExpenses_1NWxadIom1AWmCO',TRUE,'Restaurent du sofitel','sofitel 256489','resto ',60,null,0,0);

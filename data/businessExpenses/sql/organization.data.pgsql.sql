DELETE FROM "organization"."userFunction";
DELETE FROM "organization"."userPosition";
DELETE FROM "organization"."organization";
DELETE FROM "organization"."orgType";

-- Organization unit types
INSERT INTO "organization"."orgType"("code", "name") VALUES 
('Collectivité', 'Collectivité'),
('Société', 'Société'),
('Direction', 'Direction d''une entreprise ou d''une collectivité'),
('Service', 'Service d''une entreprise ou d''une collectivité'),
('Division', 'Division d''une entreprise');

-- Organizations
INSERT INTO "organization"."organization"( "orgId", "orgName", "otherOrgName", "displayName", "registrationNumber", "beginDate", "endDate", "legalClassification", "businessType", "description", "orgTypeCode", "orgRoleCodes", "taxIdentifier", "parentOrgId", "ownerOrgId", "isOrgUnit") VALUES 
('MAARCH_SAS','Maarch SAS','','Maarch','test123456789',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,FALSE),
('INTEGRATION','Intégration','','Intégration','integration',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MAARCH_NANTERRE','MAARCH_SAS',TRUE),
('MAARCH_NANTERRE','Maarch Nanterre','','Maarch Nanterre','Maarch_Nanterre',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MAARCH_SAS','MAARCH_SAS',TRUE),
('SUPPORT','Support','','Support','support','2016-03-01','2016-06-30',NULL,NULL,'Service support',NULL,NULL,NULL,'MAARCH_NANTERRE','MAARCH_SAS',TRUE);

-- User function
INSERT INTO "organization"."userFunction" ("userFunctionId", "reference", "name") VALUES 
('1N9BVc1pYn1kCAyh','VALIDATOR','Validateur'),
('1N9BVH2DL1I36PB','ACCOUNTANT','Comptable');



-- Organization user position
INSERT INTO "organization"."userPosition" ("userAccountId", "orgId", "function", "default") VALUES
('aorluc','SUPPORT','',TRUE),
('aorluc','INTEGRATION','',FALSE),
('aragot','MAARCH_NANTERRE','VALIDATOR',TRUE),
('ccotin','MAARCH_NANTERRE','ACCOUNTANT',TRUE);
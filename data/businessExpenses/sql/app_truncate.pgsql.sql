--schema lifeCycle
delete from "lifeCycle"."event";
delete from "lifeCycle"."journal";

-- schema audit
delete from "audit"."event";

-- schema busiessExpenses
delete from "busiessExpenses"."businessCategory";

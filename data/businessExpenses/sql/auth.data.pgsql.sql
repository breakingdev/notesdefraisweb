DELETE FROM "auth"."roleMember";
DELETE FROM "auth"."account";
DELETE FROM "auth"."accessRule";
DELETE FROM "auth"."privilege";
DELETE FROM "auth"."role";
DELETE FROM "auth"."publicUserStory";
DELETE FROM "auth"."ignoreClass";

INSERT INTO "auth"."account" ("accountType", "accountId", "lastName", "firstName", "title", "displayName", "accountName", "emailAddress", "password","enabled","passwordChangeRequired","passwordLastChange", "locked", "badPasswordCount","lastLogin","lastIp","replacingUserAccountId") VALUES
    ('user', 'superadmin', 'Super', 'Admin', 'M.', 'super admin', 'superadmin', 'info@maarch.org', '186cf774c97b60a1c106ef718d10970a6a06e06bef89553d9ae65d938a886eae',true,false,null,false,0,null,null,null),
    ('user', 'aragot', 'Ragot', 'Alexis', 'M.', 'Alexis RAGOT', 'aragot', 'info@businessExpenses.org', 'fffd2272074225feae229658e248b81529639e6199051abdeb49b6ed60adf13d',true,false,null,false,0,null,null,null),
    ('user', 'ccotin', 'COTIN', 'Carole', 'Mme.', 'Carole COTIN', 'ccotin', 'info@businessExpenses.org', 'fffd2272074225feae229658e248b81529639e6199051abdeb49b6ed60adf13d',true,false,null,false,0,null,null,null),
    ('user', 'aorluc', 'ORLUC', 'Alex', 'M.', 'Alex ORLUC', 'aorluc', 'info@businessExpenses.org', 'fffd2272074225feae229658e248b81529639e6199051abdeb49b6ed60adf13d',true,false,null,false,0,null,null,null);


-- ROLE
INSERT INTO "auth"."role"("roleId", "roleName", "description", "enabled") VALUES
    ('ADMIN', 'Administrateur', 'Groupe administrateur', true),
    ('UTILISATEUR', 'Utilisateur', 'Groupe utilisateur', true),
    ('COMPTABLE', 'Comptable', 'Groupe comptable', true),
    ('VALIDATEUR', 'Validateur', 'Groupe validateur', true);


-- privilege
INSERT INTO "auth"."privilege"("roleId", "userStory") VALUES
    ('ADMIN','adminFunc/adminAuth'),
    ('ADMIN','adminFunc/adminBusinessCategory'),
    ('ADMIN','adminFunc/adminCustomer'),
    ('ADMIN','adminFunc/adminMileageAllowanceScale'),
    ('ADMIN','adminFunc/adminOrganization'),
    ('ADMIN','adminFunc/adminProject'),
    ('ADMIN','adminFunc/adminUserFunction'),
    ('ADMIN','adminFunc/statistic'),
    ('UTILISATEUR','adminFunc/user'),
    ('VALIDATEUR','adminFunc/validator'),
    ('COMPTABLE','adminFunc/accountant');


-- publicUserStory
INSERT INTO "auth"."publicUserStory"("userStory") VALUES 
    ('app/*');


-- roleMember
INSERT INTO "auth"."roleMember"("roleId", "userAccountId") VALUES
    ('ADMIN', 'superadmin'),
    ('UTILISATEUR', 'aorluc'),
    ('VALIDATEUR', 'aragot'),
    ('COMPTABLE', 'ccotin');
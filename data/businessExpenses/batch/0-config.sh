#!/bin/bash

cd $SCRIPT_PATH

# Set environment/server variables
export LAABS_APP="maarchRM"
export LAABS_INSTANCE_NAME="maarchRM"
export LAABS_BUNDLES="recordsManagement;lifeCycle;medona;seda;archivesPubliques;financialRecords;digitalResource;audit;organization;auth;contact;documentManagement;businessRecords"
export LAABS_DEPENDENCIES="datasource;sdo;repository;fileSystem;xml"
export LAABS_BUFFER_MODE=1
export LAABS_PHP_INI="$SCRIPT_PATH/../conf/php_batch.ini"
export LAABS_CONTENT_TYPES="url:application/x-www-form-urlencoded;html:text/html,application/xhtml+xml;xml:application/xml;json:application/json,application/javascript;soap:application/soap+xml"
# export LAABS_CONTENT_LANGUAGES="fr:fr,fr-fr,fr-ca"
export LAABS_TMP_DIR="$SCRIPT_PATH/../tmp"
# export LAABS_LOG="$SCRIPT_PATH/../log.txt"
export LAABS_XML_NS="medona:org:afnor:medona:1.0;seda:fr:gouv:culture:archivesdefrance:seda:v1.0"
export LAABS_CRYPT_KEY="mySecretKey"
export LAABS_CRYPT_CIPHER=MCRYPT_BLOWFISH


# Change working directory to web root
cd ../../../web
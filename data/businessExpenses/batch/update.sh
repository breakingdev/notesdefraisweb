#!/bin/sh

SCRIPT_PATH=`dirname $(readlink -f "$0")`
cd $SCRIPT_PATH;

LAABS_PATH="$SCRIPT_PATH/../../../";

cd $LAABS_PATH;
svn up data/maarchRM/
svn up core/.
svn up dependency/*
svn up src/bundle/*
svn up src/presentation/maarchRM
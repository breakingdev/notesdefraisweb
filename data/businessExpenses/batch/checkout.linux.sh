#!/bin/sh

SCRIPT_PATH=`dirname $(readlink -f "$0")`

cd $SCRIPT_PATH

BUNDLE_PATH="$SCRIPT_PATH/../../../src/bundle/";
DEPENDENCY_PATH="$SCRIPT_PATH/../../../dependency/";
PRESENTATION_PATH="$SCRIPT_PATH/../../../src/presentation/";

# 1 - bundle
cd $BUNDLE_PATH
svn co http://svn.laabs-framework.in/bundle_archivesPubliques/trunk archivesPubliques
svn co http://svn.laabs-framework.in/bundle_audit/trunk audit
svn co http://svn.laabs-framework.in/bundle_auth/trunk auth
svn co http://svn.laabs-framework.in/bundle_businessRecords/trunk businessRecords
svn co http://svn.laabs-framework.in/bundle_contact/trunk contact
svn co http://svn.laabs-framework.in/bundle_digitalResource/trunk digitalResource
svn co http://svn.laabs-framework.in/bundle_documentManagement/trunk documentManagement
svn co http://svn.laabs-framework.in/bundle_financialRecords/trunk financialRecords
svn co http://svn.laabs-framework.in/bundle_lifeCycle/trunk lifeCycle
svn co http://svn.laabs-framework.in/bundle_medona/trunk medona
svn co http://svn.laabs-framework.in/bundle_organization/trunk organization
svn co http://svn.laabs-framework.in/bundle_recordsManagement/trunk recordsManagement
svn co http://svn.laabs-framework.in/bundle_seda/trunk seda

# 2 - dependency
cd $DEPENDENCY_PATH
svn co http://svn.laabs-framework.in/dependency_authentication/trunk authentication
svn co http://svn.laabs-framework.in/dependency_authorization/trunk authorization
svn co http://svn.laabs-framework.in/dependency_datasource/trunk datasource
svn co http://svn.laabs-framework.in/dependency_fileSystem/trunk fileSystem
svn co http://svn.laabs-framework.in/dependency_html/trunk html
svn co http://svn.laabs-framework.in/dependency_json/trunk json
svn co http://svn.laabs-framework.in/dependency_localisation/trunk localisation
svn co http://svn.laabs-framework.in/dependency_logger/trunk logger
svn co http://svn.laabs-framework.in/dependency_repository/trunk repository
svn co http://svn.laabs-framework.in/dependency_sdo/trunk sdo
svn co http://svn.laabs-framework.in/dependency_xml/trunk xml

# 3 - presentation
cd $PRESENTATION_PATH
svn co http://svn.laabs-framework.in/presentation_maarchRM/trunk maarchRM

# 4 - owner
chmod -R 775 "$SCRIPT_PATH/../../../"
chown -R www-data. "$SCRIPT_PATH/../../../"






#!/bin/sh

echo ""
echo ""
echo "batch test"
echo "--------------"
echo ""



SCRIPT_PATH=`dirname $(readlink -f "$0")`
cd $SCRIPT_PATH

BUNDLE_PATH="$SCRIPT_PATH/../../../src/bundle/";
DATA_SQL_PATH="$SCRIPT_PATH/../sql/";

# 1 - Recuperation params
read -p "Please enter the Postgresql port (default 5432) : " input_pgsql_port
read -p "Please enter the Postgresql database name (default maarchRM) : " input_pgsql_database

if test "$input_pgsql_port" = ""
then
  input_pgsql_port=5432
  echo "Default port value $input_pgsql_port is used"
fi

if test "$input_pgsql_database" = ""
then
  input_pgsql_database=maarchRM
  echo "Default name value $input_pgsql_database is used"
fi

# 1 - Creation des tables et schémas SQL
cd $BUNDLE_PATH
echo "$input_pgsql_port '$input_pgsql_database'"
echo '$input_pgsql_port "$input_pgsql_database"'
echo `$input_pgsql_port "$input_pgsql_database"`
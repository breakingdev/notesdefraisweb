#!/bin/sh

SCRIPT_PATH=`dirname $(readlink -f "$0")`;
cd $SCRIPT_PATH;

BUNDLE_PATH="$SCRIPT_PATH/../../../src/bundle/";
DATA_SQL_PATH="$SCRIPT_PATH/../sql/";

# 1 - Recuperation params
read -p "Please enter the Postgresql port (default 5432) : " input_pgsql_port
read -p "Please enter the Postgresql database name (default businessExpenses) : " input_pgsql_database

if test "$input_pgsql_port" = ""
then
  input_pgsql_port=5432;
  echo "Default port value $input_pgsql_port is used"
fi

if test "$input_pgsql_database" = ""
then
  input_pgsql_database=businessExpenses;
  echo "Default name value $input_pgsql_database is used"
fi

# 1 - Creation des tables et schémas SQL
cd $BUNDLE_PATH;
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < businessExpenses/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < auth/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < organization/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < audit/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < lifeCycle/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < contact/Resources/sql/schema.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < businessExpenses/Resources/sql/view.pgsql.sql";


# 2 - Mise à jour de la base de données avec les data de démo
cd $DATA_SQL_PATH;
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < auth.data.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < organization.data.pgsql.sql";
su postgres -c "psql -p $input_pgsql_port '$input_pgsql_database' < businessExpenses.data.pgsql.sql";

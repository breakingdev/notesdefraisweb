## Installation Business Expenses

---

### Informations
Installation réalisée sur un système GNU Linux Ubuntu 14.04 LTS avec posgreSQL 9.3.6
La plupart des commandes qui seront exécutées nécessiteront potentiellement des droits
administrateur sur le système.

---

### Prérequis

 - Navigateur internet HTML5 / CSS3
 - Version PHP  5.4 minimum
 - Extension PHP file info 
 - Extension PHP mcrypt
 - Extension PHP pdo
 - Extension PHP pdo_pgsql
 - Extension PHP xsl
 - Module Apache rewrite_module
 - Module Apache env_module
 - Postgres 9.3 ou supérieur

---

### Installation

#### Récupération des sources

---

### Administration de PostgreSQL
#### Création d'un utilisateur
(Optionnel si vous possédez déjà un utilisateur avec les droits pour gérer la base de données de l'application)

Se connecter en tant qu'utilisateur postgres (depuis l'utilisateur administrateur, aucune mot de passe est nécessaire) et executer la commande suivante :

    $ psql

#### Créer un utilisateur

    CREATE USER maarch;
    ALTER ROLE maarch WITH CREATEDB;
    ALTER ROLE maarch WITH SUPERUSER;
    ALTER USER maarch WITH ENCRYPTED PASSWORD 'mon mot de passe';
    CREATE DATABASE "nom de la BDD" WITH OWNER maarch;
    \q
    exit

#### Création des tables et schémas pour l'application Business Expenses
À partir du répertoire contenant les scripts développées pour l'application Business Expenses (*/var/www/laabs/data/maarchRM/batch), exécuter *checkout.linux.sh*.

    $ /bin/bash sql.linux.sh

---

### Configuration

#### Système

Ajouter dans le fichier hosts du système :
Pour l'exemple, l'adresse IP est en localhost. Le nom qui suit doit correspondre au paramètre "ServerName" du vhost. (voir le fichier "vhost.conf" dans le dossier de configurationl'application Maarch RM */var/www/laabs/data/maarchRM/conf*)

    127.0.0.1  maarchrm

#### Apache

Ajouter dans la configuration d'apache :

    # Application Maarch RM 
    Include /var/www/laabs/data/maarchRM/conf/vhost.conf

Relancer Apache :

    service apache2 restart

#### Application Maarch RM

Aller dans le répertoire de configuration de l'application Maarch RM

    cd /var/www/laabs/data/maarchRM/conf

Éditer le fichier de configuration "confvars.ini" et remplacer les paramètres par les bonnes valeurs

    @var.dsn = "pgsql:host=<adresse de la BDD>;dbname=<nom de la BDD>;port=<port de la BDD>" 
    @var.username = <utilisateur BDD> 
    @var.password = <mot de passe de l'utilisateur>
    @var.laabsDirectory = "<chemin absolu du framework Laabs>"

Éditer le fichier de configuration "vhost.conf" et remplacer les paramètres par les bonnes valeurs

    # Chemin vers le répertoire public web de Laabs
    DocumentRoot /var/www/laabs/web/
    # Nom du vhost (identique au nom associé à l'adresse IP dans le fichier host)
    ServerName maarchrm

Il ne faut pas oublié de rensigner les diverses autres valeurs si nécessaire.

---

### Connexion à l'application

Une fois toutes les opérations précédentes terminées, l'application est accessible depuis le navigateur internet.

    http://<Nom mis dans le fichier hosts et dans l'attribut ServerName du fichier vhost.conf>/

Avec notre exemple :

    http://maarchrm/

L'administrateur fonctionnel est 'superadmin', mot de passe 'superadmin'.

Tous les autres utilisateurs livrés dans les données d'exemple ont pour mot de passe par défaut 'maarch'.
DROP SCHEMA IF EXISTS "auth" CASCADE;
CREATE SCHEMA "auth";

-- Table: "auth"."role"
DROP TABLE IF EXISTS "auth"."role" CASCADE;
CREATE TABLE "auth"."role"
(
  "roleId" text,
  "roleName" text NOT NULL,
  "description" text,
  "enabled" boolean DEFAULT true,
  
  PRIMARY KEY ("roleId")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth".userAccount
DROP TABLE IF EXISTS "auth"."userAccount" CASCADE;
CREATE TABLE "auth"."userAccount"
(
  "userAccountId" text NOT NULL,
  "userName" text NOT NULL,
  "emailAddress" text NOT NULL,
  "password" text,
  "enabled" boolean DEFAULT true,
  "passwordChangeRequired" boolean DEFAULT true,
  "passwordLastChange" timestamp,
  "locked" boolean DEFAULT false,
  "lockDate" timestamp,
  "badPasswordCount" integer,
  "lastLogin" timestamp,
  "lastIp" text,
  "replacingUserAccountId" text,
  "firstName" text,
  "lastName" text,
  "title" text,
  "displayName" text NOT NULL,
  
  PRIMARY KEY ("userAccountId"),
  UNIQUE ("userName") 
)
WITH (
  OIDS=FALSE
);

-- Table: "auth".serviceAccount
DROP TABLE IF EXISTS "auth"."serviceAccount" CASCADE;
CREATE TABLE "auth"."serviceAccount"
(
  "serviceId" text NOT NULL,
  "serviceName" text NOT NULL,
  "enabled" boolean DEFAULT true,
  "salt" text,
  "tokenDate" timestamp,

  PRIMARY KEY ("serviceId"),
  UNIQUE ("serviceName")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth"."account"
DROP TABLE IF EXISTS "auth"."account" CASCADE;
CREATE TABLE "auth"."account"
(
  "accountId" text NOT NULL,
  "accountName" text NOT NULL,
  "displayName" text NOT NULL,
  "accountType" text DEFAULT 'user',
  "emailAddress" text NOT NULL,
  "enabled" boolean DEFAULT true,
  
  "password" text,
  "passwordChangeRequired" boolean DEFAULT true,
  "passwordLastChange" timestamp,
  "locked" boolean DEFAULT false,
  "lockDate" timestamp,
  "badPasswordCount" integer,
  "lastLogin" timestamp,
  "lastIp" text,
  "replacingUserAccountId" text,
  "firstName" text,
  "lastName" text,
  "title" text,
  
  "salt" text,
  "tokenDate" timestamp,
    
  PRIMARY KEY ("accountId"),
  UNIQUE ("accountName") 
)
WITH (
  OIDS=FALSE
);

-- Table: "auth"."roleMember"
DROP TABLE IF EXISTS "auth"."roleMember" CASCADE;
CREATE TABLE "auth"."roleMember"
(
  "roleId" text,
  "userAccountId" text NOT NULL,
  FOREIGN KEY ("roleId")
      REFERENCES "auth"."role" ("roleId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY ("userAccountId")
      REFERENCES "auth"."account" ("accountId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE ("roleId", "userAccountId")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth".privileges
DROP TABLE IF EXISTS "auth"."privilege" CASCADE;
CREATE TABLE "auth"."privilege"
(
  "roleId" text,
  "userStory" text,
  FOREIGN KEY ("roleId")
      REFERENCES "auth"."role" ("roleId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE ("roleId", "userStory")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth"."servicePrivilege"
DROP TABLE IF EXISTS "auth"."servicePrivilege" CASCADE;
CREATE TABLE "auth"."servicePrivilege"
(
  "accountId" text,
  "serviceURI" text,
  FOREIGN KEY ("accountId")
      REFERENCES auth.account ("accountId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE ("accountId", "serviceURI")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth".rules
DROP TABLE IF EXISTS "auth"."accessRule" CASCADE;
CREATE TABLE "auth"."accessRule"
(
  "roleId" text,
  "class" text NOT NULL,
  "context" text,
  FOREIGN KEY ("roleId")
      REFERENCES "auth"."role" ("roleId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE ("roleId", "class")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth"."publicUserStory"
DROP TABLE IF EXISTS "auth"."publicUserStory" CASCADE;
CREATE TABLE "auth"."publicUserStory"
(
  "userStory" text NOT NULL,

  PRIMARY KEY ("userStory")
)
WITH (
  OIDS=FALSE
);

-- Table: "auth"."ignoreClass"
DROP TABLE IF EXISTS "auth"."ignoreClass" CASCADE;
CREATE TABLE "auth"."ignoreClass"
(
  "class" text NOT NULL,
  
  PRIMARY KEY ("class")
)
WITH (
  OIDS=FALSE
);
<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle auth.
 *
 * Bundle auth is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle auth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle auth.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\auth\Controller;

/**
 * userAccount  controller
 *
 * @package Auth
 * @author  Alexandre Morin <alexandre.morin@maarch.org>
 */
class userAccount
{

    protected $sdoFactory;
    protected $passwordEncryption;
    protected $securityPolicy;
    protected $currentAccount;
    protected $accountPrivileges;
    protected $publicUserStoriesController;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory         The dependency Sdo Factory object
     * @param string                  $passwordEncryption The password encryption algorythm
     * @param array                   $securityPolicy     The array of security policy parameters
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null, $passwordEncryption = 'md5', $securityPolicy)
    {
        $this->sdoFactory = $sdoFactory;
        $this->passwordEncryption = $passwordEncryption;
        $this->securityPolicy = $securityPolicy;
        $this->currentAccount = \laabs::getToken('AUTH');

        $this->publicUserStoriesController = \laabs::newController("auth/publicUserStory");

    }

    /**
     * List all users to display
     * @param string $query
     *
     * @return Array The array of stdClass with dislpay name and user identifier
     */
    public function index($query = null)
    {
        if ($query) {
            $query .= "AND accountType='user'";
        } else {
            $query .= "accountType='user'";
        }

        $userAccounts = $this->sdoFactory->find('auth/account', $query);
        $userAccounts = \laabs::castMessageCollection($userAccounts, 'auth/userAccountIndex');

        return $userAccounts;
    }

    /**
     * List all users to display
     *
     * @return Array The array of stdClass
     */
    public function userList()
    {
        $users = array();

        $userAccounts = $this->sdoFactory->find('auth/account', "accountType='user'");
        
        return $userAccounts;
    }

    /**
     * List all users to display
     * @param string $query
     *
     * @return Array The array of stdClass with dislpay name and user identifier
     */
    public function search($query = null)
    {
        if ($query) {
            $query .= "AND accountType='user'";
        } else {
            $query .= "accountType='user'";
        }

        $userAccounts = $this->sdoFactory->find('auth/account', $query);

        return $userAccounts;
    }

    /**
     *  Prepare an empty user object
     *
     * @return auth/account The user object
     */
    public function newUser()
    {
        return \laabs::newInstance('auth/account');
    }

    /**
     * Record a new user & role members
     * @param auth/account $userAccount The user object
     */
    public function add($userAccount)
    {
        $userAccountId = $this->addUserAccount($userAccount);

        if (is_array($userAccount->roles)) {
            foreach ($userAccount->roles as $roleId) {
                $member = \laabs::callService("auth/roleMember/create", $roleId, $userAccountId);
            }
        }
    }

    /**
     * Record a new user account
     * @param auth/account $userAccount The user object
     *
     * @return auth/account The user object
     */
    public function addUserAccount($userAccount)
    {
        $userAccount->accountId = \laabs::newId();

        if ($this->sdoFactory->exists('auth/account', array('accountName' => $userAccount->accountName))) {
            throw \laabs::newException("auth/userAlreadyExistException");
        }

        if (!\laabs::validate($userAccount, 'auth/account')) {
            $validationErrors = \laabs::getValidationErrors();
            throw \laabs::newException("auth/invalidUserInformationException", $validationErrors);
        }

        $encryptedPassword = $userAccount->password;
        if ($this->passwordEncryption != null) {
            $encryptedPassword = hash($this->passwordEncryption, $userAccount->password);
        }

        $userAccount->password = $encryptedPassword;
        $userAccount->passwordChangeRequired = true;
        $userAccount->passwordLastChange = \laabs::newDate();
        $userAccount->badPasswordCount = 0;
        $userAccount->lastLogin = null;
        $userAccount->lastIp = null;

        $this->sdoFactory->create($userAccount, 'auth/account');

        return $userAccount->accountId;
    }

    /**
     * Prepare a user object for update
     * @param id $userAccountId The user unique identifier
     *
     * @return auth/account The user object
     */
    public function edit($userAccountId)
    {
        $userAccount = $this->sdoFactory->read('auth/account', $userAccountId);
        $roleMembers = $this->sdoFactory->find("auth/roleMember", "userAccountId='$userAccountId'");

        $userAccount = \laabs::castMessage($userAccount, 'auth/userAccount');
        foreach ($roleMembers as $roleMember) {
            $role = \laabs::callService('auth/role/read_roleId_', $roleMember->roleId);
            $userRole = \laabs::newMessage('auth/userRole');
            $userRole->roleId = $role->roleId;
            $userRole->roleName = $role->roleName;

            $userAccount->roles[] = $userRole;
        }

        return $userAccount;
    }

    /**
     * Modify a  user & role members
     * @param string           $userAccountId The user account id
     * @param auth/account $userAccount   The user object
     */
    public function update($userAccountId, $userAccount)
    {
        $userAccount = $this->updateUserInformation($userAccount);

        $this->sdoFactory->deleteChildren("auth/roleMember", $userAccount, "auth/account");

        if (is_array($userAccount->roles)) {
            foreach ($userAccount->roles as $roleId) {
                \laabs::callService("auth/roleMember/create", $roleId, $userAccount->accountId);
            }
        }
    }

    /**
     * Modify userAccount information
     * @param auth/accountInformation $userAccount The user object
     *
     * @return boolean The result of the request
     */
    public function updateUserInformation($userAccount = null)
    {
        if (!$this->sdoFactory->exists('auth/account', array('accountId' => $userAccount->accountId))) {
            throw \laabs::newException("auth/unknownUserException");
        }

        $this->sdoFactory->update($userAccount, "auth/account");

        return $userAccount;
    }

    /**
     * get user account information
     * @param id $userAccountId The user account identifier
     *
     * @return auth/accountInformation $userAccount User account information object
     */
    public function getUserAccountInformation($userAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/accountInformation", $userAccountId);

        return $userAccount;
    }

    /**
     * Change a user password
     * @param string $userAccountId The identifier of the user
     * @param string $newPassword   The new password
     *
     * @return boolean The result of the request
     */
    public function setPassword($userAccountId, $newPassword)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);

        $encryptedPassword = $newPassword;
        if ($this->passwordEncryption != null) {
            $encryptedPassword = hash($this->passwordEncryption, $newPassword);
        }

        $userAccount->password = $encryptedPassword;
        $userAccount->accountId = $userAccountId;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Required password change
     * @param string $userAccountId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function requirePasswordChange($userAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);
        $userAccount->badPassword = 0;
        $userAccount->passwordChangeRequired = true;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Lock a user
     * @param string $userAccountId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function lock($userAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);
        $userAccount->locked = true;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Unlock a user
     * @param string $userAccountId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function unlock($userAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);
        $userAccount->locked = false;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Enable a user
     * @param string $userAccountId The identifier of the user
     *
     * @return boolean The result of the request
     */
    public function enable($userAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);
        $userAccount->enabled = true;
        $userAccount->replacingUserAccountId = null;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Disable a user
     * @param string $userAccountId          The identifier of the user
     * @param string $replacingUserAccountId The identifier of the replacing user
     *
     * @return boolean The result of the request
     */
    public function disable($userAccountId, $replacingUserAccountId)
    {
        $userAccount = $this->sdoFactory->read("auth/account", $userAccountId);
        $userAccount->enabled = false;
        $userAccount->replacingUserAccountId = $replacingUserAccountId;

        return $this->sdoFactory->update($userAccount);
    }

    /**
     * Get list of user story
     * @param string $userAccountId The identifier of the user account
     *
     * @return array The user story names
     */
    public function getPrivilege($userAccountId)
    {
        $userAccountId = (string) $userAccountId;
        if (!isset($this->accountPrivileges[$userAccountId])) {

            $roleMemberController = \laabs::newController("auth/roleMember");
            $roles = $roleMemberController->readByUseraccount($userAccountId);

            $userStories = $this->publicUserStoriesController->index();

            foreach ($roles as $role) {
                $privileges = $this->sdoFactory->find("auth/privilege", "roleId='$role->roleId'");

                foreach ($privileges as $privilege) {
                    $userStories[] = $privilege->userStory;
                }
            }

            $this->accountPrivileges[$userAccountId] = array_unique($userStories);
        }

        return $this->accountPrivileges[$userAccountId];
    }

    /**
     * Check the user account has a privilege
     * @param string $userStory The user story name
     *
     * @return boolean
     */
    public function hasPrivilege($userStory)
    {
        $userAccount = $this->currentAccount;

        if (!$userAccount) {
            return $this->publicUserStoriesController->index();
        }

        $userPrivileges = $this->getPrivilege($userAccount->accountId);
        foreach ($userPrivileges as $userPrivilege) {
            if (fnmatch($userPrivilege, $userStory)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get list of access rules
     * @param qname $objectClass The class name
     *
     * @return string The lql qtring
     */
    public function getAccessRule($objectClass)
    {
        $userAccount = $this->currentAccount;
        if (!$userAccount) {
            return false;
        }

        $roleController = \laabs::newController("auth/roleMember");
        $roles = $roleController->readByUserAccount($userAccount->accountId);

        $accessRules = array();
        $contexts = array();
        foreach ($roles as $role) {
            foreach ($this->sdoFactory->find("auth/accessRule", "roleId='$role->roleId'") as $accessRule) {
                if (empty($accessRule->class)) {
                    return "true";
                }

                if (fnmatch($accessRule->class, $objectClass)) {
                    $accessRules[] = $accessRule;
                    if (empty($accessRule->context)) {
                        return "true";
                    } else {
                        $contexts[] = $accessRule->context;
                    }
                }
            }
        }

        if (count($accessRules) == 0) {
            return "false";
        }

        if (count($contexts) == 0) {
            return "true";
        }

        return "(".\laabs\implode(") OR (", $contexts).")";
    }

    /**
     * Search user account
     * @param string $query The query
     *
     * @return array The list of fouded users
     */
    public function queryUserAccounts($query = false)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $userAccountQueryProperties = array("displayName");
        $userAccountQueryPredicats = array();
        foreach ($userAccountQueryProperties as $userAccountQueryProperty) {
            foreach ($queryTokens as $queryToken) {
                $userAccountQueryPredicats[] = $userAccountQueryProperty."="."'*".$queryToken."*'";
            }
        }
        $userAccountQueryString = implode(" OR ", $userAccountQueryPredicats);
        if (!$userAccountQueryString) {
            $userAccountQueryString = "1=1";
        }
        $userAccountQueryString .= "(" . $userAccountQueryString . ") AND accountType='user'";

        $userAccounts = $this->sdoFactory->find('auth/account', $userAccountQueryString);

        return $userAccounts;
    }
}

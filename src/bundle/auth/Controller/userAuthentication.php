<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle user.
 *
 * Bundle user is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle user is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle user.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\auth\Controller;

/**
 * user authentication controller
 *
 * @package Auth
 * @author  Cyril VAZQUEZ <cyril.vazquez@maarch.org>
 */
class userAuthentication
{
    /**
     * The password encryption
     *
     * @var string
     **/
    protected $passwordEncryption;

    /**
     * The security policy of the password
     *
     * @var string
     **/
    protected $securityPolicy;

    /**
     * Constructor
     * @param object $sdoFactory         The user model
     * @param string $passwordEncryption The password encryption
     * @param array  $securityPolicy     The security policy
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, $passwordEncryption, $securityPolicy)
    {
        $this->sdoFactory = $sdoFactory;
        $this->passwordEncryption = $passwordEncryption;
        $this->securityPolicy = $securityPolicy;
    }

    /**
     * authenticate a user
     * @param string $userName The user name
     * @param string $password The user password
     *
     * @return bool
     */
    public function login($userName, $password)
    {
        // Check userAccount exists
        $currentDate = \laabs::newTimestamp();

        $exists = $this->sdoFactory->exists('auth/account', array('accountName'=>$userName));

        if (!$exists) {
            throw \laabs::newException('auth/authenticationException', 'Connection failure, invalid username or password.');
        }
        
        $userAccount = $this->sdoFactory->read('auth/account', array('accountName' => $userName));

        // Create user login object
        $userLogin = \laabs::newInstance('auth/userLogin');
        $userLogin->accountId = $userAccount->accountId;
        $userLogin->lastIp = $_SERVER["REMOTE_ADDR"];

        // Hash password
        $encryptedPassword = $password;
        if ($this->passwordEncryption != null) {
            $encryptedPassword = hash($this->passwordEncryption, $password);
        }

        // Check enabled
        if ($userAccount->enabled != true) {
            throw \laabs::newException('auth/authenticationException', "Connection failure, user ".$userName." is disabled");
        }
        
        // Check locked
        if ($userAccount->locked == true) {
            if (!isset($this->securityPolicy['lockDelay']) // No delay while locked
                || $this->securityPolicy['lockDelay'] == 0 // Unlimited delay
                || !isset($userAccount->lockDate)          // Delay but no date for lock so unlimited
                || $currentDate->diff($userAccount->lockDate)->s < $this->securityPolicy['lockDelay'] // Date + delay upper than current date
            ) {
                throw \laabs::newException('auth/authenticationException', "Connection failure, user ".$userName." is locked");
            }
        }

        // Check password
        if ($userAccount->password !== $encryptedPassword) {
            // Update bad password count
            $userLogin->badPasswordCount = $userAccount->badPasswordCount + 1;

            // If count exceeds max attemps, lock user
            if ($this->securityPolicy['loginAttempts'] && $userLogin->badPasswordCount > $this->securityPolicy['loginAttempts'] - 1) {
                $userLogin->locked = true;
                $userLogin->lockDate = $currentDate;
            }
            $this->sdoFactory->update($userLogin, 'auth/account');

            throw \laabs::newException('auth/authenticationException', 'Connection failure, invalid username or password.');
        }

        // Login success, update user account values
        $userLogin->badPasswordCount = 0;
        $userLogin->locked = false;
        $userLogin->lockDate = null;
        $userLogin->lastLogin = $currentDate;

        $this->sdoFactory->update($userLogin, 'auth/account');

        if (isset($this->securityPolicy['sessionTimeout'])) {
            $tokenDuration = $this->securityPolicy['sessionTimeout'];
        } else {
            $tokenDuration = 86400;
        }

        \laabs::setToken("AUTH", $userAccount, $tokenDuration);

        // Check password validity
        if ($this->securityPolicy['passwordValidity']
            && $currentDate->diff($userAccount->passwordLastChange)->days > $this->securityPolicy['passwordValidity']
        ) {
            throw \laabs::newException('auth/userPasswordChangeRequestException');
        }

        if ($userAccount->passwordChangeRequired == true) {
            throw \laabs::newException('auth/userPasswordChangeRequestException');
        }

        return $userAccount;
    }

    /**
     * Get form to edit user information
     * @param string $userName    The user's name
     * @param string $oldPassword The user's old password
     * @param string $newPassword The user's new password
     * @param string $requestPath The requested path
     *
     * @return boolean
     */
    public function definePassword($userName, $oldPassword, $newPassword, $requestPath)
    {
        if ($userAccount = $this->sdoFactory->read('auth/account', array('accountName' => $userName))) {
            //validation of security policy
            if ($this->securityPolicy['passwordMinLength'] && strlen($newPassword) < $this->securityPolicy['passwordMinLength']) {
                throw \laabs::newException('auth\invalidPasswordException', "The password is to short.");
            }
            if ($this->securityPolicy['passwordRequiresSpecialChars'] && !ctype_alnum($newPassword)) {
                throw \laabs::newException('auth\invalidPasswordException', "The password must contain special characters.");
            }
            if ($this->securityPolicy['passwordRequiresDigits'] && preg_match('/.*\d.*', $newPassword)) {
                throw \laabs::newException('auth\invalidPasswordException', "The password must contain digits.");
            }
            if ($this->securityPolicy['passwordRequiresMixedCase'] && !preg_match('^(?=.*[a-z])(?=.*[A-Z]).+$', $newPassword)) {
                throw \laabs::newException('auth\invalidPasswordException', "The password must contain upper and lower case characters");
            }


            $encryptedPassword = $newPassword;
            if ($this->passwordEncryption != null) {
                $encryptedPassword = hash($this->passwordEncryption, $newPassword);
            }
            if ($userAccount->password == $encryptedPassword) {
                throw \laabs::newException("auth/samePasswordException", "The password is the same as the precedent.");
            }

            $userAccount->password = $encryptedPassword;
            $userAccount->passwordLastChange = \laabs::newTimestamp();
            $userAccount->passwordChangeRequired = false;
            $this->sdoFactory->update($userAccount, 'auth/account');

            $this->login($userAccount->accountName, $newPassword, $requestPath);

            return $requestPath;
        }

        return false;
    }

    /**
     * Log out a user
     *
     * @return bool
     */
    public function logout()
    {
        \laabs::clearTokens();
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle organization.
 *
 * Bundle organization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle organization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle organization.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\organization\Controller;

/**
 * Control of the organization
 *
 * @package Organization
 * @author  Prosper De Laure <prosper.delaure@maarch.org> 
 */
class organization
{
    protected $sdoFactory;

    /**
     * Constructor
     * @param object $sdoFactory The model for organization
     *
     * @return void
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        
        $this->sdoFactory = $sdoFactory;

    }

    /**
     * Index of organizations
     * @param string $query The query of the index
     *
     * @return array An array of organization
     */
    public function index($query=null)
    {
        return $this->sdoFactory->index("organization/organization", array("orgId", "displayName", "isOrgUnit"), $query);
    }

    /**
     * Get organizations tree
     *
     * @return organization/organizationTree[] The tree of organization
     */
    public function getTree()
    {
        $tree = array();

        $organizationList = $this->sdoFactory->find("organization/organization");
        $organizationList = \laabs::castMessageCollection($organizationList, "organization/organizationTree");

        $userPositionList = $this->sdoFactory->find("organization/userPosition");
        $userPositionList = \laabs::castMessageCollection($userPositionList, "organization/userPositionTree");

        $servicePositionList = $this->sdoFactory->find("organization/servicePosition");
        $servicePositionList = \laabs::castMessageCollection($servicePositionList, "organization/servicePositionTree");

        $contactPositionList = $this->sdoFactory->find("organization/orgContact");
        $contactPositionList = \laabs::castMessageCollection($contactPositionList, "organization/contactPositionTree");

        $userAccountController = \laabs::newController('auth/userAccount');
        $serviceAccountController = \laabs::newController('auth/serviceAccount');
        $contactController = \laabs::newController('contact/contact');

        // sort organization by parentOrgId
        $organizationByParent = array();
        foreach ($organizationList as $organization) {
            $parentOrgId = (string) $organization->parentOrgId;

            if (empty($parentOrgId)) {
                $tree[] = $organization;
                continue;
            }
            if (!isset($organizationByParent[$parentOrgId])) {
                $organizationByParent[$parentOrgId] = array();
            }
            $organizationByParent[$parentOrgId][] = $organization;
        }

        // sort userPosition by orgId
        $userPositionByParent = array();
        foreach ($userPositionList as $userPosition) {
            $orgId = (string) $userPosition->orgId;

            if (!isset($userPositionByParent[$orgId])) {
                $userPositionByParent[$orgId] = array();
            }
            $userPosition->displayName = $userAccountController->edit((string) $userPosition->userAccountId)->displayName;
            $userPositionByParent[$orgId][] = $userPosition;
        }

        // sort servicePosition by orgId
        $servicePositionByParent = array();
        foreach ($servicePositionList as $servicePosition) {
            $orgId = (string) $servicePosition->orgId;

            if (!isset($servicePositionByParent[$orgId])) {
                $servicePositionByParent[$orgId] = array();
            }
            $servicePosition->displayName = $serviceAccountController->edit((string) $servicePosition->serviceAccountId)->accountName;
            $servicePositionByParent[$orgId][] = $servicePosition;
        }

        // sort contact by orgId
        $contactPositionByParent = array();
        foreach ($contactPositionList as $contactPosition) {
            $orgId = (string) $contactPosition->orgId;

            if (!isset($contactPositionByParent[$orgId])) {
                $contactPositionByParent[$orgId] = array();
            }
            $contactPosition->displayName = $contactController->get((string) $contactPosition->contactId)->displayName;
            $contactPositionByParent[$orgId][] = $contactPosition;
        }

        return $this->buildTree($tree, $organizationByParent, $userPositionByParent, $servicePositionByParent, $contactPositionByParent);
    }

    /**
     * Set positions for the organization tree
     * @param object $roots               The organization tree roots
     * @param array  $organizationList    The list of organization sorted by parent organization
     * @param array  $userPositionList    The list of userPositions sorted by parent organization
     * @param array  $servicePositionList The list of service position sorted by parent organization
     * @param array  $contactPositionList The list of contact position sorted by parent organization
     *
     */
    protected function buildTree($roots, $organizationList, $userPositionList, $servicePositionList, $contactPositionList)
    {

        foreach ($roots as $organization) {
            $orgId = (string) $organization->orgId;

            if (isset($organizationList[$orgId])) {
                $organization->organization = $this->buildTree($organizationList[$orgId], $organizationList, $userPositionList, $servicePositionList, $contactPositionList);
            }

            if (isset($userPositionList[$orgId])) {
                $organization->userPosition = $userPositionList[$orgId];
            }
            if (isset($servicePositionList[$orgId])) {
                $organization->servicePosition = $servicePositionList[$orgId];
            }
            if (isset($contactPositionList[$orgId])) {
                $organization->orgContact = $contactPositionList[$orgId];
            }
        }

        return $roots;
    }

    /**
     * Search organizations
     * @param string $name
     * @param string $businessType
     * @param string $orgRoleCode
     * @param string $orgTypeCode
     * @param string $registrationNumber
     * @param string $taxIdentifier
     *
     * @return organization/organization[] An array of organizations
     */
    public function search($name=null, $businessType=null, $orgRoleCode=null, $orgTypeCode=null, $registrationNumber=null, $taxIdentifier=null)
    {
        $queryParts = array();
        $variables = array();
        $query = null;


        if ($name) {
            $variables['name'] = "*$name*";
            $queryParts[] = "(orgName~:name OR otherOrgName~:name OR displayName~:name)";
        }

        if ($businessType) {
            $variables['businessType'] = "*$businessType*";
            $queryParts[] = "(businessType~:businessType)";
        }

        if ($orgRoleCode) {
            $variables['orgRoleCodes'] = "*$orgRoleCode*";
            $queryParts[] = "(orgRoleCodes~:orgRoleCodes)";
        }

        if ($orgTypeCode) {
            $variables['orgTypeCode'] = "*$orgTypeCode*";
            $queryParts[] = "(orgTypeCode='$orgTypeCode')";
        }

        if ($registrationNumber) {
            $variables['registrationNumber'] = $registrationNumber;
            $queryParts[] = "(registrationNumber='$registrationNumber')";
        }
        if ($taxIdentifier) {
            $variables['taxIdentifier'] = $taxIdentifier;
            $queryParts[] = "(taxIdentifier='$taxIdentifier')";
        }

        if (count($queryParts)) {
            $query = array(implode(" AND ", $queryParts), $variables);
        }

        return $this->sdoFactory->find("organization/organization", $query);
    }

    /**
     * Create an organization
     * @param organization/organization $organization The organization object to create
     *
     * @return string the new organization's Id
     */
    public function create($organization)
    {
        try {
            if (empty($organization->displayName)) {
                $organization->displayName = $organization->orgName;
            }

            $organization->orgId = \laabs::newId();
            $this->sdoFactory->create($organization, 'organization/organization');
        } catch (\Exception $e) {
            throw new \core\Exception("Key already exists");

        }

        return $organization->orgId;
    }

    /**
     * Read an organization by his orgId
     * @param string $orgId The Identifier of the organization to read
     *
     * @return organization/organization the organization
     */
    public function read($orgId)
    {
        $organization = $this->sdoFactory->read("organization/organization", $orgId);

        return \laabs::castMessage($organization, "organization/organization");
    }

    /**
     * Get an organization by his regitration number
     * @param string $registrationNumber The registration number of the organization
     *
     * @return organization/organization the organization
     */
    public function getOrgByRegNumber($registrationNumber)
    {
        $organization = $this->sdoFactory->read("organization/organization", array('registrationNumber' => $registrationNumber));

        return \laabs::castMessage($organization, "organization/organization");
    }

    /**
     * Get organizations by their role
     * @param string $role The role of the organizations
     *
     * @return organization/organization[] the organizations
     */
    public function getOrgsByRole($role)
    {
        $organizations =  $this->sdoFactory->find("organization/organization", "orgRoleCodes = '*$role*'");

        return \laabs::castMessageCollection($organizations, "organization/organization");
    }

    /**
     * List organisations
     * @param string $role The role of organizations
     *
     * @return array The organizations list
     */
    public function orgList($role = null)
    {
        $queryString = "";

        if ($role) {
            $queryString .= "orgRoleCodes = '*$role*'";
        }

        return $this->sdoFactory->index("organization/organization", array("displayName", "registrationNumber"), $queryString, null, array('registrationNumber'));
    }

    /**
     * Get organization's user positions
     * @param string $orgId The organization's identifier
     *
     * @return organization/userPositionTree[] The list of user position
     */
    public function readUserPositions($orgId)
    {
        $users = $this->sdoFactory->find("organization/userPosition", "orgId = '$orgId'");
        $users = \laabs::castMessageCollection($users, 'organization/userPositionTree');

        $userAccountController = \laabs::newController('auth/userAccount');
        $userFunctionController = \laabs::newController("organization/userFunction");

        foreach ($users as $user) {
            $user->displayName = $userAccountController->edit((string) $user->userAccountId)->displayName;
            if (!empty($user->function)) {
                $user->function = $userFunctionController->readByReference($user->function)->name;
            }
        }

        return $users;
    }

    /**
     * Get organization's service positions
     * @param string $orgId The organization's identifier
     *
     * @return organization/servicePosition[] The list of service position
     */
    public function readServicePositions($orgId)
    {
        return $this->sdoFactory->find("organization/servicePosition", "orgId = '$orgId'");
    }

    /**
     * Check if an organization has a given role
     * @param string $registrationNumber The organization identifier
     * @param string $role               The role
     *
     * @return boolean The result of the operation
     */
    public function hasRole($registrationNumber, $role)
    {
        $organization = $this->sdoFactory->find('organization/organization', "registrationNumber = '$registrationNumber' AND orgRoleCodes = '*$role*'");

        return (bool) count($organization);
    }

    /**
     * Update an organization
     * @param string                    $orgId        The organization identifier
     * @param organization/organization $organization The organization object to update
     *
     * @return boolean The result of the operation
     */
    public function update($orgId, $organization)
    {
        $organization->orgId = $orgId;

        if (empty($organization->displayName)) {
            $organization->displayName = $organization->orgName;
        }

        return $this->sdoFactory->update($organization, 'organization/organization');
    }

    /**
     * Move an organization to a new ownerOrg
     * @param string $orgId          The organization identifier
     * @param string $newParentOrgId The new parent organization identifier
     * @param string $newOwnerOrgId  The new owner organization identifier
     *
     * @return boolean The result of the operation
     */
    public function move($orgId, $newParentOrgId = null, $newOwnerOrgId = null)
    {
        if ($newParentOrgId == "") {
            $newParentOrgId = null;
        }
        if ($newOwnerOrgId == "") {
            $newOwnerOrgId = null;
        }

        $organization = $this->sdoFactory->read("organization/organization", $orgId);
        $descendants = $this->sdoFactory->readDescendants("organization/organization", $organization);

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $organization->parentOrgId = $newParentOrgId;
            $organization->ownerOrgId = $newOwnerOrgId;

            if (!$newOwnerOrgId) {
                $newOwnerOrgId = $orgId;
            }

            foreach ($descendants as $descendantOrg) {
                if((string) $descendantOrg->orgId == $organization->orgId) {
                    throw new \bunble\organization\Exception\orgMoveException("Organization can't be moved to a descendent organization");
                }

                if ($descendantOrg->isOrgUnit && $descendantOrg->ownerOrgId != $newOwnerOrgId) {
                    $descendantOrg->ownerOrgId = $newOwnerOrgId;
                    $this->sdoFactory->update($descendantOrg, 'organization/organization');
                }
            }

            $this->sdoFactory->update($organization);
        } catch (\Exception $e) {
            throw $e;
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }

    /**
     * Delete an organization
     * @param string $orgId The organization id
     *
     * @return boolean The restult of the operation
     */
    public function delete($orgId)
    {
        $organization = $this->sdoFactory->read("organization/organization", $orgId);
        $children = $this->sdoFactory->readChildren("organization/organization", $organization);
        $users = $this->sdoFactory->readChildren("organization/userPosition", $organization);
        $services = $this->sdoFactory->readChildren("organization/servicePosition", $organization);
        $contacts = $this->sdoFactory->readChildren("organization/orgContact", $organization);

        foreach ($children as $child) {
            $this->delete($child);
        }
        foreach ($users as $user) {
            $result = $this->sdoFactory->delete($user);
        }
        foreach ($services as $service) {
            $result = $this->sdoFactory->delete($service);
        }
        foreach ($contacts as $contact) {
            $result = $this->sdoFactory->delete($contact);
        }

        $result = $this->sdoFactory->delete($organization);

        return $result;
    }

    /**
     * Add a user position to an organization
     * @param string $userAccountId The user account identifier
     * @param string $orgId         The organization identifier
     * @param string $function      The function of the user
     *
     * @return boolean The result of the operation
     */
    public function addUserPosition($userAccountId, $orgId, $function = null)
    {
        $userPosition = \laabs::newInstance('organization/userPosition');
        $userPosition->userAccountId = $userAccountId;
        $userPosition->orgId = $orgId;
        $userPosition->function = $function;
        $userPosition->default = false;
        $userDefaultPosition = $this->sdoFactory->find('organization/userPosition', "userAccountId = '$userAccountId' AND default = true");

        if (empty($userDefaultPosition)) {
            $userPosition->default = true;
        }
        
        return $this->sdoFactory->create($userPosition, 'organization/userPosition');
    }

    /**
     * Add a service position to an organization
     * @param string $serviceAccountId The service account identifier
     * @param string $orgId            The organization identifier
     *
     * @return boolean The result of the operation
     */
    public function addServicePosition($orgId, $serviceAccountId)
    {
        $servicePosition = \laabs::newInstance('organization/servicePosition');
        $servicePosition->serviceAccountId = $serviceAccountId;
        $servicePosition->orgId = $orgId;

        return $this->sdoFactory->create($servicePosition);
    }

    /**
     * Set default user position for an user
     * @param string $orgId         The organization identifier
     * @param string $userAccountId The service account identifier
     *
     * @return boolean The result of the operation
     */
    public function setDefaultUserPosition($orgId, $userAccountId)
    {
        $previousDefaultUserPosition = $this->sdoFactory->find('organization/userPosition', "userAccountId='$userAccountId' AND default=true");

        if (!empty($previousDefaultUserPosition)) {
            $previousDefaultUserPosition = $previousDefaultUserPosition[0];
            $previousDefaultUserPosition->default = false;
            $this->sdoFactory->update($previousDefaultUserPosition, 'organization/userPosition');
        }

        $userPosition = $this->sdoFactory->read("organization/userPosition", array("userAccountId" => $userAccountId, "orgId" => $orgId));
        $userPosition->default = true;

        return $this->sdoFactory->update($userPosition, 'organization/userPosition');
    }

    /**
     * Remove a user's position
     * @param string $userAccountId The user account identifier
     * @param string $orgId         The organization account identifier
     *
     * @return boolean
     */
    public function deleteUserPosition($userAccountId, $orgId)
    {
        $userPosition = $this->sdoFactory->read("organization/userPosition", array("userAccountId" => $userAccountId, "orgId" => $orgId));

        if ($userPosition->default) {
            $newDefaultUserPosition = $this->sdoFactory->find('organization/userPosition', "userAccountId='$userAccountId'");

            if (!empty($newDefaultUserPosition)) {
                $newDefaultUserPosition = $newDefaultUserPosition[0];
                $newDefaultUserPosition->default = true;

                $this->sdoFactory->update($newDefaultUserPosition, 'organization/userPosition');
            }
        }

        return $this->sdoFactory->delete($userPosition);
    }

    /**
     * Remove a contact's position
     * @param string $contactId The user account identifier
     * @param string $orgId     The organization account identifier
     *
     * @return boolean
     */
    public function deleteContactPosition($contactId, $orgId)
    {
        $contactPosition = $this->sdoFactory->read("organization/orgContact", array("contactId" => $contactId, "orgId" => $orgId));

        return $this->sdoFactory->delete($contactPosition);
    }

    /**
     * Remove a service's position
     * @param string $orgId            The organization account identifier
     * @param string $serviceAccountId The service account identifier
     *
     * @return boolean
     */
    public function deleteServicePosition($orgId, $serviceAccountId)
    {
        $servicePosition = $this->sdoFactory->read("organization/servicePosition", array("serviceAccountId" => $serviceAccountId, "orgId" => $orgId));

        return $this->sdoFactory->delete($servicePosition);
    }

    /**
     * Get organization contacts
     * @param id $orgId
     *
     * @return contact/contact[]
     */
    public function getContacts($orgId)
    {
        $orgContacts = $this->sdoFactory->find('organization/orgContact', "orgId='$orgId'");

        $contacts = array();
        foreach ($orgContacts as $orgContact) {
            $contact = \laabs::callService('contact/contact/read_contactId_', $orgContact->contactId);
            $contacts[] = (object) array_merge((array) $contact, (array) $orgContact);
        }

        return $contacts;
    }

    /**
     * Bind a contact to an org
     * @param id   $orgId
     * @param id   $contactId
     * @param bool $isSelf
     *
     * @return bool
     */
    public function addContact($orgId, $contactId, $isSelf = false)
    {
        $orgContact = \laabs::newInstance('organization/orgContact');
        $orgContact->orgId = $orgId;
        $orgContact->contactId = $contactId;
        $orgContact->isSelf = $isSelf;

        return $this->sdoFactory->create($orgContact);
    }

    /**
     * Get organization adresses
     * @param id $orgId
     *
     * @return contact/address[]
     */
    public function getAddresses($orgId)
    {
        $orgContacts = $this->sdoFactory->find('organization/orgContact', "orgId='$orgId' and isSelf=true");
        if (count($orgContacts) == 0) {
            return array();
        }

        $contact = \laabs::callService('contact/contact/read_contactId_', $orgContacts[0]->contactId);

        return $contact->address;
    }

    /**
     * Get organization communications
     * @param id $orgId
     *
     * @return contact/communication[]
     */
    public function getCommunications($orgId)
    {
        $orgContacts = $this->sdoFactory->find('organization/orgContact', "orgId='$orgId' and isSelf=true");
        if (count($orgContacts) == 0) {
            return array();
        }

        $contact = \laabs::callService('contact/contact/read_contactId_', $orgContacts[0]->contactId);

        return $contact->communication;
    }
}

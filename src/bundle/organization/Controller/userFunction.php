<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle organization.
 *
 * Bundle organization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle organization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle organization.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\organization\Controller;

/**
 * Control of the user function
 *
 * @package Organization
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class userFunction
{

    private $sdoFactory;

    /**
     * Constructor of user function controller
     * @param \dependency\sdo\Factory $sdoFactory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all user function
     * @return bool
     */
    public function index()
    {
        return $this->sdoFactory->find("organization/userFunction");
    }

    /**
     * Create a new user function
     * @param organization/userFunction $userFunction The user function to records
     *
     * @return string The user function identifier
     */
    public function create($userFunction)
    {
        $userFunction->userFunctionId = \laabs\uniqid();
        if ($userFunction->reference == "" || $userFunction->name == "") {
            throw new \bundle\organization\Exception\invalidValueException("Name and reference is required");
        }
        $this->sdoFactory->create($userFunction, "organization/userFunction");

        return $userFunction->userFunctionId;
    }

    /**
     * Get a user function by ID
     * @param string $userFunctionId The user function identifier
     *
     * @return organization/userFunction The user function object
     */
    public function readById($userFunctionId)
    {
        return  $this->sdoFactory->read("organization/userFunction", array("userFunctionId", $userFunctionId));
    }

    /**
     * Get a user function by reference
     * @param string $reference The user function reference
     *
     * @return organization/userFunction The user function object
     */
    public function readByReference($reference)
    {
        return  $this->sdoFactory->find("organization/userFunction", "reference='$reference'")[0];
    }

    /**
     * Delete a user function
     * @param string $userFunctionId The user function identifier
     *
     * @return bool
     */
    public function delete($userFunctionId)
    {
        $userFunction = $this->sdoFactory->read("organization/userFunction", array("userFunctionId" => $userFunctionId));
        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $usersPosition = $this->sdoFactory->find("organization/userPosition", "function='$userFunction->reference'");

            if (count($usersPosition) > 0) {
                foreach ($usersPosition as $userPosition) {
                    $userPosition->function = "";
                    $this->sdoFactory->update($userPosition, "organization/userPosition");
                }
            }

            $this->sdoFactory->delete($userFunction, "organization/userFunction");
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }
            var_dump($exception);
            exit;
            throw new \bundle\businessExpenses\Exception\invalidValueException("User function not deleted");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }
}

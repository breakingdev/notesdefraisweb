<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses;

/**
 * Interface for expense report
 */
interface expenseReportInterface
{

    /**
     * List the expense reports
     *
     * @action businessExpenses/expenseReport/index
     */
    public function readIndex();

    /**
     * List the expense reports to validate
     *
     * @action businessExpenses/expenseReport/validatorIndex
     */
    public function readValidatorIndex();

    /**
     * List the expense reports to accounted
     *
     * @action businessExpenses/expenseReport/accountantIndex
     */
    public function readAccountantIndex();

    /**
     * Get an expense report
     *
     * @action businessExpenses/expenseReport/read
     */
    public function read_expenseReportId_();

    /**
     * Record a new expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report object to record
     *
     * @action businessExpenses/expenseReport/create
     */
    public function create($expenseReport);

    /**
     * Update an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report object to update
     *
     * @action businessExpenses/expenseReport/update
     */
    public function update($expenseReport);

    /**
     * Send an expense report in validation
     *
     * @action businessExpenses/expenseReport/sendInValidation
     */
    public function updateSendinvalidation_expenseReportId_();

    /**
     * Delete an expense report
     *
     * @action businessExpenses/expenseReport/delete
     */
    public function delete_expenseReportId_();

    /**
     * Validate an expense report
     *
     * @action businessExpenses/expenseReport/validateExpenseReport
     */
    public function read_expenseReportId_Validate();

    /**
     * Reject an expense report
     * @param businessExpenses/rejectExpenseReport $rejectExpenseReport The expense report object to reject
     * @action businessExpenses/expenseReport/rejectExpenseReport
     */
    public function updateReject($rejectExpenseReport);

    /**
     * Close an expense report
     *
     * @action businessExpenses/expenseReport/closeExpenseReport
     */
    public function read_expenseReportId_Close();

    /**
     * List the history for the validator
     *
     * @action businessExpenses/expenseReport/validatorHistory
     */
    public function readValidatorHistory();

    /**
     * List the history for the validator
     * @param date $beginDateMin The min date for the begin date
     * @param date $beginDateMax The max date for the begin date
     * @param string $endDateMin   The min date for the end date
     * @param string $endDateMax   The max date for the end date
     *
     * @action businessExpenses/expenseReport/accountantSearchHistory
     */
    public function readAccountantSearchhistory($beginDateMin = false, $beginDateMax = false, $endDateMin = false, $endDateMax = false);

    /**
     * Export an expense report
     *
     * @action businessExpenses/expenseReport/accountantExport
     */
    public function readAccountantExport_expenseReportId_();
}

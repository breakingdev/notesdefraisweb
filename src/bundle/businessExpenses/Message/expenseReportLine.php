<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Message;

/**
 * expense report Line definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class expenseReportLine
{
    /**
     * @var id
     */
    public $expenseReportId;

    /**
     * @var id
     */
    public $validatorId;

    /**
     * @var id
     */
    public $accountantId;

    /**
     * @var id
     */
    public $personalId;

    /**
     * @var id
     */
    public $serviceId;

    /**
     * @var string
     */
    public $status;

    /**
     * @var date
     */
    public $beginDate;

    /**
     * @var date
     */
    public $endDate;

    /**
     * @var timestamp
     */
    public $sendToValidationDate;

    /**
     * @var timestamp
     */
    public $validationDate;

    /**
     * @var timestamp
     */
    public $closingDate;

    /**
     * @var id
     */
    public $expenseLineId;

    /**
     * @var date
     */
    public $date;

    /**
     * @var id
     * @notempty
     */
    public $businessCategoryId;

    /**
     * @var id
     */
    public $customerId;

    /**
     * @var id
     */
    public $projectId;

    /**
     * @var bool
     */
    public $rebilling;

    /**
     * @var string
     */
    public $partyName;

    /**
     * @var number
     */
    public $amount;

    /**
     * @var number
     */
    public $VatAmount;

    /**
     * @var int
     */
    public $distance;

    /**
     * @var int
     */
    public $taxHorsepower;
}

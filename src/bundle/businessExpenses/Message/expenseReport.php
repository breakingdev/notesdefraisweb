<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Message;

/**
 * expense report definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class expenseReport
{
    /**
     * @var id
     */
    public $expenseReportId;

    /**
     * @var id
     */
    public $validatorId;

    /**
     * @var id
     */
    public $accountantId;

    /**
     * @var id
     */
    public $personalId;

    /**
     * @var id
     * @notempty
     */
    public $serviceId;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     * @notempty
     */
    public $description;

    /**
     * @var date
     * @notempty
     */
    public $beginDate;

    /**
     * @var date
     * @notempty
     */
    public $endDate;

    /**
     * @var timestamp
     */
    public $sendToValidationDate;

    /**
     * @var timestamp
     */
    public $validationDate;

    /**
     * @var timestamp
     */
    public $closingDate;

    /**
     * @var string
     */
    public $validatorComment;
}

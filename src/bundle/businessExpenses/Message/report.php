<?php

namespace bundle\businessExpenses\Message;

/**
 *
 */
class report
{
    public $title;

    public $labels = array();

    public $series = array();

    public $values = array();

    public function load($dataset, $bFacet, $xFacet)
    {
        foreach ($dataset as $item) {
            // Get X facet for labels and value position in matrix
            $xValue = $item->$xFacet;

            if (!in_array($xValue, $this->labels)) {
                $this->labels[] = $xValue;
            }

            $xPos = array_search($xValue, $this->labels);

            // Get break value in row
            $bValue = $item->$bFacet;
            if (!$series = $this->getSeries($bValue)) {
                $series = $this->addSeries($bValue);
            }

            $series->addValue($xPos, $item->value);
        }

        foreach ($this->series as $bValue => $series) {
            $series->fill(count($this->labels));
        }
    }

    public function addSeries($bValue)
    {
        $series = new series();
        $series->label = $bValue;
        $this->series[] = $series;

        return $series;
    }

    public function getSeries($bValue)
    {
        foreach ($this->series as $series) {
            if ($series->label == $bValue) {
                return $series;
            }
        }
    }
}

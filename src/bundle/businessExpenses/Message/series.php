<?php

namespace bundle\businessExpenses\Message;

/**
 *
 */
class series
{
    public $label;

    public $values = array();

    public function addValue($pos, $value)
    {
        $this->values[$pos] = $value;
    }

    public function fill($length)
    {
        $tpl = array_fill(0, $length, null);

        $this->values = $this->values + $tpl;

        ksort($this->values);
    }
}

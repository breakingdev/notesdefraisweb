<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Message;

/**
 * resource definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class resource
{
    /**
     * @var resource
     */
    public $handler;

    /**
     * @var string
     */
    public $mimetype;

    /**
     * Set resource handler
     * @param resource $handler
     */
    public function setHandler($handler)
    {
        $this->handler = $handler;
    }

    /**
     * Get resource handler
     *
     * @return resource handler
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * Set resource contents
     * @param string $contents The contents of resource
     */
    public function setContents($contents)
    {
        $this->handler = \laabs::createMemoryStream($contents);
    }

    /**
     * Get resource contents
     *
     * @return string
     */
    public function getContents()
    {
        $contents = stream_get_contents($this->handler);
        rewind($this->handler);

        return $contents;
    }
}

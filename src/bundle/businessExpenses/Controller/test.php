<?php

namespace bundle\businessExpenses\Controller;

class test
{
    protected $sdoFactory;
    protected $mileageAllowanceScale;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory            The sdo factory
     * @param array                   $mileageAllowanceScale Config pour la gestion de frais km
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory, $mileageAllowanceScale)
    {
        $this->sdoFactory = $sdoFactory;
        $this->mileageAllowanceScale = $mileageAllowanceScale;
    }

    /**
     * test method
     */
    public function test()
    {
        // recup des services de l'utilisateur
            //$userPositionController = \laabs::newController('organization/userPosition');
            //var_dump($userPositionController->getMyPositions());

        // recup service enfant
//                $servicePositionController = \laabs::newController('organization/servicePosition');
//                $childrenServices = $servicePositionController->readDescandantService(\laabs::getToken("ORGANIZATION")->orgId);
//
//                $str = [];
//
//                foreach ($childrenServices as $key => $value) {
//                    $str[] = $key;
//                }
//
//                $res = $this->sdoFactory->find("businessExpenses/expenseReport", "serviceId=['".implode("','", $str)."']");
//
//                var_dump($res);
//                exit;


        /*$masController = \laabs::newController("businessExpenses/mileageAllowanceScale");
        var_dump($masController->calculateTheMileageAllowances(3, 20001, 2016));
        exit;*/

        // recup du validateur dans les service au dessus du user
//                $orgController = \laabs::newController("organization/organization");
//                $myOrg = $orgController->read(\laabs::getToken("ORGANIZATION")->orgId);
//                $stop = false;
//                $validator = false;
//
//                do {
//                    $validatorsPositions = $this->sdoFactory->find("organization/userPosition", "function='VALIDATOR' AND orgId='$myOrg->orgId'");
//
//                    if (count($validatorsPositions) > 0) {
//                        $validator = $validatorsPositions[0];
//                        $stop = true;
//                    }
//
//                    if (!isset($myOrg->parentOrgId)) {
//                        $stop = true;
//                    } else {
//                        $myOrg = $orgController->read($myOrg->parentOrgId);
//                    }
//                } while ($stop != true);
//
//                if ($validator) {
//                    var_dump($validator);
//                    exit;
//                } else {
//                    var_dump("banane");
//                    exit;
//                }

        exit;
    }
}

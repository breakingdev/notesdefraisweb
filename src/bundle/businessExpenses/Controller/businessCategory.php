<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * businessCategory  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class businessCategory
{

    private $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all business categories
     * @param boolean $system Get system business expenses lines
     *
     * @return array Array of business category object
     */
    public function index($system = false)
    {
        $whereClause = [];

        if (!$system) {
            $whereClause[] = "system=false";
        }

        return $this->sdoFactory->find("businessExpenses/businessCategory", \laabs\implode(" AND ", $whereClause));
    }

    /**
     * Create a new business category
     * @param businessExpenses/businessCategory $businessCategory The business category to records
     *
     * @return id The identifier of the new business category
     */
    public function create($businessCategory)
    {
        $businessCategory->businessCategoryId = \laabs::newId();
        $this->sdoFactory->create($businessCategory, "businessExpenses/businessCategory");

        return $businessCategory->businessCategoryId;
    }

    /**
     * Delete a business category
     * @param string $businessCategoryId the business category identifier
     *
     * @return boolean
     */
    public function delete($businessCategoryId)
    {
        $businessCategory = $this->sdoFactory->read("businessExpenses/businessCategory", array("businessCategoryId" => $businessCategoryId));

        return $this->sdoFactory->delete($businessCategory, "businessExpenses/businessCategory");
    }

    /**
     * Get a business category
     * @param string $businessCategoryId the business category identifier
     *
     * @return businessExpense/businessCategory object
     */
    public function read($businessCategoryId)
    {
        return $this->sdoFactory->read("businessExpenses/businessCategory", array("businessCategoryId" => $businessCategoryId));
    }
}

<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * expenseReport validator trait
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
trait expenseReportValidatorTrait
{
    /**
     * Get all business reports to validate
     *
     * @return array Array of expense report object
     */
    public function validatorIndex()
    {
        $servicePositionController = \laabs::newController('organization/servicePosition');
        $childrenServices = $servicePositionController->readDescandantService(\laabs::getToken("ORGANIZATION")->orgId);

        $serviceIds = [];

        foreach ($childrenServices as $key => $value) {
            $serviceIds[] = $key;
        }

        return $this->sdoFactory->find("businessExpenses/expenseReport", "serviceId=['".implode("','", $serviceIds)."'] AND status='inValidation'");
    }

    /**
     * Get all business reports to validate
     *
     * @return array Array of expense report object
     */
    public function validatorHistory()
    {
        $validatorId = \laabs::getToken("AUTH")->accountId;

        return $this->sdoFactory->find("businessExpenses/expenseReport", "validatorId='$validatorId' AND status=['inAccounting', 'close']");
    }

    /**
     * Validate an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string The expense report identifier
     */
    public function validateExpenseReport($expenseReportId)
    {
        $expenseReport = $this->read($expenseReportId);
        $expenseReport->status = "inAccounting";
        $expenseReport->validatorId = \laabs::getToken("AUTH")->accountId;
        $expenseReport->validationDate = \laabs::newDateTime();

        $this->sdoFactory->update($expenseReport, "businessExpenses/expenseReport");

        return $expenseReportId;
    }

    /**
     * Reject an expense report
     * @param businessExpenses/rejectExpenseReport $rejectExpenseReport The reject expense report object
     *
     * @return string The expense report identifier
     */
    public function rejectExpenseReport($rejectExpenseReport)
    {
        $expenseReport = $this->read($rejectExpenseReport->expenseReportId);
        $expenseReport->status = "reject";
        $expenseReport->validatorComment = $rejectExpenseReport->validatorComment;

        $this->sdoFactory->update($expenseReport, "businessExpenses/expenseReport");

        return $expenseReport->expenseReportId;
    }
}

<?php

namespace bundle\businessExpenses\Controller;

/**
 * Class for report display
 */
class statistic
{
    protected $ds;
    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\datasource\DatasourceInterface $ds
     */
    public function __construct(\dependency\datasource\DatasourceInterface $ds, \dependency\sdo\Factory $sdoFactory)
    {
        $this->ds = $ds;
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get categories by services
     * @param date $dateMin The start date of the raqnge
     * @param date $dateMax The end date of the raqnge
     *
     * @return object
     */
    public function getCategoriesByServices($dateMin = null, $dateMax = null)
    {
        $objectClass = '"businessExpenses"."expenseReportLine"';
        $facets = array(
            "serviceId",
            "businessCategoryId",
            );

        $bFacet = 'serviceId';
        $xFacet = 'businessCategoryId';

        $valueExpr = 'sum("amount") "value"';

        $facetsExpr = '"'.implode('", "', $facets).'"';

        $query  = "SELECT ";
        $query .= $facetsExpr;
        $query .= ', '.$valueExpr;
        $query .= " FROM ".$objectClass;
        $query .= " WHERE \"businessCategoryId\"!='REGULARIZATION'";

        if ($dateMin) {
            $dateMin = date("Y-m-d", strtotime($dateMin));
            $query .= " AND \"date\">='$dateMin'";
        }
        if ($dateMax) {
            $dateMax = date("Y-m-d", strtotime($dateMax));
            $query .= " AND \"date\"<='$dateMax'";
        }

        $query .= " GROUP BY ".$facetsExpr;
        $query .= " ORDER BY ".$facetsExpr;

        $stmt = $this->ds->query($query);

        $resultSet = $stmt->fetchAll();

        $report = \laabs::newMessage("businessExpenses/report");

        $report->load($resultSet, $bFacet, $xFacet);

        $businessCategories = \laabs::newController("businessExpenses/businessCategory")->index();
        $categoriesArray = [];

        foreach ($businessCategories as $key => $businessCategory) {
            $categoriesArray[(string) $businessCategory->businessCategoryId] = $businessCategory->description;
        }

        for ($i = 0, $count = count($report->labels); $i < $count; $i++) {
            $report->labels[$i] = $categoriesArray[$report->labels[$i]];
        }

        for ($i = 0, $count = count($report->series); $i < $count; $i++) {
            $report->series[$i]->label = $this->sdoFactory->read("organization/organization", array("orgId" => $report->series[$i]->label))->orgName;
        }

        return $report;
    }
}

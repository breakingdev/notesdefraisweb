<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * project  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class project
{

    private $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all projects
     *
     * @return array Array of project object
     */
    public function index()
    {
        return $this->sdoFactory->find("businessExpenses/project");
    }

    /**
     * Create a new project
     * @param businessExpenses/project $project The project to records
     *
     * @return id The identifier of the new project
     */
    public function create($project)
    {
        $project->projectId = \laabs::newId();

        if (!empty($project->customerId) && !$this->sdoFactory->exists("businessExpenses/customer", array("customerId" => $project->customerId))) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The customer doesn't exists");
        }

        $this->sdoFactory->create($project, "businessExpenses/project");

        return $project->projectId;
    }

    /**
     * get a project by identifier
     * @param string $projectId the project identifier
     *
     * @return businessExpenses/project The project object
     */
    public function read($projectId)
    {
        return $this->sdoFactory->read("businessExpenses/project", array("projectId" => $projectId));
    }

    /**
     * get projects by customer identifier
     * @param string $customerId The customer identifier
     *
     * @return array The array of businessExpenses/project object
     */
    public function readByCustomerId($customerId)
    {
        $projects = $this->sdoFactory->find("businessExpenses/project", "customerId='$customerId'");

        return $projects;
    }

    /**
     * Update a project
     * @param businessExpenses/project $project The project to update
     *
     * @return id The identifier of the project
     */
    public function update($project)
    {
        $this->sdoFactory->update($project, "businessExpenses/project");

        return $project->projectId;
    }

    /**
     * Delete a project
     * @param string $projectId the project identifier
     *
     * @return boolean
     */
    public function delete($projectId)
    {
        if ($count = $this->sdoFactory->count("businessExpenses/expenseLine", "projectId='$projectId'") > 0) {
            $exception = new \bundle\businessExpenses\Exception\foreingKeyException('The project can not be remove because it\'s use by %1$d expense line(s)');
            $exception->count = $count;
            throw $exception;
        }

        $project = $this->sdoFactory->read("businessExpenses/project", array("projectId" => $projectId));

        return $this->sdoFactory->delete($project, "businessExpenses/project");
    }

    /**
     * Get the list of projects
     * @param string $query A query string of tokens
     *
     * @return array The list of groups
     */
    public function queryProjects($query)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $queryProperties = array("name");
        $queryPredicats = array();
        foreach ($queryProperties as $queryProperty) {
            foreach ($queryTokens as $queryToken) {
                $queryPredicats[] = $queryProperty."="."'*".$queryToken."*'";
            }
        }
        $queryString = implode(" OR ", $queryPredicats);

        $result = $this->sdoFactory->find('businessExpenses/project', $queryString);

        return $result;
    }
}

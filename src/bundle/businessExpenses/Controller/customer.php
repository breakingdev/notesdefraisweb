<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * customer  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class customer
{

    private $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all customers
     *
     * @return array Array of customer object
     */
    public function index()
    {
        return $this->sdoFactory->find("businessExpenses/customer");
    }

    /**
     * Create a customer
     * @param businessExpenses/customer $customer The customer to records
     *
     * @return id The identifier of the new customer
     */
    public function create($customer)
    {
        $customer->customerId = \laabs::newId();
        $this->sdoFactory->create($customer, "businessExpenses/customer");

        return $customer->customerId;
    }

    /**
     * get a customer by identifier
     * @param string $customerId the customer identifier
     *
     * @return id The identifier of the new customer
     */
    public function read($customerId)
    {
        return $this->sdoFactory->read("businessExpenses/customer", array("customerId" => $customerId));
    }

    /**
     * Update a customer
     * @param businessExpenses/customer $customer The customer to update
     *
     * @return id The identifier of the customer
     */
    public function update($customer)
    {
        $this->sdoFactory->update($customer, "businessExpenses/customer");

        return $customer->customerId;
    }

    /**
     * Delete a customer
     * @param string $customerId the customer identifier
     *
     * @return boolean
     */
    public function delete($customerId)
    {
        if ($count = $this->sdoFactory->count("businessExpenses/project", "customerId='$customerId'") > 0) {
            $exception = new \bundle\businessExpenses\Exception\foreingKeyException('The customer can not be remove because it\'s use by %1$d project(s)');
            $exception->count = $count;
            throw $exception;
        }

        $customer = $this->sdoFactory->read("businessExpenses/customer", array("customerId" => $customerId));

        return $this->sdoFactory->delete($customer, "businessExpenses/customer");
    }

    /**
     * Get the list of customers
     * @param string $query A query string of tokens
     *
     * @return array The list of groups
     */
    public function queryCustomers($query)
    {
        $queryTokens = \laabs\explode(" ", $query);
        $queryTokens = array_unique($queryTokens);

        $queryProperties = array("name");
        $queryPredicats = array();

        foreach ($queryProperties as $queryProperty) {
            foreach ($queryTokens as $queryToken) {
                $queryPredicats[] = $queryProperty."="."'*".$queryToken."*'";
            }
        }
        $queryString = implode(" OR ", $queryPredicats);

        $result = $this->sdoFactory->find('businessExpenses/customer', $queryString);

        return $result;
    }
}

<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * expenseReport accountant trait
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
trait expenseReportAccountantTrait
{
    /**
     * Get all business reports to accounted
     *
     * @return array Array of expense report object
     */
    public function accountantIndex()
    {
        return $this->sdoFactory->find("businessExpenses/expenseReport", "status='inAccounting'");
    }

    /**
     * Close an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string The expense report identifier
     */
    public function closeExpenseReport($expenseReportId)
    {
        $expenseReport = $this->read($expenseReportId);
        $expenseReport->status = "close";
        $expenseReport->accountantId = \laabs::getToken("AUTH")->accountId;
        $expenseReport->closingDate = \laabs::newDateTime();

        $this->sdoFactory->update($expenseReport, "businessExpenses/expenseReport");

        return $expenseReportId;
    }

    /**
     * Get all business reports to validate
     * @param date $beginDateMin
     * @param date $beginDateMax
     *
     * @return array Array of expense report object
     */
    public function accountantSearchHistory($beginDateMin = false, $beginDateMax = false, $endDateMin = false, $endDateMax = false)
    {
        $whereClause = [];
        $queryParams = [];

        $whereClause[] = "status='close'";

        if ($beginDateMin) {
            $queryParams["beginDateMin"] = $beginDateMin;
            $whereClause[] = "beginDate >= :beginDateMin";
            //$whereClause[] = "beginDate >= '$beginDateMin'";
        }
        if ($beginDateMax) {
            $queryParams["beginDateMax"] = $beginDateMax;
            $whereClause[] = "beginDate <= :beginDateMax";
            //$whereClause[] = "beginDate <= '$beginDateMax'";
        }
        if ($endDateMin) {
            $queryParams["endDateMin"] = $endDateMin;
            $whereClause[] = "endDate >= :endDateMin";
            //$whereClause[] = "endDate >= '$endDateMin'";
        }
        if ($endDateMax) {
            $queryParams["endDateMax"] = $endDateMax;
            $whereClause[] = "endDate <= :endDateMax";
            //$whereClause[] = "endDate <= '$endDateMax'";
        }

        //return $this->sdoFactory->find("businessExpenses/expenseReport", \laabs\implode(" AND ", $whereClause));
        return $this->sdoFactory->find("businessExpenses/expenseReport", \laabs\implode(" AND ", $whereClause), $queryParams);
    }

    /**
     * Export an expense report
     * @param id $expenseReportId The expense report identifier
     *
     * @return mixted File
     */
    public function accountantExport($expenseReportId)
    {
        $package = new \stdClass();
        $package->packageId = \laabs::newId();
        $package->method = 'LZMA';

        $expenseLineCtrl = \laabs::newController("businessExpenses/expenseLine");
        $expenseLines = $expenseLineCtrl->index($expenseReportId);

        $packageDir = \laabs\tempdir().DIRECTORY_SEPARATOR.\laabs::getInstanceName().DIRECTORY_SEPARATOR."zip".DIRECTORY_SEPARATOR.$package->packageId;

        mkdir($packageDir, 0775, true);

        $zipfile = $packageDir.DIRECTORY_SEPARATOR.$package->packageId.'.7z';

        for ($i = 0, $count = count($expenseLines); $i < $count; $i++) {
            if (empty($expenseLines[$i]->file)) {
                continue;
            }
            file_put_contents($packageDir.DIRECTORY_SEPARATOR.$expenseLines[$i]->lineNumber, (string) $expenseLines[$i]->file);
        }
        $resourceCSV = fopen($packageDir.DIRECTORY_SEPARATOR.$expenseReportId.".csv", "w+");

        $expenseReportCsv = $this->getExpenseReportForCsv($expenseReportId);
        fputcsv($resourceCSV, $expenseReportCsv);

        $expenseLinesCsv = $this->getExpenseLinesForCsv($expenseLines);
        $count = count($expenseLinesCsv);
        for ($i = 0; $i < $count; $i++) {
            fputcsv($resourceCSV, $expenseLinesCsv[$i]);
        }

        $zip = \laabs::newService("dependency/fileSystem/plugins/zip");
        $zip->add($zipfile, $packageDir.DIRECTORY_SEPARATOR."*");

        return $zipfile;
    }

    /**
     * Get expense report information to export in csv
     * @param id $expenseReportId The expense report identifier
     *
     * @return array The expense report in array
     */
    private function getExpenseReportForCsv($expenseReportId)
    {
        $this->translator->setCatalog('businessExpenses/expenseReport');

        $organizationController = \laabs::newController('organization/organization');
        $userAccountController = \laabs::newController('auth/userAccount');

        $expenseReport = $this->read($expenseReportId);

        $res = [];
        //$res[] = $expenseReport->personalId->__toString();
        $userAccountController->edit($expenseReport->personalId)->displayName;
        //$res[] = $expenseReport->serviceId->__toString();
        $organizationController->read($expenseReport->serviceId)->orgName;
        //$res[] = $expenseReport->status;
        $res[] = $this->translator->getText($expenseReport->status);
        $res[] = $expenseReport->beginDate->__toString();
        $res[] = $expenseReport->endDate->__toString();
        $res[] = $expenseReport->description;

        return $res;
    }

    /**
     * Get expense lines information to export in csv
     * @param id $expenseReportId The expense report identifier
     *
     * @return array The expense report in array
     */
    private function getExpenseLinesForCsv($expenseLines)
    {
        $businessCategoryCtrl = \laabs::newController('businessExpenses/businessCategory');
        $customerCtrl = \laabs::newController('businessExpenses/customer');
        $projectCtrl = \laabs::newController('businessExpenses/project');

        $totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $res = [];

        foreach ($expenseLines as $expenseLine) {
            $line = [];
            $line[] = $expenseLine->lineNumber;
            $line[] = $expenseLine->date->__toString();
            $line[] = $businessCategoryCtrl->read($expenseLine->businessCategoryId)->description;
            $line[] = !empty($expenseLine->customerId) ? $customerCtrl->read($expenseLine->customerId)->name : "";
            $line[] = !empty($expenseLine->projectId) ? $projectCtrl->read($expenseLine->projectId)->name : "";
            $line[] = $expenseLine->rebilling;
            $line[] = $expenseLine->reason;
            $line[] = !empty($expenseLine->taxHorsepower) ? $expenseLine->taxHorsepower : "";
            $line[] = !empty($expenseLine->distance) ? $expenseLine->distance : "";
            $line[] = !empty($expenseLine->partyName) ? $expenseLine->partyName : "";
            $line[] = !empty($expenseLine->proofReference) ? $expenseLine->proofReference : "";
            $line[] = !empty($expenseLine->VatAmount) ? $expenseLine->VatAmount->__toString() : "";
            $line[] = $expenseLine->amount->__toString();

            $res[] = $line;
        }
        $line = [];
        $line = array_pad($line , 12, "");
        $line[] = $totalAmount;
        $res[] = $line;

        return $res;
    }
}

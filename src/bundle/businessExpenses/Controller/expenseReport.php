<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * expenseReport  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class expenseReport
{
    use expenseReportValidatorTrait;
    use expenseReportAccountantTrait;

    private $sdoFactory;
    private $translator;

    /**
     * Constructor
     * @param \dependency\sdo\Factory                      $sdoFactory The dependency Sdo Factory object
     * @param \dependency\localisation\TranslatorInterface $translator The translator
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->sdoFactory = $sdoFactory;
        $this->translator = $translator;
    }

    /**
     * Get all business reports
     *
     * @return array Array of expense report object
     */
    public function index()
    {
        $personalId = \laabs::getToken("AUTH")->accountId;
//        $res = $this->sdoFactory->find("businessExpenses/expenseReport", "personalId='$personalId'", null , ">beginDate");
//        foreach ($res as $test) {
//            var_dump($test->beginDate);
//        }
//        exit;
        return $this->sdoFactory->find("businessExpenses/expenseReport", "personalId='$personalId'");
    }

    /**
     * Get all business reports to validate
     *
     * @return array Array of expense report object
     */
    public function validatorIndex()
    {
        $servicePositionController = \laabs::newController('organization/servicePosition');
        $childrenServices = $servicePositionController->readDescandantService(\laabs::getToken("ORGANIZATION")->orgId);

        $serviceIds = [];

        foreach ($childrenServices as $key => $value) {
            $serviceIds[] = $key;
        }

        return $this->sdoFactory->find("businessExpenses/expenseReport", "serviceId=['".implode("','", $serviceIds)."'] AND status='inValidation'");
    }

    /**
     * Create a new expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report to record
     *
     * @return id The identifier of the new expense report
     */
    public function create($expenseReport)
    {
        $expenseReport->expenseReportId = \laabs::newId();
        $expenseReport->personalId = \laabs::getToken("AUTH")->accountId;
        $expenseReport->status = "inEdition";

        $this->validationDate($expenseReport);

        if (!$this->sdoFactory->exists("organization/userPosition", array("userAccountId" => $expenseReport->personalId, "orgId" => $expenseReport->serviceId))) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The user isn't in this service");
        }

        $this->sdoFactory->create($expenseReport, "businessExpenses/expenseReport");

        return $expenseReport->expenseReportId;
    }

    /**
     * Get an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return businessExpenses/expenseReport The expense report
     */
    public function read($expenseReportId)
    {
        if (!$this->sdoFactory->exists("businessExpenses/expenseReport", array("expenseReportId" => $expenseReportId))) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("This expense report doesn't exists");
        }

        $expenseReport = $this->sdoFactory->read("businessExpenses/expenseReport", array("expenseReportId" => $expenseReportId));

        return $expenseReport;
    }

    /**
     * Update an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report to update
     *
     * @return id The identifier of the expense report
     */
    public function update($expenseReport)
    {
        $this->validationDate($expenseReport);

        $expenseReportToUpdate = $this->read($expenseReport->expenseReportId);
        $expenseReportToUpdate->beginDate = $expenseReport->beginDate;
        $expenseReportToUpdate->endDate = $expenseReport->endDate;
        $expenseReportToUpdate->description = $expenseReport->description;
        $expenseReportToUpdate->serviceId = $expenseReport->serviceId;

        if ($expenseReportToUpdate->personalId != \laabs::getToken("AUTH")->accountId) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("Only the owner of this expense report can modify it");
        }

        $this->sdoFactory->update($expenseReportToUpdate, "businessExpenses/expenseReport");

        return $expenseReport->expenseReportId;
    }

    /**
     * Send an expense report in validation
     * @param string $expenseReportId The expense report identifier
     *
     * @return id The identifier of the expense report
     */
    public function sendInValidation($expenseReportId)
    {
        $expenseReport = $this->read($expenseReportId);

        if ($expenseReport->personalId != \laabs::getToken("AUTH")->accountId) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("Only the owner of this expense report can send it in validation");
        }

        $expenseLines = $this->sdoFactory->count("businessExpenses/expenseLine", "expenseReportId='$expenseReportId'");

        if (count($expenseLines) == 0) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The expense report must have expense lines to be send in validation");
        }
        $expenseReport->status = "inValidation";
        $expenseReport->validatorId = $this->getValidator($expenseReport);
        $expenseReport->validatorComment = "";

        if (empty($expenseReport->validatorId)) {
            $expenseReport->status = "inAccounting";
        }
        $expenseReport->sendToValidationDate = \laabs::newDateTime();

        $this->sdoFactory->update($expenseReport, "businessExpenses/expenseReport");

        return $expenseReport->expenseReportId;
    }

    /**
     * Delete an expense report
     * @param string $expenseReportId the expense report identifier
     *
     * @return boolean
     */
    public function delete($expenseReportId)
    {
        $expenseReport = $this->sdoFactory->read("businessExpenses/expenseReport", array("expenseReportId" => $expenseReportId));

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $this->sdoFactory->deleteChildren("businessExpenses/expenseLine", $expenseReport);
            $this->sdoFactory->delete($expenseReport, "businessExpenses/expenseReport");
        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw new \bundle\businessExpenses\Exception\invalidValueException("Expense report not deleted ");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }

    /**
     * Get the validator for expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report to record
     *
     * @return mixte The validator identifier or null
     */
    public function getValidator($expenseReport)
    {
        $orgController = \laabs::newController("organization/organization");
        $myOrg = $orgController->read(\laabs::getToken("ORGANIZATION")->orgId);
        $stop = false;
        $validator = false;

        do {
            $validatorsPositions = $this->sdoFactory->find("organization/userPosition", "function='VALIDATOR' AND orgId='$myOrg->orgId'");

            if (count($validatorsPositions) > 0) {
                $validator = $validatorsPositions[0];
                $stop = true;
            }

            if (!isset($myOrg->parentOrgId)) {
                $stop = true;
            } else {
                $myOrg = $orgController->read($myOrg->parentOrgId);
            }
        } while ($stop != true);

        if ($validator) {
            return $validator->userAccountId;
        }

        return null;
    }

    /**
     * Check if this date overlaps an other expense report
     * @param businessExpenses/expenseReport $expenseReportToCheck The expense report to check
     */
    private function validationDate($expenseReportToCheck)
    {
        $expenseReports = $this->index();

        if ($expenseReportToCheck->beginDate > $expenseReportToCheck->endDate) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The begin date is after the end date");
        }

        foreach ($expenseReports as $key => $expenseReport) {
            if ($expenseReport->expenseReportId == $expenseReportToCheck->expenseReportId) {
                continue;
            }
            if (($expenseReportToCheck->beginDate >= $expenseReport->beginDate) && ($expenseReportToCheck->beginDate <= $expenseReport->endDate) && ($expenseReportToCheck->serviceId == $expenseReport->serviceId)) {
                throw new \bundle\businessExpenses\Exception\invalidValueException("The date range overlaps with another expense report");
            } elseif (($expenseReportToCheck->endDate >= $expenseReport->beginDate) && ($expenseReportToCheck->endDate <= $expenseReport->endDate) && ($expenseReportToCheck->serviceId == $expenseReport->serviceId)) {
                throw new \bundle\businessExpenses\Exception\invalidValueException("The date range overlaps with another expense report");
            }
        }

        return true;
    }

    private function getExpenseReportTotalAmount($expenseLines)
    {
        $total = 0;

        if (!is_array($expenseLines)) {
            return $total;
        }

        foreach ($expenseLines as $key => $expenseLine) {
            $total += $expenseLine->amount->__toFLoat();
        }

        return number_format($total, 2);
    }
}

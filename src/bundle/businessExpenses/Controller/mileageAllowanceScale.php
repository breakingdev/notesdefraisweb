<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * businessCategory  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class mileageAllowanceScale
{

    private $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all business categories
     *
     * @return array Array index by category with mileage allowances object
     */
    public function index()
    {
        $mileageAllowances = $this->sdoFactory->find("businessExpenses/mileageAllowanceScale");

        $mileageAllowancesSort = [];

        foreach ($mileageAllowances as $key => $mileageAllowance) {
            if (!isset($mileageAllowancesSort[$mileageAllowance->category])) {
                $mileageAllowancesSort[$mileageAllowance->category] = [];
            }
            array_push($mileageAllowancesSort[$mileageAllowance->category], $mileageAllowance);
        }

        foreach ($mileageAllowancesSort as $key => $array) {
            usort($array, array($this, "cmpMileageExpense"));
        }

        return reset($mileageAllowancesSort);
    }

    /**
     * Record a new mileage allowance
     * @param businessExpenses/mileageAllowanceScale $mileageAllowanceScale The mileage allowance to records
     *
     * @return id The identifier of the new mileage allowance
     */
    public function create($mileageAllowanceScale)
    {
        foreach ($mileageAllowanceScale as $key => $value) {
            $mileageAllowanceScale->$key = trim($value);
        }

        $mileageAllowanceScale->mileageAllowanceScaleId = \laabs::newId();

        if ($mileageAllowanceScale->regularization == "") {
            unset($mileageAllowanceScale->regularization);
        }

        $this->sdoFactory->create($mileageAllowanceScale, "businessExpenses/mileageAllowanceScale");

        return $mileageAllowanceScale->mileageAllowanceScaleId;
    }

    /**
     * Delete a mileage allowance
     * @param string $mileageAllowanceScaleId the mileage allowance identifier
     *
     * @return boolean
     */
    public function delete($mileageAllowanceScaleId)
    {
        $mileageAllowanceScale = $this->sdoFactory->read("businessExpenses/mileageAllowanceScale", array("mileageAllowanceScaleId" => $mileageAllowanceScaleId));

        return $this->sdoFactory->delete($mileageAllowanceScale, "businessExpenses/mileageAllowanceScale");
    }

    /**
     * Calculate the mileage allowances
     * @param integrer $taxHorsepower The tax horsepower
     * @param integrer $distance      The distance
     * @param integrer $year          The year
     * @param string   $category      The category
     *
     * @return boolean
     */
    public function calculateTheMileageAllowances($taxHorsepower, $distance, $year, $category = "car")
    {
        if (!is_int($taxHorsepower)) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The tax horsepower isn't an integer");
        }

        if (!is_double($distance) && !is_float($distance) && !is_int($distance)) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The distance isn't a number");
        }

        if (0 == $this->sdoFactory->count("businessExpenses/mileageAllowanceScale", "rangeBegin<=$distance AND rangeEnd>=$distance AND category='$category' AND taxHorsepower='$taxHorsepower' AND year='$year'")) {
            throw new \dependency\sdo\Exception\objectNotFoundException("Mileage allowance not found");
        }

        $mileageAllowanceScale = $this->sdoFactory->find("businessExpenses/mileageAllowanceScale", "rangeBegin<=$distance AND rangeEnd>=$distance AND category='$category' AND taxHorsepower='$taxHorsepower' AND year='$year'")[0];

        $result = $mileageAllowanceScale->rate->__toFloat() * $distance;

        if (!empty($mileageAllowanceScale->regularization)) {
            $result += $mileageAllowanceScale->regularization->__toFloat();
        }

        return $result;
    }

    /**
     * Sort mileage allowance by tax horsepower
     * @param type $a
     * @param type $b
     * @return int
     */
    private function cmpMileageExpense($a, $b)
    {
        if ($a->taxHorsepower == $b->taxHorsepower) {
            return 0;
        }

        return ($a < $b) ? -1 : 1;
    }
}

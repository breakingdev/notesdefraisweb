<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Controller;

/**
 * expenseLine  controller
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 */
class expenseLine
{

    private $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory The dependency Sdo Factory object
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory = null)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all expense lines
     * @param string $expenseReportId The expense report identifier
     *
     * @return array Array of expense line object
     */
    public function index($expenseReportId)
    {
        $expenseLines = $this->sdoFactory->find("businessExpenses/expenseLine", "expenseReportId='$expenseReportId'", null, "lineNumber");

        return $expenseLines;
    }

    /**
     * Create a new expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line to records
     *
     * @return string The identifier of the new expense line
     */
    public function create($expenseLine)
    {
        $expenseLine->expenseLineId = \laabs::newId();

        $businessCategory = $this->sdoFactory->read("businessExpenses/businessCategory", array("businessCategoryId" => $expenseLine->businessCategoryId));

        if ($businessCategory->formType == "mileageExpense") {
            if (empty($expenseLine->distance) || empty($expenseLine->taxHorsepower)) {
                throw new \bundle\businessExpenses\Exception\invalidValueException("The distance and the tax horsepower must be not null for mileage expense");
            }

            $year = date("Y", strtotime($expenseLine->date));
            $expenseLine->amount = \laabs::newController("businessExpenses/mileageAllowanceScale")->calculateTheMileageAllowances($expenseLine->taxHorsepower, $expenseLine->distance, $year);
            $expenseLine->amount = \laabs::cast($expenseLine->amount, "number");
        }

        if ($expenseLine->amount->__toFloat() <= 0) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The amount must be positive");
        }

        $expenseLine->lineNumber = $this->sdoFactory->count("businessExpenses/expenseLine", "expenseReportId='$expenseLine->expenseReportId'")+1;

        if (isset($expenseLine->file)) {
            $expenseLine->file = base64_decode($expenseLine->file);
        }

        $this->sdoFactory->create($expenseLine, "businessExpenses/expenseLine");

        return $expenseLine->expenseLineId;
    }

    /**
     * Create a new regularization line
     * @param businessExpenses/regularizationline $regularizationLine The regularization line to records
     *
     * @return string The identifier of the new regularization line
     */
    public function createRegularizationline($regularizationLine)
    {
        $regularizationLine->expenseLineId = \laabs::newId();

        $regularizationLine->lineNumber = $this->sdoFactory->count("businessExpenses/expenseLine", "expenseReportId='$regularizationLine->expenseReportId'")+1;
        $regularizationLine->businessCategoryId = "REGULARIZATION";
        $regularizationLine->date = \laabs::newDate();
        $regularizationLine->rebilling = false;

        $this->sdoFactory->create($regularizationLine, "businessExpenses/expenseLine");

        return $regularizationLine->expenseLineId;
    }

    /**
     * Get an expense line
     * @param string $expenseLineId The expense line identifier
     *
     * @return businessExpenses/expenseLine The expense line object
     */
    public function read($expenseLineId)
    {
        if (!$this->sdoFactory->exists("businessExpenses/expenseLine", array("expenseLineId" => $expenseLineId))) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("This expense line doesn't exists");
        }

        $expenseLine = $this->sdoFactory->read("businessExpenses/expenseLine", array("expenseLineId" => $expenseLineId));

        return $expenseLine;
    }

    /**
     * Get a resource of expense line
     * @param string $expenseLineId The expense line identifier
     *
     * @return businessExpenses/resource The expense line resource object
     */
    public function getResource($expenseLineId)
    {
        if (!$this->sdoFactory->exists("businessExpenses/expenseLine", array("expenseLineId" => $expenseLineId))) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("This expense line doesn't exists");
        }

        $expenseLine = $this->sdoFactory->read("businessExpenses/expenseLine", array("expenseLineId" => $expenseLineId));

        if (!isset($expenseLine->file)) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("This resource doesn't exists");
        }
        $fi = new \finfo(FILEINFO_MIME_TYPE);

        $resource = \laabs::newMessage("businessExpenses/resource");
        $resource->setContents($expenseLine->file);
        $resource->mimetype = $fi->buffer($resource->getContents());

        return $resource;
    }

    /**
     * Update an expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line to update
     *
     * @return id The identifier of the expense line
     */
    public function update($expenseLine)
    {
        $businessCategory = $this->sdoFactory->read("businessExpenses/businessCategory", array("businessCategoryId" => $expenseLine->businessCategoryId));

        if ($businessCategory->formType == "mileageExpense") {
            if (empty($expenseLine->distance) || empty($expenseLine->taxHorsepower)) {
                throw new \bundle\businessExpenses\Exception\invalidValueException("The distance and the tax horsepower must be not null for mileage expense");
            }

            $year = date("Y", strtotime($expenseLine->date));
            $expenseLine->amount = \laabs::newController("businessExpenses/mileageAllowanceScale")->calculateTheMileageAllowances($expenseLine->taxHorsepower, $expenseLine->distance, $year);
            $expenseLine->amount = \laabs::cast($expenseLine->amount, "number");
        }

        if ($expenseLine->amount->__toFloat() <= 0) {
            throw new \bundle\businessExpenses\Exception\invalidValueException("The amount must be positive");
        }

        if (isset($expenseLine->file)) {
            $expenseLine->file = base64_decode($expenseLine->file);
        }

        $this->sdoFactory->update($expenseLine, "businessExpenses/expenseLine");

        return $expenseLine->expenseLineId;
    }

    /**
     * Delete an expense line
     * @param string $expenseLineId The expense line identifier
     *
     * @return boolean
     */
    public function delete($expenseLineId)
    {
        $expenseLine = $this->sdoFactory->read("businessExpenses/expenseLine", array("expenseLineId" => $expenseLineId));

        $transactionControl = !$this->sdoFactory->inTransaction();

        if ($transactionControl) {
            $this->sdoFactory->beginTransaction();
        }

        try {
            $expenseLines = $this->sdoFactory->find("businessExpenses/expenseLine", "lineNumber > $expenseLine->lineNumber AND expenseReportId='$expenseLine->expenseReportId'");

            $this->sdoFactory->delete($expenseLine, "businessExpenses/expenseLine");

            foreach ($expenseLines as $key => $line) {
                --$line->lineNumber;
                $this->sdoFactory->update($line, "businessExpenses/expenseLine");
            }

        } catch (\Exception $exception) {
            if ($transactionControl) {
                $this->sdoFactory->rollback();
            }

            throw new \bundle\businessExpenses\Exception\invalidValueException("Expense line not deleted ");
        }

        if ($transactionControl) {
            $this->sdoFactory->commit();
        }

        return true;
    }
}

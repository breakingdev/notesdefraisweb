<?php

namespace bundle\businessExpenses;

interface testInterface
{
    /**
     * List archival agreements
     *
     * @action businessExpenses/test/test
     */
    public function readTest();
}

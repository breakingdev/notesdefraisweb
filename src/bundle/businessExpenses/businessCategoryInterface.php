<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses;

/**
 * Interface for businessCategory
 */
interface businessCategoryInterface
{

    /**
     * List the business categories
     *
     * @action businessExpenses/businessCategory/index
     */
    public function readIndex();

    /**
     * Record a new business category
     * @param businessExpenses/businessCategory $businessCategory The business category object to record
     *
     * @action businessExpenses/businessCategory/create
     */
    public function create($businessCategory);

    /**
     * Delete a business category
     *
     * @action businessExpenses/businessCategory/delete
     */
    public function delete_businessCategoryId_();
}

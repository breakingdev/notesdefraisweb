<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses;

/**
 * Interface for expense line
 */
interface expenseLineInterface
{
    /**
     * List the expense lines for an expense report
     *
     * @action businessExpenses/expenseLine/index
     */
    public function readIndex_expenseReportId_();

    /**
     * Get an expense line
     *
     * @action businessExpenses/expenseLine/read
     */
    public function read_expenseLineId_();

    /**
     * Get a resource of expense line
     *
     * @action businessExpenses/expenseLine/getResource
     */
    public function readResource_expenseLineId_();

    /**
     * Record a new expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line object to record
     *
     * @action businessExpenses/expenseLine/create
     */
    public function create($expenseLine);

    /**
     * Record a new regularization line
     * @param businessExpenses/regularizationLine $regularizationLine The regularization line object to record
     *
     * @action businessExpenses/expenseLine/createRegularizationline
     */
    public function createRegularizationline($regularizationLine);

    /**
     * Update an expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line object to update
     *
     * @action businessExpenses/expenseLine/update
     */
    public function update($expenseLine);

    /**
     * Delete an expense line
     *
     * @action businessExpenses/expenseLine/delete
     */
    public function delete_expenseLineId_();
}

<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Model;

/**
 * mileageAllowanceScale definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 *
 * @pkey [mileageAllowanceScaleId]
 */
class mileageAllowanceScale
{
    /**
     * @var id
     * @notempty
     */
    public $mileageAllowanceScaleId;

    /**
     * @var string
     * @notempty
     * @enumeration [car, twoWheels]
     */
    public $category;

    /**
     * @var int
     * @notempty
     */
    public $year;

    /**
     * @var int
     * @notempty
     */
    public $taxHorsepower;

    /**
     * @var int
     * @notnull
     */
    public $rangeBegin;

    /**
     * @var int
     * @notnull
     */
    public $rangeEnd;

    /**
     * @var number
     * @notempty
     */
    public $rate;

    /**
     * @var number
     */
    public $regularization;
}

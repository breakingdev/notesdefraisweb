<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Model;

/**
 * expense line definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 *
 * @pkey [expenseLineId]
 * @fkey [businessCategoryId] businessExpenses/businessCategory [businessCategoryId]
 * @fkey [expenseReportId] businessExpenses/expenseReport [expenseReportId]
 */
Class expenseline
{
    /**
     * @var id
     * @notempty
     */
    public $expenseReportId;

    /**
     * @var id
     * @notempty
     */
    public $expenseLineId;

    /**
     * @var date
     * @notempty
     */
    public $date;

    /**
     * @var int
     * @notempty
     */
    public $lineNumber;

    /**
     * @var id
     * @notempty
     */
    public $businessCategoryId;

    /**
     * @var id
     */
    public $customerId;

    /**
     * @var id
     */
    public $projectId;

    /**
     * @var bool
     */
    public $rebilling;

    /**
     * @var string
     */
    public $partyName;

    /**
     * @var string
     */
    public $proofReference;

    /**
     * @var string
     * @notempty
     */
    public $reason;

    /**
     * @var number
     * @notempty
     */
    public $amount;

    /**
     * @var number
     */
    public $VatAmount;

    /**
     * @var int
     */
    public $distance;

    /**
     * @var int
     */
    public $taxHorsepower;

    /**
     * @var int
     */
    public $nbDay;

    /**
     * @var int
     */
    public $flateRateAmount;

    /**
     * @var binary
     */
    public $file;
}

<?php

/*
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses\Model;

/**
 * project definition
 *
 * @package businessExpenses
 * @author Alexis RAGOT <alexis.ragot@maarch.org>
 *
 * @pkey [projectId]
 * @key [code]
 * @fkey [customerId] businessExpenses/customer [customerId]
 */
class project
{
    /**
     * @var id
     * @notempty
     */
    public $projectId;

    /**
     * @var id
     * @notempty
     */
    public $customerId;

    /**
     * @var name
     * @notempty
     */
    public $code;

    /**
     * @var string
     * @notempty
     */
    public $name;

    /**
     * @var boolean
     * @notempty
     */
    public $rebilling = false;
}

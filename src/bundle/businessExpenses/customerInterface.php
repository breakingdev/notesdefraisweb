<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses;

/**
 * Interface for customer
 */
interface customerInterface
{

    /**
     * List the customers
     *
     * @action businessExpenses/customer/index
     */
    public function readIndex();

    /**
     * Get a customer
     *
     * @action businessExpenses/customer/read
     */
    public function read_customerId_();

    /**
     * Record a new customer
     * @param businessExpenses/customer $customer The customer object to record
     *
     * @action businessExpenses/customer/create
     */
    public function create($customer);
    /**
     * Update a customer
     * @param businessExpenses/customer $customer The customer object to update
     *
     * @action businessExpenses/customer/update
     */
    public function update($customer);

    /**
     * Delete a customer
     *
     * @action businessExpenses/customer/delete
     */
    public function delete_customerId_();

    /**
     * Get the list customer
     *
     * @action businessExpenses/customer/queryCustomers
     */
    public function readCustomers_query_();
}

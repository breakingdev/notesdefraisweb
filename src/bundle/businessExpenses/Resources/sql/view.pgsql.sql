CREATE VIEW "businessExpenses"."expenseReportLine" AS 
 SELECT "expenseReport"."expenseReportId",
    "expenseReport"."validatorId",
    "expenseReport"."accountantId",
    "expenseReport"."personalId",
    "expenseReport"."serviceId",
    "expenseReport"."status",
    "expenseReport"."beginDate",
    "expenseReport"."endDate",
    "expenseReport"."sendToValidationDate",
    "expenseReport"."validationDate",
    "expenseReport"."closingDate",

    "expenseLine"."expenseLineId",
    "expenseLine"."date",
    "expenseLine"."businessCategoryId",
    "expenseLine"."customerId",
    "expenseLine"."projectId",
    "expenseLine"."rebilling",
    "expenseLine"."partyName",
    "expenseLine"."amount",
    "expenseLine"."VatAmount",
    "expenseLine"."distance",
    "expenseLine"."taxHorsepower"

   FROM "businessExpenses"."expenseLine"
     LEFT JOIN "businessExpenses"."expenseReport" ON "expenseLine"."expenseReportId" = "expenseReport"."expenseReportId";
DROP SCHEMA IF EXISTS "businessExpenses" CASCADE;
CREATE SCHEMA "businessExpenses";

-- Table: "businessExpenses"."businessCategory"
DROP TABLE IF EXISTS "businessExpenses"."businessCategory" CASCADE;
CREATE TABLE "businessExpenses"."businessCategory"
(
  "businessCategoryId" text NOT NULL,
  "code" text NOT NULL,
  "description" text,
  "formType" text,
  "system" boolean NOT NULL DEFAULT false,
  
  PRIMARY KEY ("businessCategoryId"),
  UNIQUE ("code")
)
WITH (
  OIDS=FALSE
);

-- Table: "businessExpenses"."mileageAllowanceScale"
DROP TABLE IF EXISTS "businessExpenses"."mileageAllowanceScale";
CREATE TABLE "businessExpenses"."mileageAllowanceScale"
(
  "mileageAllowanceScaleId" text NOT NULL,
  "category" text NOT NULL,
  "year" integer NOT NULL,
  "taxHorsepower" integer NOT NULL,
  "rangeBegin" integer NOT NULL,
  "rangeEnd" integer NOT NULL,
  "rate" double precision NOT NULL,
  "regularization" double precision,

  PRIMARY KEY ("mileageAllowanceScaleId")
)
WITH (
  OIDS=FALSE
);

-- Table: "businessExpenses"."expenseReport"
DROP TABLE IF EXISTS"businessExpenses"."expenseReport";
CREATE TABLE "businessExpenses"."expenseReport"
(
  "expenseReportId" text NOT NULL,
  "validatorId" text,
  "accountantId" text,
  "personalId" text NOT NULL,
  "description" text NOT NULL,
  "serviceId" text NOT NULL,
  "status" text,
  "validatorComment" text,
  "beginDate" timestamp,
  "endDate" timestamp,
  "sendToValidationDate" timestamp,
  "validationDate" timestamp without time zone,
  "closingDate" timestamp without time zone,
  PRIMARY KEY ("expenseReportId")
)
WITH (
  OIDS=FALSE
);

-- Table: "businessExpenses"."customer"
DROP TABLE IF EXISTS"businessExpenses"."customer";
CREATE TABLE "businessExpenses"."customer"
(
  "customerId" text NOT NULL,
  "code" text NOT NULL,
  "name" text NOT NULL,
  PRIMARY KEY ("customerId"),
  UNIQUE ("code")
)
WITH (
  OIDS=FALSE
);

-- Table: "businessExpenses"."project"
DROP TABLE IF EXISTS "businessExpenses"."project";
CREATE TABLE "businessExpenses"."project"
(
  "projectId" text NOT NULL,
  "customerId" text,
  "code" text NOT NULL,
  "name" text NOT NULL,
  "rebilling" boolean DEFAULT false,
  PRIMARY KEY ("projectId"),
  UNIQUE ("code"),
  FOREIGN KEY ("customerId")
      REFERENCES "businessExpenses"."customer" ("customerId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: "businessExpenses"."expenseLine"
DROP TABLE IF EXISTS"businessExpenses"."expenseLine";
CREATE TABLE "businessExpenses"."expenseLine"
(
  "expenseLineId" text NOT NULL,
  "expenseReportId" text NOT NULL,
  "date" timestamp NOT NULL,
  "lineNumber" integer NOT NULL,
  "businessCategoryId" text NOT NULL,
  "customerId" text,
  "projectId" text,
  "rebilling" boolean NOT NULL,
  "partyName" text,
  "proofReference" text,
  "reason" text NOT NULL,

  "amount" double precision NOT NULL,
  "VatAmount" double precision,

  "distance" integer,
  "taxHorsepower" integer,
  
  "file" bytea,

  "nbDay" integer,
  "flateRateAmount" money,

  PRIMARY KEY ("expenseLineId"),
  FOREIGN KEY ("expenseReportId")
      REFERENCES "businessExpenses"."expenseReport" ("expenseReportId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY ("businessCategoryId")
      REFERENCES "businessExpenses"."businessCategory" ("businessCategoryId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  UNIQUE ("lineNumber", "expenseReportId")
)
WITH (
  OIDS=FALSE
);
<?php

/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace bundle\businessExpenses;

/**
 * Interface for project
 */
interface projectInterface
{

    /**
     * List the projects
     *
     * @action businessExpenses/project/index
     */
    public function readIndex();

    /**
     * Get a project
     *
     * @action businessExpenses/project/read
     */
    public function read_projectId_();

    /**
     * Get projects by customer identifier
     *
     * @action businessExpenses/project/readByCustomerId
     */
    public function read_customerId_Customer();

    /**
     * Record a new project
     * @param businessExpenses/project $project The project object to record
     *
     * @action businessExpenses/project/create
     */
    public function create($project);

    /**
     * Update a project
     * @param businessExpenses/project $project The project object to update
     *
     * @action businessExpenses/project/update
     */
    public function update($project);

    /**
     * Delete a project
     *
     * @action businessExpenses/project/delete
     */
    public function delete_projectId_();

    /**
     * Get the list project
     *
     * @action businessExpenses/project/queryProjects
     */
    public function readProjects_query_();
}

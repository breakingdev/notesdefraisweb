<?php

namespace bundle\businessExpenses;

interface statisticInterface
{
    /**
     * Get categories by services
     * @param date $dateMin
     * @param date $dateMax
     *
     * @action businessExpenses/statistic/getCategoriesByServices
     */
    public function readCategoriesbyservices($dateMin = null, $dateMax = null);
}

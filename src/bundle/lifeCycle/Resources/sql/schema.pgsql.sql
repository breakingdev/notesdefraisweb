-- Schema: lifeCycle

DROP SCHEMA IF EXISTS "lifeCycle" CASCADE;

CREATE SCHEMA "lifeCycle"
  AUTHORIZATION postgres;


-- Table: "lifeCycle"."journal"

-- DROP TABLE "lifeCycle"."journal";

CREATE TABLE "lifeCycle"."journal"
(
  "journalId" text NOT NULL,
  "previousJournalId" text,
  "timestamp" timestamp NOT NULL,
  "closingTimestamp" timestamp,
  PRIMARY KEY ("journalId"),
  FOREIGN KEY ("previousJournalId")
      REFERENCES "lifeCycle"."journal" ("journalId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: "lifeCycle"."event"

-- DROP TABLE "lifeCycle"."event";

CREATE TABLE "lifeCycle"."event"
(
  "eventId" text NOT NULL,
  "eventType" text NOT NULL,
  "timestamp" timestamp NOT NULL,
  "accountId" text,
  "objectClass" text NOT NULL,
  "objectId" text NOT NULL,
  "operationResult" boolean,
  "description" text,
  "eventInfo" text,
  PRIMARY KEY ("eventId")
)
WITH (
  OIDS=FALSE
);


-- Table: "lifeCycle"."eventFormat"

-- DROP TABLE "lifeCycle"."eventFormat";

CREATE TABLE "lifeCycle"."eventFormat"
(
  "type" text NOT NULL,
  "format" text NOT NULL,
  PRIMARY KEY ("type")
)
WITH (
  OIDS=FALSE
);

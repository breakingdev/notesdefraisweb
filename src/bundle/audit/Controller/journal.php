<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit\Controller;
/**
 * Controller for the audit trail journal
 *
 * @package Audit
 */
class journal
{
    protected $sdoFactory;

    protected $eventController;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Chain the last journal
     *
     * @return string The chained journal file name
     */
    public function chainJournal()
    {
        $toDate = \laabs::newTimestamp();

        $newJournal = \laabs::newInstance('recordsManagement/log');
        $newJournal->archiveId = \laabs::newId();
        $newJournal->type = "application";

        $previousJournal = $this->getLastJournal();
        if ($previousJournal) {
            $journalEndTimestamp = $previousJournal->toDate;
            $newJournal->previousJournalId = $previousJournal->archiveId;

            $events = $this->sdoFactory->find('audit/event', "eventDate > '$journalEndTimestamp' AND eventDate <= '$toDate'", null, "<eventDate");
            
            $newJournal->fromDate = $journalEndTimestamp;

        } else {
            // No previous journal, select all events
            $events = $this->sdoFactory->find('audit/event', "eventDate <= '$toDate'", null, "<eventDate");
            if (count($events) > 0) {
                $newJournal->fromDate = reset($events)->eventDate;
            } else {
                $newJournal->fromDate = \laabs::newTimestamp('1970-01-01');
            }
        } 

        $newJournal->toDate = $toDate;
        
        $tmpdir = \laabs::getTmpDir();

        $journalFilename = $tmpdir.DIRECTORY_SEPARATOR.(string) $newJournal->archiveId.".csv";
        $journalFile = fopen($journalFilename, "w");
        
        // First event : chain with previous journal
        $eventLine = array();
        $eventLine[0] = (string) $newJournal->archiveId;
        $eventLine[1] = (string) $newJournal->fromDate;
        $eventLine[2] = (string) $newJournal->toDate;

        // Write previous journal informations
        if ($previousJournal) {
            $eventLine[3] = (string) $previousJournal->archiveId;

            $documentController = \laabs::newController('documentManagement/document');
            $journalDocument = $documentController->getArchiveDocument($previousJournal->archiveId, true, false);
            
            $eventLine[4] = (string) $journalDocument->digitalResource->hashAlgorithm;
            $eventLine[5] = (string) $journalDocument->digitalResource->hash;
        }

        fputcsv($journalFile, $eventLine);

        // Write events
        foreach ($events as $event) {
            $eventLine = array();
            
            $eventLine[] = (string) $event->eventDate;
            $eventLine[] = (string) $event->accountId;
            $eventLine[] = (string) $event->path;
            $eventLine[] = (string) $event->status;

            fputcsv($journalFile, $eventLine);
        }

        fclose($journalFile);

        return $this->createJournalArchive($newJournal, $journalFilename);
    }

        /**
     * Create journal resource
     * @param lifeCycle/journal $journal     The journal
     * @param stream            $journalFile The journal file
     *
     * @return object $journalArchive
     */
    protected function createJournalArchive($journal, $journalFile)
    {
        $archiveController = \laabs::newController('recordsManagement/archive');
        $digitalResourceController = \laabs::newController('digitalResource/digitalResource');
        $orgController = \laabs::newController('organization/organization');

        // Create archive
        $archive = $archiveController->newArchive();

        $archive->archiveId = $journal->archiveId;
        $archive->accesRulesDuration = null;
        $archive->accesRulesCode = null;
        $archive->retentionDuration = '0D';
        $archive->finalDisposition = 'preserve';

        // Create resource
        $journalResource = $digitalResourceController->createFromFile($journalFile);
        $digitalResourceController->getHash($journalResource, "SHA256");

        // Add document
        $document = \laabs::newInstance('documentManagement/document');
        $document->archiveId = $archive->archiveId;
        $document->resId = $journalResource->resId;
        $document->type = "CDO";
        $document->digitalResource = $journalResource;
        
        $archive->document[] = $document;

        $archive->descriptionObject = $journal;
        $archive->descriptionId = $journal->archiveId;
        $archive->descriptionClass = 'recordsManagement/log';

        $depositorOrg = $orgController->getOrgsByRole('owner')[0];
        if (!isset($orgController->getOrgsByRole('owner')[0])) {
            throw \laabs::newException("audit/entryException", "Owner organization not found.");
        }

        $archive->originatorOrgRegNumber = (string) $depositorOrg->registrationNumber;

        $archive->serviceLevelReference = $archiveController->useServiceLevel("deposit", "logServiceLevel")->reference;

        return $archiveController->deposit($archive);
    }

    /**
     * Get the last usable journal
     *
     * @return audit/journal The journal object
     */
    public function getLastJournal()
    {
        $journals = $this->sdoFactory->find('recordsManagement/log', "type='application'", null, ">fromDate", 0, 1);

        if (empty($journals)) {
            return null;
        }

        $journal = end($journals);

        return $journal;
    }
}
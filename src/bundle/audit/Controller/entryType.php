<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit\Controller;
/**
 * Controller for the audit trail entries types
 * 
 * @package Audit
 */
class entryType
{

    protected $sdoFactory;

    /**
     * Constructor
     * @param \dependency\sdo\Factory $sdoFactory
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Get all entry types
     * 
     * @return audit/entryType[] The array of audit types
     */
    public function index()
    {
        $entryTypes = array();

        foreach ($this->sdoFactory->find('audit/entryType') as $entryType) {
            $entryTypes[$entryType->code] = $entryType;
        }

        return $entryTypes;
    }

    /**
     * Get one entry type
     * @param qname $code
     * 
     * @return audit/entryType
     */
    public function get($code)
    {
        return $this->sdoFactory->read('audit/entryType', $code);
    }

   

}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit;
/**
 * Interface for audit view
 */
interface entryInterface
{
    /**
     * Find entries for an identified object
     * @param qname $objectClass
     * @param id    $objectId
     * 
     *
     * @action audit/entry/byObject
     */
    public function read_objectClass__objectId_($objectClass, $objectId);
       
    /**
     * Get search form for entries
     * 
     * @action audit/entry/result
     */
    public function readSearch();
}
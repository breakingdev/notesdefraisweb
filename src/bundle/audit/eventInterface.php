<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle audit.
 *
 * Bundle audit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle audit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle audit.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace bundle\audit;

/**
 * Interface for audit
 */
interface eventInterface
{
    /**
     * Create new audit events
     * @param string $path      The path of called service
     * @param array  $variables The path variables
     * @param mixed  $input     The input data
     * @param mixed  $output    The output data
     * @param bool   $status    The result of action: success or failure (business exception)
     * @param mixed  $info      The info on caller process/client/system
     *
     * @action audit/event/add
     */
    public function create($path, array $variables=null, $input=null, $output=null, $status=false, $info=null);

    /**
     * Get search form for entries
     *@param string $eventType Type of event
     *@param date   $fromDate  Start date
     *@param date   $toDate    End date
     *@param string $accountId Id of account
     *@param string $event     Variables
     *
     * @action audit/event/search
     */
    public function readSearch($eventType = null, $fromDate = null, $toDate = null, $accountId = null, $event = null, $status = null);
    
    /**
     * Get search form for entries
     *
     * @action audit/event/getEvent
     */
    public function read_eventId_();

    /**
     * Chain the last journal
     *
     * @action audit/journal/chainJournal
     */
    public function createChainjournal();
}

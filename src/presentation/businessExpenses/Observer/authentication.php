<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle user.
 *
 * Bundle user is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle user is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle user.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Observer;

/**
 * Service for authentication check2
 *
 * @package User
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 * @chboob
 */
class authentication
{
    protected $sdoFactory;

    /**
     * Construct the observer
     * @param object $sdoFactory The user model
     */
    public function __construct(\dependency\sdo\Factory $sdoFactory)
    {
        $this->sdoFactory = $sdoFactory;
    }

    /**
     * Observer for user authentication
     * @param \core\Reflection\Command &$userCommand
     * @param array                    &$args
     *
     * @return auth/account
     * @subject LAABS_USER_COMMAND
     */
    public function check(&$userCommand, array &$args=null)
    {
        $account = null;

        // Check user story access
        $userStory = \laabs::presentation()->getUserStory($userCommand->userStory);

        if ($userStory->isPublic()) {
            return true;
        }

        if ($userStory->isPrivate()) {
            return false;
        }

        // Check authentication
        switch (true) {
            // Token authentication
            case ($account = \laabs::getToken("AUTH")):
                if (!$this->sdoFactory->exists('auth/account', array('accountName' => $account->accountName))) {
                    $userCommand->reroute('app/authentication/readUserPrompt');

                    return false;
                }

                $authentified = true;
                $userName = $account->accountName;

                break;

            // Request authentication
            case ($requestAuth = \core\Kernel\abstractKernel::get()->request->authentication):
                switch ($requestAuth::$mode) {
                    case LAABS_BASIC_AUTH:
                        try {
                            $account = \laabs::callService('auth/authentication/createUserlogin', $requestAuth->username, $requestAuth->password);
                        } catch (\Exception $e) {
                            throw $e;
                        }
                        break;

                    /*case LAABS_DIGEST_AUTH:
                        if ($this->authenticationService->logIn($requestAuth->username, $requestAuth->nonce, $requestAuth->uri, $requestAuth->response, $requestAuth->qop, $requestAuth->nc, $requestAuth->cnonce)) {
                            $token = $this->encrypt($_SESSION['dependency']['authentication']['credential']);
                        }
                        break;

                    case LAABS_APP_AUTH:
                        if (isset($_SERVER['LAABS_AUTH_TOKEN'])) {
                            $token = $_SERVER['LAABS_AUTH_TOKEN'];

                            $credential = $this->decrypt($token);
                            $_SESSION['dependency']['authentication']['credential'] = $credential;
                        }
                        break;
                    */
                }
                break;
        }

        if (!$account) {
            $userCommand->reroute('app/authentication/readUserPrompt');

            return false;
        }

        // Read user account information
        $account = $this->sdoFactory->read('auth/account', array('accountName' => $account->accountName));

        // Reset auth token to update expiration
        $authConfig = \laabs::configuration("auth");
        if (isset($authConfig['securityPolicy']['sessionTimeout'])) {
            $sessionTimeout = $authConfig['securityPolicy']['sessionTimeout'];
        } else {
            $sessionTimeout = 86400;
        }

        \laabs::setToken("AUTH", $account, $sessionTimeout);

        return $account;
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle organization.
 *
 * Bundle organization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle organization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle organization.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\organization;

/**
 * organization user function serializer
 *
 * @package Organization
 * @author  Maarch Alexis Ragot <alexis.ragot@maarch.org>
 */
class userFunction
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;
    protected $json;

    /**
     * Constructor
     * @param \dependency\html\Document   $view A new ready-to-use empty view
     * @param \dependency\json\JsonObject $json The json base object
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $this->view->translator;
        $this->translator->setCatalog('organization/userFunction');
    }

    /**
     * Serializer JSON for create method
     * @param array $userFunctions The array of userFunction object
     *
     * @return string
     */
    public function index($userFunctions)
    {
        $this->view->addContentFile("organization/userFunction/index.html");

        $table = $this->view->getElementById("userFunctionsList");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setUnsortableColumns(3);
        $dataTable->setUnsearchableColumns(3);

        $this->view->setSource("userFunctions", $userFunctions);
        $this->view->merge();
        $this->view->translate();

        return $this->view->saveHtml();
    }

    /**
     * Serializer JSON for create method
     * @param string $userFunctionId The user function identifier
     *
     * @return object JSON object with a status and message parameters
     */
    public function create($userFunctionId)
    {
        $this->json->message = "The user function has been created";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * Serializer JSON for read method
     *
     * @return object JSON object with a status and message parameters
     */
    public function delete()
    {
        $this->json->message = "The user function has been deleted";

        return $this->json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\organization\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }
}

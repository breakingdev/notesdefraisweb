<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle user.
 *
 * Bundle user is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle user is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle user.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\auth;

/**
 * user admin html serializer
 *
 * @package User
 * @author  Arnaud VEBER <arnaud.veber@maarch.org>
 */
class user
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    /**
     *
     */
    public $view;
    public $json;
    public $translator;

    /**
     * Constructor
     * @param object $view A new empty Html document
     */
    public function __construct(
        \dependency\html\Document $view,
        \dependency\json\JsonObject $json,
        \dependency\localisation\TranslatorInterface $translator)
    {

        $this->view = $view;

        $this->json = $json;
        $this->translator = $translator;
        $this->translator->setCatalog('auth/messages');
        $this->json->status = true;
    }

    /**
     * View for the users admin index panel
     * @param array  $users  An array of user objects to display
     * @param string $offset The offset
     * @param string $length The length
     *
     * @return string The html view string
     */
    public function indexHtml($users, $offset = 0, $length = 10)
    {
        $view = $this->view;

        $view->addContentFile("auth/userAccount/admin/index.html");
        $view->translate();

        $table = $view->getElementById("user_userList");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setUnsortableColumns(3, 4, 5);
        $dataTable->setUnsearchableColumns(3, 4, 5);
        
        $view->setSource('users', $users);

        $view->merge();

        return $view->saveHtml();
    }

    /**
     * View for the edit user form
     * @param user/user $user The user object
     *
     * @return string The html view string
     */
    public function edit($user)
    {
        $view = $this->view;

        $view->addContentFile("auth/userAccount/admin/edit.html");

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * View for the create user form
     * @param object $user The user object
     *
     * @return string The html view string
     */
    public function newUser($user)
    {
        $view = $this->view;

        $view->addContentFile("auth/userAccount/admin/edit.html");

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * View to see information on user
     * @param object $user The user object
     *
     * @return string The html view string
     */
    public function visualisation($user)
    {
        $view = $this->view;

        $view->addContentFile("auth/userAccount/admin/visualisation.html");

        if (\laabs::hasBundle('organization')) {
            $orgModel = \laabs::newInstance("organization/organization");

            $organizations = $orgModel->getOrganizationTree();
            $this->mergeOrganizations($organizations);
        }

        $view->setSource('user', $user);

        $view->merge();
        $view->translate();

        return $view->saveHtml();
    }

    /**
     * undocumented function
     *
     * @return void
     **/
    protected function mergeOrganizations($organizations)
    {
        $orgList = $this->view->getElementById("organizationList");

        foreach ($organizations as $organization) {
            $orgFragment = $this->view->createDocumentFragment();
            $orgFragment->appendHtmlFile("organization/organizationItem.html");

            $this->view->merge($orgFragment, $organization);

            $orgItem = $orgList->appendChild($orgFragment);

            $this->mergeOrgUnits($organization, $orgItem);
        }
    }

    protected function mergeOrgUnits($parent, $container)
    {
        $orgUnitList = $this->view->createElement('ul');
        $container->appendChild($orgUnitList);

        foreach ($parent->orgUnit as $orgUnit) {
            $orgUnitFragment = $this->view->createDocumentFragment();
            $orgUnitFragment->appendHtmlFile("organization/orgUnitItem.html");
            $this->view->merge($orgUnitFragment, $orgUnit);

            $orgUnitItem = $orgUnitList->appendChild($orgUnitFragment);

            $this->mergeOrgUnits($orgUnit, $orgUnitItem);
        }
    }

    //JSON
    /**
     * undocumented function
     *
     * @return void
     */
    public function addUser($user)
    {
        $json = $this->json;
        $json->message = "User added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function lock()
    {
        $json = $this->json;
        $json->message = "User locked";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function unlock()
    {
        $json = $this->json;
        $json->message = "User unlocked";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function enable()
    {
        $json = $this->json;
        $json->message = "User enable";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function disable()
    {
        $json = $this->json;
        $json->message = "User disable";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function setPassword()
    {
        $json = $this->json;
        $json->message = "Password has been changed";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    public function requirePasswordChange()
    {
        $json = $this->json;
        $json->message = "Request to changed password sent.";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Modify a user information
     *
     * @return array
     */
    public function updateUserInformation()
    {
        $this->json->message = "User updated";
        $this->json->message = $this->translator->getText($this->json->message);

        return $this->json->save();
    }

    /**
     * invalidUserInformationException
     * @param string $exception exception
     * @return void
     */
    public function invalidUserInformationException($exception)
    {
        $exception->message = $this->translator->getText($exception->message);
        $this->json->load($exception);
        $this->json->status = false;

        return $this->json->save();
    }
}

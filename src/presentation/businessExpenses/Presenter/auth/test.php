<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle user.
 *
 * Bundle user is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle user is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle user.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\auth;

/**
 * user admin html serializer
 *
 * @package test
 * @author  Arnaud VEBER <arnaud.veber@maarch.org>
 */
class test
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    /**
     *
     */
    public $view;
    public $json;
    public $translator;

    /**
     * Constructor
     * @param object $view A new empty Html document
     */
    public function __construct(
        \dependency\html\Document $view,
        \dependency\json\JsonObject $json,
        \dependency\localisation\TranslatorInterface $translator)
    {

        $this->view = $view;

        $this->json = $json;
        $this->translator = $translator;
        $this->translator->setCatalog('auth/messages');
        $this->json->status = true;
    }

    public function test($obj)
    {
        $view = $this->view;

        $view->addContentFile("seda/archiveTransfert.html");
        $view->translate();

        $size = 0;
        $nbObject = 0;
        
        foreach ($obj->archive as $archive) {
            foreach ($archive->document as $document) {
                $size = $size + $document->size->getValue();
                $nbObject++;
            }
            $archive->id= new \core\Type\Id();
        }

        foreach ($obj->archivalAgency->communication as $communication) {
            switch ($communication->channel->getValue()) {
                case 'TE' :
                    $communication->channel->setValue('Phone');
                    break;
                case 'FX' :
                    $communication->channel->setValue('Fax');
                    break;
                case 'EM' :
                    $communication->channel->setValue('Email');
                    break;
                case 'AL' :
                    $communication->channel->setValue('Cellular');
                    break;
            }
        }
        
        foreach ($obj->archivalAgency->contact as $contact) {
            foreach ($contact->communication as $communication) {
                switch ($communication->channel->getValue()) {
                    case 'TE' :
                        $communication->channel->setValue('Phone');
                        break;
                    case 'FX' :
                        $communication->channel->setValue('Fax');
                        break;
                    case 'EM' :
                        $communication->channel->setValue('Email');
                        break;
                    case 'AL' :
                        $communication->channel->setValue('Cellular');
                        break;
                }
            }
        }

        $obj->size = $size;
        $obj->nbObject = $nbObject;
        $view->setSource('message', $obj);
        $view->setSource('contact', $contact);

        $view->merge();

        return $view->saveHtml();
    }
}

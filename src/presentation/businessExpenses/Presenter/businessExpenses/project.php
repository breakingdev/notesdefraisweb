<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * project serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class project
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of project presenter
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/project');
    }

    /**
     * Get all projects
     * @param array $projects The array of projects object
     *
     * @return string
     */
    public function index($projects)
    {
        $this->view->addContentFile("businessExpenses/project/index.html");

        $table = $this->view->getElementById("listProject");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setUnsortableColumns(3);
        $dataTable->setUnsearchableColumns(3);

        $this->view->translate();

        $this->view->setSource('projects', $projects);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Edit a new or an existing project
     * @param businessExpenses/project $project The project to update or null value
     *
     * @return string
     */
    public function edit($project = null)
    {
        $this->view->addContentFile("businessExpenses/project/edit.html");

        $this->view->translate();

        if ($project == null) {
            $project = \laabs::newMessage("businessExpenses/project");
        }

        $customerController = \laabs::newController('businessExpenses/customer');
        $customers = $customerController->index();

        $this->view->setSource('customers', $customers);

        $this->view->setSource('project', $project);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Projects by customer
     * @param array $projects Array of project
     *
     * @return string
     */
    public function readByCustomer($projects)
    {
        $json = $this->json;
        $json->projects = $projects;

        return $json->save();
    }

    /**
     * Create a new project
     * @param string $projectId The project identifier
     *
     * @return string
     */
    public function create($projectId)
    {
        $json = $this->json;
        $json->message = "Project added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Update a project
     * @param string $projectId The project identifier
     *
     * @return string
     */
    public function update($projectId)
    {
        $json = $this->json;
        $json->message = "Project updated";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete a project
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Project deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function foreingKeyException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $this->translator->getText($exception->getMessage());
        $json->message = sprintf($json->message, $exception->count);

        return $json->save();
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * expense report serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class expenseReport
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $sdoFactory;

    public $json;

    public $translator;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document                    $view       The view
     * @param \dependency\sdo\Factory                      $sdoFactory The sdo factory
     * @param \dependency\json\JsonObject                  $json       The json dependency
     * @param \dependency\localisation\TranslatorInterface $translator The translator
     *
     */
    public function __construct(\dependency\html\Document $view, \dependency\sdo\Factory $sdoFactory, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;
        $this->sdoFactory = $sdoFactory;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/expenseReport');
    }

    /**
     * Get all expense reports
     * @param array $expenseReports The array of expense reports object
     *
     * @return string
     */
    public function index($expenseReports)
    {
        $this->view->addContentFile("businessExpenses/expenseReport/index.html");

        $table = $this->view->getElementById("listExpenseReport");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setSorting(array(array(2)));
        $dataTable->setUnsortableColumns(5);
        $dataTable->setUnsearchableColumns(5);


        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');

        foreach ($expenseReports as $expenseReport) {
            $expenseReport->statusTrad = $this->translator->getText($expenseReport->status);
            $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;
            $expenseReport->nbLines = $this->sdoFactory->count("businessExpenses/expenseLine", "expenseReportId='$expenseReport->expenseReportId'");
        }

        $this->view->setSource('expenseReports', $expenseReports);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Get all expense reports to validate
     * @param array $expenseReports The array of expense reports object
     *
     * @return string
     */
    public function validatorIndex($expenseReports)
    {
        $this->view->addContentFile("businessExpenses/expenseReportValidator/index.html");

        $table = $this->view->getElementById("listExpenseReportToValidate");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setSorting(array(array(3)));
        $dataTable->setUnsortableColumns(5);
        $dataTable->setUnsearchableColumns(5);

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $userAccountController = \laabs::newController('auth/userAccount');

        foreach ($expenseReports as $expenseReport) {
            $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;
            $expenseReport->personalName = $userAccountController->edit($expenseReport->personalId)->displayName;
        }

        $this->view->setSource('expenseReports', $expenseReports);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Get the history of expense reports for the validator
     * @param array $expenseReports The array of expense reports object
     *
     * @return string
     */
    public function validatorHistory($expenseReports)
    {
        $this->view->addContentFile("businessExpenses/expenseReportValidator/indexHistory.html");

        $table = $this->view->getElementById("listExpenseReportHistory");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setSorting(array(array(3)));
        $dataTable->setUnsortableColumns(6);
        $dataTable->setUnsearchableColumns(6);

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $userAccountController = \laabs::newController('auth/userAccount');

        foreach ($expenseReports as $expenseReport) {
            $expenseReport->statusTrad = $this->translator->getText($expenseReport->status);
            $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;
            $expenseReport->personalName = $userAccountController->edit($expenseReport->personalId)->displayName;
        }

        $this->view->setSource('expenseReports', $expenseReports);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Get the history of expense reports for the accountant
     *
     * @return string
     */
    public function accountantHistory()
    {
        $this->view->addContentFile("businessExpenses/expenseReportAccountant/historyForm.html");

        $this->view->translate();

        return $this->view->saveHtml();
    }

    /**
     * Get all expense reports to accounted
     * @param array $expenseReports The array of expense reports object
     *
     * @return string
     */
    public function accountantIndex($expenseReports)
    {
        $this->view->addContentFile("businessExpenses/expenseReportAccountant/index.html");

        $table = $this->view->getElementById("listExpenseReportToAccounted");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setSorting(array(array(3)));
        $dataTable->setUnsortableColumns(5);
        $dataTable->setUnsearchableColumns(5);

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $userAccountController = \laabs::newController('auth/userAccount');

        foreach ($expenseReports as $expenseReport) {
            $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;
            $expenseReport->personalName = $userAccountController->edit($expenseReport->personalId)->displayName;
        }

        $this->view->setSource('expenseReports', $expenseReports);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Edit a new or an existing expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report to update or null value
     *
     * @return string
     */
    public function edit($expenseReport = null)
    {
        $this->view->addContentFile("businessExpenses/expenseReport/edit.html");

        $this->view->translate();

        if ($expenseReport == null) {
            $expenseReport = \laabs::newMessage("businessExpenses/expenseReport");
        }

        $userPositionController = \laabs::newController('organization/userPosition');
        $expenseLineController = \laabs::newController('businessExpenses/expenseLine');
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');
        $customerController = \laabs::newController('businessExpenses/customer');

        // services
        $services = $userPositionController->getMyPositions();
        $this->view->setSource('services', $services);
        $element = $this->view->getElementById("serviceId");
        $this->view->merge($element);

        // business categories
        $businessCategories = $businessCategoryController->index(true);
        $this->view->setSource('businessCategories', $businessCategoryController->index());

        // expense lines
        $expenseLines = $this->getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReport->expenseReportId);

        // total amoutn
        $expenseReport->totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $this->view->setSource('expenseLines', $expenseLines);

        // customers
        $customers = $customerController->index();
        $this->view->setSource('customers', $customers);

        $this->view->setSource('expenseReport', $expenseReport);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * View an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report
     *
     * @return string
     */
    public function view($expenseReport)
    {
        $this->view->addContentFile("businessExpenses/expenseReport/visualization.html");

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $expenseLineController = \laabs::newController('businessExpenses/expenseLine');
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');

        // serviceName
        $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;

        // business categories
        $businessCategories = $businessCategoryController->index(true);
        // expense lines
        $expenseLines = $this->getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReport->expenseReportId);

        // total amoutn
        $expenseReport->totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $this->view->setSource('expenseLines', $expenseLines);

        $this->view->setSource('expenseReport', $expenseReport);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * View an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report
     *
     * @return string
     */
    public function viewValidator($expenseReport)
    {
        $this->view->addContentFile("businessExpenses/expenseReportValidator/visualization.html");

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $expenseLineController = \laabs::newController('businessExpenses/expenseLine');
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');
        $customerController = \laabs::newController('businessExpenses/customer');

        // serviceName
        $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;

        // business categories
        $businessCategories = $businessCategoryController->index(true);
        $this->view->setSource('businessCategories', $businessCategoryController->index());

        // expense lines
        $expenseLines = $this->getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReport->expenseReportId);

        // total amoutn
        $expenseReport->totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $this->view->setSource('expenseLines', $expenseLines);

        // customers
        $customers = $customerController->index();
        $this->view->setSource('customers', $customers);

        $this->view->setSource('expenseReport', $expenseReport);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * View an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report
     *
     * @return string
     */
    public function viewAccountant($expenseReport)
    {
        $this->view->addContentFile("businessExpenses/expenseReportAccountant/visualization.html");

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $expenseLineController = \laabs::newController('businessExpenses/expenseLine');
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');

        // serviceName
        $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;

        // business categories
        $businessCategories = $businessCategoryController->index(true);
        // expense lines
        $expenseLines = $this->getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReport->expenseReportId);

        // total amoutn
        $expenseReport->totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $this->view->setSource('expenseLines', $expenseLines);

        $this->view->setSource('expenseReport', $expenseReport);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * View an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report
     *
     * @return string
     */
    public function viewHistoryAccountant($expenseReport)
    {
        $this->view->addContentFile("businessExpenses/expenseReportAccountant/historyVisualization.html");

        $this->view->translate();

        $organizationController = \laabs::newController('organization/organization');
        $expenseLineController = \laabs::newController('businessExpenses/expenseLine');
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');

        // serviceName
        $expenseReport->serviceName = $organizationController->read($expenseReport->serviceId)->orgName;

        // business categories
        $businessCategories = $businessCategoryController->index(true);
        // expense lines
        $expenseLines = $this->getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReport->expenseReportId);

        // total amoutn
        $expenseReport->totalAmount = $this->getExpenseReportTotalAmount($expenseLines);

        $this->view->setSource('expenseLines', $expenseLines);

        $this->view->setSource('expenseReport', $expenseReport);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Search expense report for accountant
     * @param businessExpenses/expenseReport $expenseReports The expenses reports finded
     *
     * @return string
     */
    public function searchAccountantHistory($expenseReports)
    {
        $this->view->addContentFile("businessExpenses/expenseReportAccountant/searchResult.html");
        $userAccountController = \laabs::newController('auth/userAccount');

        $table = $this->view->getElementById("accountantHistoryList");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setSorting(array(array(1)));
        $dataTable->setUnsortableColumns(4);
        $dataTable->setUnsearchableColumns(4);

        foreach ($expenseReports as $expenseReport) {
            $expenseReport->personalName = $userAccountController->edit($expenseReport->personalId)->displayName;
        }

        $this->view->translate();

        $this->view->setSource('expenseReports', $expenseReports);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Export an expense report
     * @param resource $resource The resource to export
     *
     * @return string
     */
    public function accountantExport($resource)
    {
        $stream = fopen($resource, 'r');
        $contents = stream_get_contents($stream, -1, 0);

        \laabs::setResponseType("application/zip");
        $response = \laabs::kernel()->response;
        $response->setHeader("Content-Disposition", "attachements; filename=expenseReport.zip");

        return $contents;
    }

    /**
     * Create a new expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function create($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report added";
        $json->message = $this->translator->getText($json->message);
        $json->expenseReportId = $expenseReportId;

        return $json->save();
    }

    /**
     * Update an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function update($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report updated";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Send an expense report in validation
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function sendInValidation($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report send in validation";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete an expense report
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Expense report deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Validate an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function validate($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report has been validated";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Reject an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function reject($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report has been rejected";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Close an expense report
     * @param string $expenseReportId The expense report identifier
     *
     * @return string
     */
    public function close($expenseReportId)
    {
        $json = $this->json;
        $json->message = "Expense report has been closed";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    private function getReferencedExpenseLines($expenseLineController, $businessCategories, $expenseReportId)
    {
        $expenseLines = $expenseLineController->index($expenseReportId);
        foreach ($expenseLines as $key => $expenseLine) {
            foreach ($businessCategories as $key => $businessCategory) {
                if ($expenseLine->businessCategoryId == $businessCategory->businessCategoryId) {
                    $expenseLine->category = $businessCategory->description;
                }
            }
        }

        return $expenseLines;
    }

    private function getExpenseReportTotalAmount($expenseLines)
    {
        $total = 0;

        if (!is_array($expenseLines)) {
            return $total;
        }

        foreach ($expenseLines as $key => $expenseLine) {
            $total += $expenseLine->amount->__toFLoat();
        }

        return number_format($total, 2);
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * expense line serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class expenseLine
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/expenseLine');
    }

    /**
     * Edit an existing expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line
     *
     * @return string
     */
    public function read($expenseLine)
    {
        $businessCategoryController = \laabs::newController('businessExpenses/businessCategory');
        $customerController = \laabs::newController('businessExpenses/customer');
        $projectController = \laabs::newController('businessExpenses/project');

        if (!empty($expenseLine->customerId)) {
            $expenseLine->customerName = $customerController->read($expenseLine->customerId)->name;
        }
        if (!empty($expenseLine->projectId)) {
            $expenseLine->projectName = $projectController->read($expenseLine->projectId)->name;
        }

        $businessCategory = $businessCategoryController->read($expenseLine->businessCategoryId);
        $expenseLine->businessCategoryName = $businessCategory->description;
        $expenseLine->businessCategoryFormType = $businessCategory->formType;
        $expenseLine->date = (string) $expenseLine->date;

        $expenseLine->file = base64_encode($expenseLine->file);

        $json = $this->json;
        $json->expenseLine = $expenseLine;

        return $json->save();
    }

    /**
     * Get an expense line resource
     * @param businessExpenses/resource $resource The expense line resource
     *
     * @return string
     */
    public function getResource($resource)
    {
        $contents = $resource->getContents();
        $mimetype = $resource->mimetype;

        \laabs::setResponseType($mimetype);
        $response = \laabs::kernel()->response;
        $response->setHeader("Content-Disposition", "inline; filename=expense");

        return $contents;
    }

    /**
     * Create a new expense line
     * @param string $expenseLineId The expense line identifier
     *
     * @return string
     */
    public function create($expenseLineId)
    {
        $json = $this->json;
        $json->message = "Expense line added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Create a new regularization line
     * @param string $expenseLineId The regularization line identifier
     *
     * @return string
     */
    public function createRegularizationLine($expenseLineId)
    {
        $json = $this->json;
        $json->message = "Regularization line added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Update an expense line
     * @param string $expenseLineId The expense line identifier
     *
     * @return string
     */
    public function update($expenseLineId)
    {
        $json = $this->json;
        $json->message = "Expense line updated";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete an expense line
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Expense line deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }
}

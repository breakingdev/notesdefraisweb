<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * businessCategory serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class mileageAllowanceScale
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/mileageAllowanceScale');
    }

    /**
     * Get all mileag allowance scale
     * @param array $mileageAllowances The array of mileage allowance scale object
     *
     * @return string
     */
    public function index($mileageAllowances)
    {
        $this->view->addContentFile("businessExpenses/mileageAllowanceScale/index.html");

        $table = $this->view->getElementById("listMileageAllowanceScale");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");
        $dataTable->setUnsortableColumns(3, 4);
        $dataTable->setUnsearchableColumns(3, 4);

        $this->view->translate();

        $this->view->setSource('mileageAllowances', $mileageAllowances);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Create a new mileage allowance
     * @param string $mileageAllowanceScaleId The mileage allowance identifier
     *
     * @return string
     */
    public function create($mileageAllowanceScaleId)
    {
        $json = $this->json;
        $json->message = "Mileage allowance added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete a mileage allowance
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Mileage allowance deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }
}

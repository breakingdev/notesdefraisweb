<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * customer serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class customer
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of customer presenter
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/customer');
    }

    /**
     * Get all customers
     * @param array $customers The array of customers object
     *
     * @return string
     */
    public function index($customers)
    {
        $this->view->addContentFile("businessExpenses/customer/index.html");

        $table = $this->view->getElementById("listCustomer");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setUnsortableColumns(2);
        $dataTable->setUnsearchableColumns(2);

        $this->view->translate();

        $this->view->setSource('customers', $customers);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Edit a new or an existing customer
     * @param businessExpenses/customer $customer The customer to update or null value
     *
     * @return string
     */
    public function edit($customer = null)
    {
        $this->view->addContentFile("businessExpenses/customer/edit.html");

        $this->view->translate();

        if ($customer == null) {
            $customer = \laabs::newMessage("businessExpenses/customer");
        }

        $this->view->setSource('customer', $customer);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Create a new customer
     * @param string $customerId The customer identifier
     *
     * @return string
     */
    public function create($customerId)
    {
        $json = $this->json;
        $json->message = "Customer added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Update a customer
     * @param string $customerId The customer identifier
     *
     * @return string
     */
    public function update($customerId)
    {
        $json = $this->json;
        $json->message = "Customer updated";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete a customer
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Customer deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function foreingKeyException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $this->translator->getText($exception->getMessage());
        $json->message = sprintf($json->message, $exception->count);

        return $json->save();
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * statitic serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class statistic
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of statitic presenter
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/statistic');
    }

    /**
     * Get statistic
     *
     * @return string
     */
    public function index()
    {
        $this->view->addContentFile("businessExpenses/statistic/index.html");

        $this->view->translate();

        $colors = array(
                "#F7464A",
                "#46BFBD",
                "#FDB45C",
                "#949FB1",
                "#4D5360",
                "#B19CD9",
                "#B39EB5",
            );

        $this->view->setSource('colors', $colors);
        $this->view->translate();
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Get categories by services
     * @param mixte $data The data
     * @return string
     */
    public function getCategoriesbyservices($data)
    {
        $json = $this->json;
        $json->data = $data;

        return $json->save();
    }

    /**
     * invalidValueException
     * @param \bundle\businessExpenses\Exception\invalidValueException $exception The exception
     * @return string
     */
    public function invalidValueException($exception)
    {
        $json = $this->json;
        $this->json->status = false;
        $json->message = $exception->getMessage();
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }
}

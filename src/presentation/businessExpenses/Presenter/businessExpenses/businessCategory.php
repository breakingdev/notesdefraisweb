<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter\businessExpenses;

/**
 * businessCategory serializer html
 *
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
class businessCategory
{
    use \presentation\businessExpenses\Presenter\exceptions\exceptionTrait;

    public $view;

    public $json;

    public $translator;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view, \dependency\json\JsonObject $json, \dependency\localisation\TranslatorInterface $translator)
    {
        $this->view = $view;

        $this->json = $json;
        $this->json->status = true;

        $this->translator = $translator;
        $this->translator->setCatalog('businessExpenses/businessCategory');
    }

    /**
     * Get all business categories
     * @param array $businessCategories The array of business categories object
     *
     * @return string
     */
    public function index($businessCategories)
    {
        $this->view->addContentFile("businessExpenses/businessCategory/index.html");
        
        $table = $this->view->getElementById("listBusinessCategory");
        $dataTable = $table->plugin['dataTable'];
        $dataTable->setPaginationType("full_numbers");

        $dataTable->setUnsortableColumns(3);
        $dataTable->setUnsearchableColumns(3);

        $this->view->translate();

        foreach ($businessCategories as $businessCategory) {
            $businessCategory->formType = $this->translator->getText($businessCategory->formType);
        }

        $this->view->setSource('businessCategories', $businessCategories);
        $this->view->merge();

        return $this->view->saveHtml();
    }

    /**
     * Edit a new business category
     *
     * @return string
     */
    public function edit()
    {
        $this->view->addContentFile("businessExpenses/businessCategory/edit.html");

        $this->view->translate();

        return $this->view->saveHtml();
    }

    /**
     * Create a new business category
     * @param string $businessCategoryId The business category identifier
     *
     * @return string
     */
    public function create($businessCategoryId)
    {
        $json = $this->json;
        $json->message = "Business category added";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }

    /**
     * Delete a business category
     * @return string
     */
    public function delete()
    {
        $json = $this->json;
        $json->message = "Business category deleted";
        $json->message = $this->translator->getText($json->message);

        return $json->save();
    }
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of bundle businessExpenses.
 *
 * Bundle businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bundle businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\Presenter;

/**
 * Exception serializer html
 *
 */
class Exception
{
    public $view;

    /**
     * Constuctor of welcomePage html serializer
     * @param \dependency\html\Document $view The view
     */
    public function __construct(\dependency\html\Document $view)
    {
        $this->view = $view;
    }

    /**
     * Display error
     * @param Exception $exception
     *
     * @return string
     */
    public function Exception($exception)
    {
        //$this->view->addHeaders();
        //$this->view->useLayout();
        $this->view->addContentFile("businessExpenses/welcome/error.html");

        $this->view->setSource('error', $exception);
        $this->view->merge();

        $this->view->translate();

        return $this->view->saveHtml();
    }
}

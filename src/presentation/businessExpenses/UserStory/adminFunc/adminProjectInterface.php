<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Admin project
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminProjectInterface
{
    /**
     * List all projects
     *
     * @return businessExpenses/project/index
     * @uses businessExpenses/project/readIndex
     */
    public function readProjectIndex();

    /**
     * Get view to record an expense report object
     *
     * @return businessExpenses/project/edit
     */
    public function readProjectNew();

    /**
     * Get a project
     *
     * @uses businessExpenses/project/read_projectId_
     *
     * @return businessExpenses/project/edit
     */
    public function readProject_projectId_();

    /**
     * Get a project
     *
     * @uses businessExpenses/project/read_customerId_Customer
     *
     * @return businessExpenses/project/readByCustomer
     */
    public function readProject_customerId_Customer();

    /**
     * Record a new project
     * @param businessExpenses/project $project The project object to record
     *
     * @uses businessExpenses/project/create
     *
     * @return businessExpenses/project/create
     */
    public function createProject($project);

    /**
     * Update a project
     * @param businessExpenses/project $project The expense report object to update
     *
     * @uses businessExpenses/project/update
     *
     * @return businessExpenses/project/update
     */
    public function updateProject($project);

    /**
     * Delete a project
     *
     * @uses businessExpenses/project/delete_projectId_
     *
     * @return businessExpenses/project/delete
     */
    public function deleteProject_projectId_();

    /**
     * Get the list of available projects
     *
     * @uses businessExpenses/project/readProjects_query_
     */
    public function readProjectQuery_query_();
}

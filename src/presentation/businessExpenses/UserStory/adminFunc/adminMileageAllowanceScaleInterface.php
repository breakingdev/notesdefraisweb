<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Admin mileage allowance scale
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminMileageAllowanceScaleInterface
{
    /**
     * List all mileage allowance scale
     *
     * @return businessExpenses/mileageAllowanceScale/index
     * @uses businessExpenses/mileageAllowanceScale/readIndex
     */
    public function readMileageallowancescaleIndex();

    /**
     * Record a new mileage allowance
     * @param businessExpenses/mileageAllowanceScale $mileageAllowanceScale The mileage allowance object to record
     *
     * @uses businessExpenses/mileageAllowanceScale/create
     *
     * @return businessExpenses/mileageAllowanceScale/create
     */
    public function createMileageallowancescale($mileageAllowanceScale);

    /**
     * Delete a mileage allowance
     *
     * @uses businessExpenses/mileageAllowanceScale/delete_mileageAllowanceScaleId_
     *
     * @return businessExpenses/mileageAllowanceScale/delete
     */
    public function deleteMileageallowancescale_mileageAllowanceScaleId_();
}

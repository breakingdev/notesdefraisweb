<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of maarchRM.
 *
 * maarchRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * maarchRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * User story admin organization
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface AdminOrganizationInterface
{
        /**
     * Get the organizations' index
     *
     * @return organization/orgTree/index
     * 
     * @uses organization/organization/readTree
     * @uses organization/orgType/readList
     * @uses organization/orgRole/readList
     */
    public function readOrganizationIndex();

    /**
     * Get the organizations' index
     *
     * @return organization/orgTree/getTree
     * 
     * @uses organization/organization/readTree
     */
    public function readOrganizationtree();

    /**
     * Add an organization
     * @param organization/organization $organization The organization object to add
     *
     * @return organization/orgTree/addOrganization
     * 
     * @uses organization/organization/create
     */
    public function createOrganization($organization);

    /**
     * Edit an organization
     * 
     * @uses organization/organization/read_orgId_
     */
    public function readOrganization_orgId_();

    /**
     * Update an organization
     * @param object $organization The organization object to update
     *
     * @return organization/orgTree/modifyOrganization
     * 
     * @uses organization/organization/update_orgId_
     */
    public function updateOrganization_orgId_($organization);

    /**
     * Update an organization
     * @param string $newParentOrgId The new parent organization identifier
     * @param string $newOwnerOrgId  The new owner organization identifier
     *
     * @return organization/orgTree/modifyOrganization
     * 
     * @uses organization/organization/updateMove_orgId_
     */
    public function updateMoveorganization_orgId_($newParentOrgId, $newOwnerOrgId);

    /**
     * Delete an organization
     *
     * @return organization/orgTree/deleteOrganization
     * 
     * @uses organization/organization/delete_orgId_
     */
    public function deleteOrganization_orgId_($orgId);

    /**
     * Get the list of persons of a given organization unit
     * 
     * @uses organization/organization/readUserpositions_orgId_
     */
    public function readOrganizationUserposition_orgId_();

     /**
     * Add a user position to an organization
     * 
     * @return organization/orgTree/addUserPosition
     *
     * @uses organization/organization/createUserposition_orgId__userAccountId_
     */
    public function createOrganizationUserposition_orgId__userAccountId_($function = null);

    /**
     * Set a default userPosition 
     * 
     * @return organization/orgTree/setDefaultPosition
     *
     * @uses organization/organization/updateSetdefaultposition_orgId__userAccountId_
     */
    public function updateOrganizationSetdefaultposition_orgId__userAccountId_();

    /**
     * Remove a person's position
     * @param string $positionId The position of the person
     *
     * @return organization/orgTree/deleteUserPosition
     * @uses organization/organization/deleteUserposition_orgId__userAccountId_
     */
    public function deleteOrganizationUserposition_orgId__userAccountId_($positionId);

    /**
     * ORGANIZATION CONTACT
     */

    /**
     * Add an orgnization contact
     * @param bool $isSelf
     *
     * @return organization/orgContact/create
     * @uses organization/organization/create_orgId_Contact_contactId_
     */
    public function createOrganization_orgId_Contact_contactId_($isSelf);

    /**
     * Get all organization contacts by organization identifier
     *
     * @return organization/orgContact/read
     * @uses organization/organization/read_orgId_Contacts
     */
    public function readOrganizationOrgcontact_orgId_Org();

    /**
     * Get all organization contacts by contact identifier
     *
     * @return organization/orgContact/read
     * @uses organization/orgContact/readOrgcontact_contactId_Contact
     */
    public function readOrganizationOrgcontact_contactId_Contact();

    /**
     * Delete organization contact
     * @param organisation/orgContact $orgContact The organisation contact to record
     *
     * @return organization/orgContact/delete
     * @uses organization/orgContact/deleteOrgcontact
     */
    public function deleteOrgcontact($orgContact);

    /**
     * Remove a contact's position
     * @param string $contactId The position of the contact
     *
     * @return organization/orgTree/deleteContactPosition
     * @uses organization/organization/deleteContactposition_orgId__contactId_
     */
    public function deleteOrganizationContactposition_orgId__contactId_($contactId);

    /**
     * Get the organization types
     *
     * @return organization/orgType/index
     * @uses organization/orgType/readList
     */
    public function readOrganizationtypeIndex();

    /**
     * Add an org type
     * @param organization/orgType $orgType the orgType to create
     *
     * @return organization/orgType/createOrganizationtype
     * @uses organization/orgType/create
     */
    public function createOrganizationtype($orgType);
    
    /**
     * Edit an org type 
     *
     * @uses organization/orgType/read_code_
     */
    public function readOrganizationtype_code_();
    
    /**
     * Update an org type
     * @param organization/orgType $orgType The orgType to update
     *
     * @return organization/orgType/updateOrganizationtype
     * @uses organization/orgType/update_code_
     */
    public function updateOrganizationtype_code_($orgType);

    /**
     * Get all contacts
     *
     * @return contact/contact/index
     * @uses contact/contact/readIndex
     */
    public function readContactIndex();

    /**
     * Create contact
     * @param contact/contact $contact The contact object to create
     *
     * @return contact/contact/create
     * @uses contact/contact/create
     */
    public function createContact($contact);

    /**
     * Get form to add a new contact
     *
     * @return contact/contact/newContact
     */
    public function readContactNew();

    /**
     * Read contact
     *
     * @return contact/contact/newContact
     * @uses contact/contact/read_contactId_
     */
    public function readContact_contactId_();

    /**
     * Update a contact
     * @param contact/contact $contact The contact object to update
     *
     * @return contact/contact/update
     * @uses contact/contact/update_contactId_
     */
    public function updateContact_contactId_($contact);

    /**
     * Delete a contact
     *
     * @return contact/contact/delete
     * @uses contact/contact/delete_contactId_
     */
    public function deleteContact_contactId_();

    /**
     * CONTACT ADDRESS
     */

    /**
     * Create a contact address
     * @param contact/address $address The address to add
     *
     * @return contact/contact/createAddress
     * @uses contact/contact/create_contactId_Address
     */
    public function createContact_contactId_Address($address);

    /**
     * Get an address
     *
     * @return contact/contact/readAddress
     * @uses contact/contact/readAddress_addressId_
     */
    public function readContactaddress_addressId_();

    /**
     * Modify an address
     * @param contact/address $address The address to update
     *
     * @return contact/contact/updateAddress
     * @uses contact/contact/updateAddress_addressId_
     */
    public function updateContactaddress_addressId_($address);

    /**
     * Delete an address
     *
     * @return contact/contact/deleteAddress
     * @uses contact/contact/deleteAddress_addressId_
     */
    public function deleteContactaddress_addressId_();
    
    /**
     * Record a new communication
     * @param contact/communication $communication The communication to record
     *
     * @return contact/contact/createCommunication
     * @uses contact/contact/create_contactId_Communication
     */
    public function createContactcommunication_contactId_communication($communication);

    /**
     * Get a communication
     *
     * @return contact/contact/readCommunication
     * @uses contact/contact/readCommunication_communicationId_
     */
    public function readContactcommunication_communicationId_();

    /**
     * Modify a communication
     * @param contact/communication $communication The communication to modify
     *
     * @return contact/contact/updateCommunication
     * @uses contact/contact/updateCommunication_communicationId_
     */
    public function updateContactcommunication_communicationId_($communication);

    /**
     * Delete a communication
     *
     * @return contact/contact/deleteCommunication
     * @uses contact/contact/deleteCommunication_communicationId_
     */
    public function deleteContactcommunication_communicationId_();

}
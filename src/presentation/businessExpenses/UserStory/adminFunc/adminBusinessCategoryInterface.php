<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Admin business category
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminBusinessCategoryInterface
{
    /**
     * List all business categories
     *
     * @return businessExpenses/businessCategory/index
     * @uses businessExpenses/businessCategory/readIndex
     */
    public function readBusinesscategoryIndex();

    /**
     * Prepare an empty buisiness category object
     *
     * @return businessExpenses/businessCategory/edit
     */
    public function readBusinesscategoryNew();

    /**
     * Record a new business category
     * @param businessExpenses/businessCategory $businessCategory The business category object to record
     *
     * @uses businessExpenses/businessCategory/create
     *
     * @return businessExpenses/businessCategory/create
     */
    public function createBusinesscategory($businessCategory);

    /**
     * Delete a business category
     *
     * @uses businessExpenses/businessCategory/delete_businessCategoryId_
     *
     * @return businessExpenses/businessCategory/delete
     */
    public function deleteBusinesscategory_businessCategoryId_();
}
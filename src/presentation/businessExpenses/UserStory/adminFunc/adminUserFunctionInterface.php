<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Admin user function
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminUserFunctionInterface
{
    /**
     * List all user function
     *
     * @return organization/userFunction/index
     * @uses organization/userFunction/readIndex
     */
    public function readUserfunctionIndex();

    /**
     * Record a new user function
     * @param organization/userFunction $userFunction The user function object to record
     *
     * @uses organization/userFunction/create
     *
     * @return organization/userFunction/create
     */
    public function createUserfunction($userFunction);

    /**
     * Delete a business category
     *
     * @uses organization/userFunction/delete_userFunctionId_
     *
     * @return organization/userFunction/delete
     */
    public function deleteUserfunction_userFunctionId_();
}
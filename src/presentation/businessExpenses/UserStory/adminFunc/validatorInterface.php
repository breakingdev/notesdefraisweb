<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Validator interface
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface validatorInterface
{
    //****************************************************************
    //*******************      Expense report       ******************
    //****************************************************************

    /**
     * List all expense reports to validate
     *
     * @uses businessExpenses/expenseReport/readValidatorIndex
     *
     * @return businessExpenses/expenseReport/validatorIndex
     */
    public function readExpensereportValidatorIndex();

    /**
     * Validate an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_Validate
     *
     * @return businessExpenses/expenseReport/validate
     */
    public function readExpensereport_expenseReportId_Validate();

    /**
     * Reject an expense report
     * @param businessExpenses/rejectExpenseReport $rejectExpenseReport The expense report object to update
     * @uses businessExpenses/expenseReport/updateReject
     *
     * @return businessExpenses/expenseReport/reject
     */
    public function updateExpensereportReject();

    /**
     * View an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_
     *
     * @return businessExpenses/expenseReport/viewValidator
     */
    public function readExpensereport_expenseReportId_viewvalidator();

    /**
     * List the history for the validator
     *
     * @uses businessExpenses/expenseReport/readValidatorHistory
     *
     * @return businessExpenses/expenseReport/validatorHistory
     */
    public function readExpensereportValidatorHistory();

    //****************************************************************
    //*******************      Expenses lines       ******************
    //****************************************************************

    /**
     * Get an expense line
     *
     * @uses businessExpenses/expenseLine/read_expenseLineId_
     *
     * @return businessExpenses/expenseLine/read
     */
    public function readExpenseline_expenseLineId_();

    /**
     * Get a resource of expense line
     *
     * @uses businessExpenses/expenseLine/readResource_expenseLineId_
     *
     * @return businessExpenses/expenseLine/getResource
     */
    public function readExpenselineResource_expenseLineId_();

    //****************************************************************
    //**********************       Project       *********************
    //****************************************************************

}

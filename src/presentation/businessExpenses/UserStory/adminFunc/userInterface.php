<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * user interface
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface userInterface
{
    //****************************************************************
    //*******************      Expense report       ******************
    //****************************************************************
    /**
     * List all expense reports
     *
     * @return businessExpenses/expenseReport/index
     * @uses businessExpenses/expenseReport/readIndex
     */
    public function readExpensereportIndex();

    /**
     * Get view to record an expense report object
     *
     * @return businessExpenses/expenseReport/edit
     */
    public function readExpensereportNew();

    /**
     * Get an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_
     *
     * @return businessExpenses/expenseReport/edit
     */
    public function readExpensereport_expenseReportId_();

    /**
     * Record a new expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report object to record
     *
     * @uses businessExpenses/expenseReport/create
     *
     * @return businessExpenses/expenseReport/create
     */
    public function createExpensereport($expenseReport);

    /**
     * Update an expense report
     * @param businessExpenses/expenseReport $expenseReport The expense report object to update
     *
     * @uses businessExpenses/expenseReport/update
     *
     * @return businessExpenses/expenseReport/update
     */
    public function updateExpensereport($expenseReport);

    /**
     * Delete an expense report
     *
     * @uses businessExpenses/expenseReport/delete_expenseReportId_
     *
     * @return businessExpenses/expenseReport/delete
     */
    public function deleteExpensereport_expenseReportId_();

    /**
     * Send expense report in validation
     *
     * @uses businessExpenses/expenseReport/updateSendinvalidation_expenseReportId_
     *
     * @return businessExpenses/expenseReport/sendInValidation
     */
    public function updateExpensereport_expenseReportId_Tovalidate();

    /**
     * View an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_
     *
     * @return businessExpenses/expenseReport/view
     */
    public function readExpensereport_expenseReportId_view();

    //****************************************************************
    //*******************      Expenses lines       ******************
    //****************************************************************

    /**
     * Get an expense line
     *
     * @uses businessExpenses/expenseLine/read_expenseLineId_
     *
     * @return businessExpenses/expenseLine/read
     */
    public function readExpenseline_expenseLineId_();

    /**
     * Get a resource of expense line
     *
     * @uses businessExpenses/expenseLine/readResource_expenseLineId_
     *
     * @return businessExpenses/expenseLine/getResource
     */
    public function readExpenselineResource_expenseLineId_();

    /**
     * Record a new expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line object to record
     *
     * @uses businessExpenses/expenseLine/create
     *
     * @return businessExpenses/expenseLine/create
     */
    public function createExpenseline($expenseLine);

    /**
     * Update an expense line
     * @param businessExpenses/expenseLine $expenseLine The expense line object to update
     *
     * @uses businessExpenses/expenseLine/update
     *
     * @return businessExpenses/expenseLine/update
     */
    public function updateExpenseline($expenseLine);

    /**
     * Delete an expense line
     *
     * @uses businessExpenses/expenseLine/delete_expenseLineId_
     *
     * @return businessExpenses/expenseLine/delete
     */
    public function deleteExpenseline_expenseLineId_();


    //****************************************************************
    //**********************       Project       *********************
    //****************************************************************

    /**
     * Get a project
     *
     * @uses businessExpenses/project/read_customerId_Customer
     *
     * @return businessExpenses/project/readByCustomer
     */
    public function readProject_customerId_Customer();
}

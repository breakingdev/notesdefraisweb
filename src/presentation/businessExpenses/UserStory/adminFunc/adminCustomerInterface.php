<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Admin customer
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminCustomerInterface
{
    /**
     * List all customers
     *
     * @return businessExpenses/customer/index
     * @uses businessExpenses/customer/readIndex
     */
    public function readCustomerIndex();

    /**
     * Get view to record an expense report object
     *
     * @return businessExpenses/customer/edit
     */
    public function readCustomerNew();

    /**
     * Get a customer
     *
     * @uses businessExpenses/customer/read_customerId_
     *
     * @return businessExpenses/customer/edit
     */
    public function readCustomer_customerId_();

    /**
     * Record a new customer
     * @param businessExpenses/customer $customer The customer object to record
     *
     * @uses businessExpenses/customer/create
     *
     * @return businessExpenses/customer/create
     */
    public function createCustomer($customer);

    /**
     * Update a customer
     * @param businessExpenses/customer $customer The expense report object to update
     *
     * @uses businessExpenses/customer/update
     *
     * @return businessExpenses/customer/update
     */
    public function updateCustomer($customer);

    /**
     * Delete a customer
     *
     * @uses businessExpenses/customer/delete_customerId_
     *
     * @return businessExpenses/customer/delete
     */
    public function deleteCustomer_customerId_();

    /**
     * Get the list of available customers
     *
     * @uses businessExpenses/customer/readCustomers_query_
     */
    public function readCustomerQuery_query_();
}

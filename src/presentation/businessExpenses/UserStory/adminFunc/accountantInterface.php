<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * Accountant interface
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface accountantInterface
{
    //****************************************************************
    //*******************      Expense report       ******************
    //****************************************************************

    /**
     * List all expense reports to acconted
     *
     * @uses businessExpenses/expenseReport/readAccountantIndex
     *
     * @return businessExpenses/expenseReport/accountantIndex
     */
    public function readExpensereportAccountantIndex();

    /**
     * Close an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_Close
     *
     * @return businessExpenses/expenseReport/close
     */
    public function readExpensereport_expenseReportId_Close();

    /**
     * View an expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_
     *
     * @return businessExpenses/expenseReport/viewAccountant
     */
    public function readExpensereport_expenseReportId_viewaccountant();

    /**
     * View a closed expense report
     *
     * @uses businessExpenses/expenseReport/read_expenseReportId_
     *
     * @return businessExpenses/expenseReport/viewHistoryAccountant
     */
    public function readExpensereport_expenseReportId_viewhistoryaccountant();

    /**
     * Export an expense report
     *
     * @uses businessExpenses/expenseReport/readAccountantExport_expenseReportId_
     *
     * @return businessExpenses/expenseReport/accountantExport
     */
    public function readExpensereportAccountantExport_expenseReportId_();

    /**
     * List the history of the accountant
     *
     * @return businessExpenses/expenseReport/accountantHistory
     */
    public function readExpensereportAccountantHistory();

    /**
     * List the history of the accountant
     *
     * @uses businessExpenses/expenseReport/readAccountantSearchhistory
     *
     * @return businessExpenses/expenseReport/searchAccountantHistory
     */
    public function readExpensereportAccountantSearchhistory($beginDateMin = false, $beginDateMax = false, $endDateMin = false, $endDateMax = false);

    //****************************************************************
    //*******************      Expenses lines       ******************
    //****************************************************************

    /**
     * Get an expense line
     *
     * @uses businessExpenses/expenseLine/read_expenseLineId_
     *
     * @return businessExpenses/expenseLine/read
     */
    public function readExpenseline_expenseLineId_();

    /**
     * Get a resource of expense line
     *
     * @uses businessExpenses/expenseLine/readResource_expenseLineId_
     *
     * @return businessExpenses/expenseLine/getResource
     */
    public function readExpenselineResource_expenseLineId_();

    /**
     * Record a new regularization line
     * @param businessExpenses/regulationLine $regularizationLine The regularization line object to record
     *
     * @uses businessExpenses/expenseLine/createRegularizationline
     *
     * @return businessExpenses/expenseLine/createRegularizationLine
     */
    public function createRegularizationline($regularizationLine);

    //****************************************************************
    //**********************       Project       *********************
    //****************************************************************

}

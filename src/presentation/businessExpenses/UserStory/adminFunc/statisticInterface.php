<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of businessExpenses.
 *
 * businessExpenses is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * businessExpenses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle businessExpenses.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * statistic interface
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface statisticInterface
{
    /**
     * Get statistic page
     *
     * @return businessExpenses/statistic/index
     */
    public function readStatistic();

    /**
     * Get categories by services
     *
     * @uses businessExpenses/statistic/readCategoriesbyservices
     * @return businessExpenses/statistic/getCategoriesbyservices
     */
    public function readStatisticCategoriesbyservices();
}

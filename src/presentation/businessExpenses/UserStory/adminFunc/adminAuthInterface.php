<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of maarchRM.
 *
 * maarchRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * maarchRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bundle digitalResource.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace presentation\businessExpenses\UserStory\adminFunc;

/**
 * User story admin authorization
 * @author Alexis Ragot <alexis.ragot@maarch.org>
 */
interface adminAuthInterface
{
    /**
     * List all users for administration
     *
     * @return auth/user/indexHtml
     * @uses auth/userAccount/readIndex
     */
    public function readUseraccountIndex();

    /**
     * List all users for administration
     *
     * @return auth/user/indexHtml
     * @uses auth/userAccount/readUserlist
     */
    public function readUseraccountUserlist();

    /**
     * Prepare an empty user object
     *
     * @return auth/user/newUser
     *
     * @uses auth/userAccount/readNew
     */
    public function readUseraccountNew();

    /**
     * Record a new user account
     * @param auth/account $userAccount The user account object to record
     *
     * @uses auth/userAccount/create
     *
     * @return auth/user/addUser
     */
    public function createUseraccount($userAccount);

    /**
     * Prepare a user object for update
     *
     * @uses auth/userAccount/read_userAccountId_
     *
     * @return auth/user/edit
     */
    public function readUseraccountEdit_userAccountId_();

    /**
     * Allow to modify user information
     * @param auth/roleMember[] $roleMembers Array of role member object
     *
     * @uses auth/userAccount/update_userAccountId_
     * @return auth/user/updateUserInformation
     */
    public function updateUseraccount_userAccountId_($roleMembers);

    /**
     * Disable a user
     *
     * @return auth/user/disable
     * @uses auth/userAccount/updateDisable_userAccountId__replacingUserAccountId_
     */
    public function updateUseraccountDisable_userAccountId__replacingUserAccountId_();

    /**
     * Enable a user
     *
     * @return auth/user/enable
     * @uses auth/userAccount/updateEnable_userAccountId_
     */
    public function updateUseraccountEnable_userAccountId_();

    /**
     * Lock a user
     *
     * @uses auth/userAccount/updateLock_userAccountId_
     * @return auth/user/lock
     */
    public function updateUseraccountLock_userAccountId_();

    /**
     * Unlock a user
     *
     * @uses auth/userAccount/updateUnlock_userAccountId_
     * @return auth/user/unlock
     */
    public function updateUseraccountUnlock_userAccountId_();

    /**
     * Change a user password
     * @param string $newPassword The new password
     *
     * @uses auth/userAccount/updatePassword_userAccountId_
     * @return auth/user/setPassword
     */
    public function updateUseraccountSetpassword_userAccountId_($newPassword);

    /**
     * Required password change
     *
     * @uses auth/userAccount/updatePasswordchangerequest_userAccountId_
     * @return auth/user/requirePasswordChange
     */
    public function updateUseraccountRequirepasswordchange_userAccountId_();

    /**
     * Get the list of available roles
     *
     * @uses auth/role/readRoles_query_
     */
    public function readUseraccountQueryroles_query_();

    /**
     * Get the list of available users
     *
     * @uses auth/userAccount/readQuery_query_
     */
    public function readUseraccountQueryusers_query_();

    /* ************************************************************************
     * Roles
     *********************************************************************** */
    /**
     * List the authorization's roles
     *
     * @uses auth/role/readIndex
     * @return auth/adminRole/index
     */
    public function readAuthRoleIndex();

    /**
     * Prepare an empty role object
     *
     * @uses auth/role/readNewrole
     * @return auth/adminRole/newRole
     */
    public function readAuthRoleNew();

    /**
     * Add a new role
     * @param auth/role $role The role object to record
     *
     * @uses auth/role/create
     * @return auth/adminRole/create
     */
    public function createAuthRole($role);

    /**
     * Prepares access control object for update or create
     *
     * @uses auth/role/read_roleId_
     * @uses auth/publicUserStory/read
     *
     * @return auth/adminRole/edit
     */
    public function readAuthRole_roleId_();

    /**
     * Updates a role
     * @param auth/role $role The role object
     *
     * @uses auth/role/update_roleId_
     * @return auth/adminRole/update
     */
    public function updateAuthRole_roleId_($role);

    /**
     * Lock or unlock a role
     *
     * @uses auth/role/update_roleId_Status_status_
     *
     * @return auth/adminRole/changeStatus
     */
    public function updateAuthRole_roleId_Status_status_();

    /**
     * Delete an authorization role
     *
     * @uses auth/role/delete_roleId_
     *
     * @return auth/adminRole/delete
     */
    public function deleteAuth_roleId_();

    /*
     * SERVICE ACCOUNTS
     */
    /**
     * List all service account
     *
     * @uses auth/serviceAccount/readSearch
     * @return auth/serviceAccount/indexHtml
     */
    public function readServiceaccountList();

    /**
     * Get a service account
     *
     * @uses auth/serviceAccount/read_serviceAccountId_
     * @return auth/serviceAccount/edit
     */
    public function readServiceaccountEdit_serviceAccountId_();

    /**
     * Get a new service account
     *
     * @uses auth/serviceAccount/readNewservice
     * @return auth/serviceAccount/edit
     */
    public function readServiceaccountNew();

    /**
     * Get a new service account
     * @param auth/serviceAccount $serviceAccount The service account object
     * @param string              $orgId          The organization identifier
     *
     * @uses auth/serviceAccount/create
     *
     * @return auth/serviceAccount/create
     */
    public function createServiceaccount($serviceAccount, $orgId);

    /**
     * Get a new service account
     * @param string $serviceName The service name
     *
     * @uses auth/serviceAccount/createServicetoken
     */
    public function createServicetoken($serviceName);

    /**
     * Update service account
     * @param auth/serviceAccount $serviceAccount The service account object
     * @param string              $orgId          The organization identifier
     *
     * @uses auth/serviceAccount/update
     * @return auth/serviceAccount/update
     */
    public function updateServiceaccount($serviceAccount, $orgId);

    /**
     * Enable service account
     *
     * @uses auth/serviceAccount/updateEnable_serviceAccountId_
     * @return auth/serviceAccount/enable
     */
    public function updateServiceaccountEnable_serviceAccountId_();

    /**
     * Disable service account
     *
     * @uses auth/serviceAccount/updateDisable_serviceAccountId_
     * @return auth/serviceAccount/disable
     */
    public function updateServiceaccountDisable_serviceAccountId_();

    /**
     * Get the list of available users
     *
     * @uses auth/userAccount/readQuery_query_
     */
    public function readServiceaccountQueryservices_query_();
}
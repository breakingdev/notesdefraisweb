<?php
namespace presentation\businessExpenses\UserStory\app;

/**
 * Interface for user administration
 */
interface testInterface
{
    /**
     * test
     * @uses businessExpenses/test/readTest
     */
    public function readTest();
}

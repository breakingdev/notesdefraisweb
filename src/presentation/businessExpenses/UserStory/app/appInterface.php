<?php
namespace presentation\businessExpenses\UserStory\app;

/**
 * Interface for user administration
 */
interface appInterface
{
    /**
     * Welcome page
     *
     * @return businessExpenses/welcome/welcomePage
     */
    public function read();

    /**
     * No privilege
     *
     * @return auth/authentication/noPrivilege
     */
    public function readNoprivilege();
}

<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authentication.
 *
 * Dependency authentication is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authentication is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authentication.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authentication\Adapter\Database;
/**
 * Class for authentication of a user
 *
 * @package Dependency\Authentication
 * @author  Maarch Cyril  VAZQUEZ <cyril.vazquez@maarch.org>
 */
class Authentication
    implements \dependency\authentication\AuthenticationInterface
{
    /* Properties */
    protected $ds;

    protected $schema = "user";
    protected $class = "user";
    protected $id = "userId";
    protected $name = "userName";
    protected $password = "password";
    protected $enabled = "enabled";
    protected $locked = "locked";
    protected $displayName = "displayName";
    protected $passwordChangeRequired = 'passwordChangeRequired';

    protected $passwordEncryption = "sha256";


    /* Methods */
    /**
     * Constructor
     * @param \dependency\datasource\DatasourceInterface $ds      The Data Access Service
     * @param array                                      $options The authentication options
     *
     * @return void
     */
    public function __construct(\dependency\datasource\DatasourceInterface $ds, array $options=null)
    {
        $this->ds = $ds;
        if ($options) {
            foreach ($options as $name => $value) {
                if (property_exists(__CLASS__, $name)) {
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * Log a user account
     * @param string $username The user name
     * @param strinf $password The password
     *
     * @return bool
     */
    public function logIn($username, $password=false)
    {
        /* Check user existence */
        $databaseUser = $this->getDatabaseUserByName($username);

        if (!$databaseUser || !$this->checkUserPassword($databaseUser, $password)) {
            throw new \dependency\authentication\Exception\authenticationException("Username not registred or wrong password.");
        }

        /* Check locked */
        if (!$this->checkUserLock($databaseUser)) {
            throw new \dependency\authentication\Exception\userLockException("User is lock");
        }

        /* Check enabled */
        if (!$this->checkUserEnabled($databaseUser)) {
            throw new \dependency\authentication\Exception\userDisabledException("User is disabled");
        }

        /* Check passwordChangeRequired */
        if (!$this->checkUserPasswordValid($databaseUser)) {
            throw new \dependency\authentication\Exception\passwordChangeRequired();
        }

        $credential = new \dependency\authentication\credential($databaseUser->{$this->name});

        \laabs::setToken("AUTH", $credential, 86400);

        return $credential;
    }

    /**
     * Log out the currently logged user
     *
     * @return bool
     */
    public function logOut()
    {
         session_destroy();
    }

    /**
     * Get the currently logged user
     *
     * @return object
     */
    public function credential()
    {
        return \core\Globals\Session::get('dependency/authentication/credential');
    }

    /* Protected */
    /**
     * Get a user by its id
     * @param string $userId The user id
     *
     * @return object The database user
     * @author 
     */
    protected function getDatabaseUserById($userId)
    {
        $queryString = 'SELECT * FROM "' . $this->schema . '"."' . $this->class . '" WHERE "' . $this->id . '" = ?';

        $stmt = $this->ds->prepare($queryString);

        $stmt->execute(array($userId));

        $databaseUser = $stmt->fetch();

        return $databaseUser;
    }

    /**
     * Get a user by its name
     * @param string $username The user name
     *
     * @return object The database user
     * @author 
     */
    protected function getDatabaseUserByName($username)
    {
        $queryString = 'SELECT * FROM "' . $this->schema . '"."' . $this->class . '" WHERE "' . $this->name . '" = ?';

        $stmt = $this->ds->prepare($queryString);

        $stmt->execute(array($username));

        $databaseUser = $stmt->fetch();

        return $databaseUser;
    }

    /**
     * Get all database users
     * @param string $query
     *
     * @return array
     * @author 
     */
    protected function getDatabaseUsers($query=false)
    {
        $queryString = 'SELECT * FROM "' . $this->schema . '"."' . $this->class . '"';
        
        if ($query) {
            $queryString .= ' WHERE ' . $query;
        }

        $stmt = $this->ds->query($queryString);

        return $stmt->fetchAll();
    }

    protected function checkUserPassword($databaseUser, $password)
    {
        //$encryptedPassword = hash($this->passwordEncryption, $password);

        return ($databaseUser->{$this->password} == (string) $password);
    }

    protected function checkUserEnabled($databaseUser)
    {
        if (!property_exists($databaseUser, $this->enabled)) {
            throw new \Exception("User enable status is not available", 44);
        }

        return (bool) $databaseUser->{$this->enabled};
    }

    protected function checkUserLock($databaseUser)
    {
        if (!property_exists($databaseUser, $this->locked)) {
            throw new \Exception("User lock status is not available", 44);
        }

        return !((bool) $databaseUser->{$this->locked});
    }

    protected function checkUserPasswordValid($databaseUser)
    {
        if (!property_exists($databaseUser, $this->passwordChangeRequired)) {
            throw new \Exception("User password change required status is not available", 44);
        }

        return  !((bool) $databaseUser->{$this->passwordChangeRequired});
    }
}

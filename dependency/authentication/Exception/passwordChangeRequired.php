<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authentication.
 *
 * Dependency authentication is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authentication is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authentication.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authentication\Exception;

class passwordChangeRequired
    extends \core\Exception
{
    public $message = false;

    /**
     * undocumented function
     *
     * @return void
     * @author 
     */
    public function __construct(array $validationErrors=null)
    {
        parent::__construct("Invalid user information");
        $this->message = "Password must be change";
    }

}

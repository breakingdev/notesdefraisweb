<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency logger.
 *
 * Dependency logger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency logger is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency logger.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\logger;
require_once __DIR__ . DIRECTORY_SEPARATOR . "log4php" . DIRECTORY_SEPARATOR . "Logger.php";

class Logger 
{

    /**
     * Constuctor of logger
     * @param string $timeFormat
     */
    public function __construct($rootLogger=array(), $appenders=array())
    {
        $this->configure($rootLogger, $appenders);
    }

    /**
     * Set the time format
     * @param string $timeFormat
     */
    public function configure($rootLogger=array(), $appenders=array())
    {
        \Logger::configure(
            array(
                'rootLogger' => $rootLogger,
                'appenders' => $appenders
            )
        );
    }

    /**
     * Get logger
     * @param string $name
     */
    public function getLogger($name='main')
    {
        return \Logger::getLogger($name);
    }

}
<?php
/*
 * Copyright (C) 2015 Maarch
 *
 * This file is part of dependency authorization.
 *
 * Dependency authorization is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dependency authorization is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with dependency authorization.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace dependency\authorization;

interface AuthorizationInterface
{
    /* Properties */

    /* Methods */
    /**
     * Get the list of a user privileges, i.e. the bundle/controllers he can use
     *
     * @return array The array of unique (string) routes (bundle/controller) where the user can call actions
     */
    public function getUserPrivileges();

    /**
     * Checks if the user has access to the action
     * @param string $route  The action route to check privilege for
     *
     * @return bool
     */
    public function hasUserPrivilege($route);

    /**
     * Get a user list of access rules
     * @param string $className The class name for objects
     *
     * @return array The list of rules
     */
    public function getUserAccessRule($className);

    //public function hasUserAccess($class, $object);
}
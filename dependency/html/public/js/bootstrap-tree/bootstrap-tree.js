function initTree() {
    $('.tree > ul').attr('role', 'tree')
                    .find('ul')
                    .attr('role', 'group');

    $('.tree').find('li')
        .children('ul')
        .parent()
        .addClass('parent_li')
        .attr('role', 'treeitem')
        .find('> span')
        .attr('title', 'closedNode')
        .find('i:first')
        .on('click', function (e) {
            var children = $(this).closest('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                $(this).parent().attr('title', 'closedNode').find(' > i').addClass('fa-plus-square').removeClass('fa-minus-square');
            }
            else {
                children.show('fast');
                $(this).parent().attr('title','openedNode').find(' > i').addClass('fa-minus-square').removeClass('fa-plus-square');
            }
            e.stopPropagation();
            $('.tree').find('.hideTreeElement').css('display', 'none');

        });

                
    $('.parent_li').find('span:first').find('.fa:first').addClass('fa-plus-square');
    $('.parent_li').find(' > ul > li').hide();
}